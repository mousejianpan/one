package zhaoyang.music.Domain;

/**
 * Created by SPREADTRUM\david.zhao on 18-3-6.
 */

public class PlayEvent {
    public static int PLAY = 0;
    public static int PAUSE = 1;
    public static int PREVIOUS = 2;
    public static int NEXT = 3;
    public int status = 4;
    public void setStatus(int s) {
        status = s;
    }
    public int getStatus() {
        return status;
    }
}
