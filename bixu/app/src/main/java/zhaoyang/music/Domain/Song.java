package zhaoyang.music.Domain;

/**
 * Created by SPREADTRUM\david.zhao on 18-3-5.
 */

public class Song {
    private String name;
    private String path;
    private int longTime;
    private int nowTime;

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public int getLongTime() {
        return longTime;
    }

    public int getNowTime() {
        return nowTime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setLongTime(int longTime) {
        this.longTime = longTime;
    }

    public void setNowTime(int nowTime) {
        this.nowTime = nowTime;
    }
}
