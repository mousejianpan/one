package zhaoyang.music.Domain;

import java.util.Date;

import zhaoyang.music.DB.DataBaseHelper;
import zhaoyang.music.annotation.Column;
import zhaoyang.music.annotation.Table;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-9.
 */
@Table(name = DataBaseHelper.LOCAL_TABLE_NAME)
public class LocalList {
    private long id;
    @Column(name = "playlist_id")
    private int playlist_id;
    @Column(name = "audio_name")
    private String audio_name;
    @Column(name = "audio_path")
    private String audio_path;
    @Column(name = "add_date")
    private String add_date;
    @Column(name = "modified_date")
    private String modified_date;

    private int countlist;

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPlaylist_id() {
        return playlist_id;
    }

    public void setPlaylist_id(int playlist_id) {
        this.playlist_id = playlist_id;
    }

    public String getAudio_name() {
        return audio_name;
    }

    public void setAudio_name(String audio_name) {
        this.audio_name = audio_name;
    }

    public String getAudio_path() {
        return audio_path;
    }

    public void setAudio_path(String audio_path) {
        this.audio_path = audio_path;
    }

    public String getAdd_date() {
        return add_date;
    }

    public void setAdd_date(String add_date) {
        this.add_date = add_date;
    }

    public String getModified_date() {
        return modified_date;
    }

    public void setModified_date(String modified_date) {
        this.modified_date = modified_date;
    }

    public int getCountlist() {
        return countlist;
    }

    public void setCountlist(int countlist) {
        this.countlist = countlist;
    }
}
