package zhaoyang.music.Domain;

import java.util.Date;

import zhaoyang.music.DB.DataBaseHelper;
import zhaoyang.music.annotation.Column;
import zhaoyang.music.annotation.Table;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-9.
 */
@Table (name = DataBaseHelper.PLAY_TABLE_NAME)
public class PlayList {
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "add_date")
    private Date add_date;
    @Column(name = "modified_date")
    private Date modified_date;

    private int countSongs;
    public long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Date getAdd_date() {
        return add_date;
    }
    public void setAdd_date(Date add_date) {
        this.add_date = add_date;
    }

    public Date modified_date() {
        return modified_date;
    }

    public void setmodified_date(Date modified_date) {
        this.modified_date = modified_date;
    }

    public Integer getCountSongs() {
        return countSongs;
    }

    public void setCountSongs(Integer countSongs) {
        this.countSongs = countSongs;
    }
}
