package zhaoyang.music.utils;

import android.content.ContentValues;
import android.util.Log;

import java.lang.reflect.Field;

import zhaoyang.music.annotation.Column;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-9.
 */
public class ContentValuesBuilder <T>{
    private static ContentValuesBuilder ourInstance;
    public static ContentValues contentValues;

    public static ContentValuesBuilder getInstance() {
        if (null == ourInstance)
        {
            ourInstance = new ContentValuesBuilder();
        }
        contentValues = new ContentValues();
        return ourInstance;
    }

    private ContentValuesBuilder() {
    }

    public ContentValues builder(T value) throws IllegalAccessException{
        for (Field f: value.getClass().getDeclaredFields()) {
            //Log.e("david", "make data");
            if (f.getAnnotations().length != 0) {
                //Log.e("david", "make data not null");
                f.setAccessible(true);
                f.getType().getName();
                contentValues.put(f.getAnnotation(Column.class).name(),f.get(value).toString());
            }
        }
        return contentValues;

    }
}
