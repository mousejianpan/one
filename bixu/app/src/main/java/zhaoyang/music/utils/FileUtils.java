package zhaoyang.music.utils;

import java.io.File;

/**
 * Created by SPREADTRUM\david.zhao on 18-3-30.
 */

public class FileUtils {

    public static String getType(String name) {
        File f = new File(name);
        String fileName = f.getName();
        String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
        return prefix;
    }

    public static Boolean isType(String name, String type) {
        File f = new File(name);
        String fileName = f.getName();
        Boolean result = fileName.endsWith("." + type);
        return result;
    }

    public static String getType2(String name) {
        File file =new File(name);
        String fileName=file.getName();
        String[] token = fileName.split(".");
        String prefix = token[token.length - 1];
        return  prefix;
    }
}
