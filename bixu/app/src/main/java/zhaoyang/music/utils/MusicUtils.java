package zhaoyang.music.utils;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-12.
 */

public class MusicUtils {
    public String StringformatTime(long time) {
        time=time/1000;
        String strHour="" + (time / 3600);
        String strMinute= "" + time % 3600 / 60;
        String strSecond= "" + time % 3600 % 60;
        strHour = strHour.length() < 2 ? "0" + strHour : strHour;
        strMinute = strMinute.length() < 2 ? "0" + strMinute : strMinute;
        strSecond = strSecond.length() < 2 ? "0" + strSecond : strSecond;
        String strRsult = "";
        if(!strHour.equals("00")) {
            strRsult += strHour + ":";
        }
        strRsult += strMinute + ":" + strSecond;
        return strRsult;
    }
}
