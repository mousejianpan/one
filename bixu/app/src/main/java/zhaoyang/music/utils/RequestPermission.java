package zhaoyang.music.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-11.
 */

public class RequestPermission extends ContextWrapper {
    public final static int CODE_FOR_WRITE_PERMISSION = 0;
    public RequestPermission(Context base) {
        super(base);
    }
    public void requestPermissions(Activity activity, String string, int result) {
        int hasWriteContactsPermission = getApplicationContext().checkSelfPermission(string);

        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[] {string}, result);
        }
    }
}
