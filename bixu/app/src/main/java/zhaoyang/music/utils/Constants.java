package zhaoyang.music.utils;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-7.
 */

public class Constants {
    public static enum TAB_SPEC {
        MAIN_SPEC("1"), NET_SPEC("2"), SONG_SPEC("3");
        private String id;
        private TAB_SPEC(String id) {
            this.id = id;
        }
        public String getId() {
            return this.id;
        }
    };
    public static String rootPath = "/mymusic";
    public static String lrcPath = "/storage/emulated/0/lrc";
}
