package zhaoyang.music.Fragment;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.aspsine.swipetoloadlayout.OnLoadMoreListener;
import com.aspsine.swipetoloadlayout.OnRefreshListener;
import com.aspsine.swipetoloadlayout.SwipeRefreshHeaderLayout;
import com.aspsine.swipetoloadlayout.SwipeToLoadLayout;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;
import retrofit2.http.POST;
import zhaoyang.music.Activity.NewsDetailActivity;
import zhaoyang.music.Adapter.NewsAdapter;
import zhaoyang.music.Bean.News;
import zhaoyang.music.Bean.Picture;
import zhaoyang.music.R;
import zhaoyang.music.View.MyLoadingView;
import zhaoyang.music.View.MySwipeFooter;
import zhaoyang.music.View.MySwipeHeader;
import zhaoyang.music.interface_.OnCLickRecyclerViewItemListener;
import zhaoyang.music.interface_.OnClickNewsItemListener;
import zhaoyang.music.interface_.OnFragmentInteractionListener;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewsFragment extends Fragment implements OnClickNewsItemListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<News> newsList;
    private MyLoadingView loadImage;
    private SwipeToLoadLayout swipeToLoadLayout;
    private MySwipeHeader mySwipeHeader;
    private MySwipeFooter mySwipeFooter;
    private NewsAdapter newsAdapter;
    private int start = 0;
    private boolean update = false;
    private boolean noData = false;
    private AnimatorSet animatorSet;

    private OnFragmentInteractionListener mListener;

    public NewsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NewsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewsFragment newInstance(String param1, String param2) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setRetainInstance(true);
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_layout, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadImage = (MyLoadingView)getActivity().findViewById(R.id.load_image_refresh);
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(loadImage, "rotation", 0, 360);
        objectAnimator.setRepeatCount(ValueAnimator.INFINITE);
        objectAnimator.setInterpolator(new LinearInterpolator());
        objectAnimator.setDuration(2000);
        animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator);
        animatorSet.start();
        /*Glide.with(getActivity())
                .load(R.drawable.loading_now)
                .into(loadImage);*/
        loadData(0);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (animatorSet != null && animatorSet.isStarted()) {
            animatorSet.end();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCLick(News news) {
        Intent intent = new Intent(getActivity(), NewsDetailActivity.class);
        intent.putExtra("detail", news.getDetails());
        intent.putExtra("title", news.getTitle());
        intent.putExtra("time", news.getTime());
        intent.putExtra("source", news.getSource());
        intent.putExtra("image", news.getImage());
        startActivity(intent);
    }

    @Override
    public void onLongClick(News news) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface GetRequest_Interface {
        //@GET("openapi.do?keyfrom=Yanzhikai&key=2032414398&type=data&doctype=json&version=1.1&q=car")
        //@Url("openapi.do?keyfrom=Yanzhikai&key=2032414398&type=data&doctype=json&version=1.1&q=car")
        //@POST()

        //@HTTP(method = "POST", path = "nc/article/headline/T1348647909107/0-20.html", hasBody = true)
        @POST("nc/article/headline/T1348647909107/0-20.html")

            //Call<ResponseBody> getCall(@Body Gson gson);
        //Observable<ResponseBody> getMessage(@Body Gson gson);
        Observable<ResponseBody> getMessage();
    }

    public void loadData( final int begin) {
        newsList = new ArrayList<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                // UTF_8 for chinese
            /*try {
                musicName = URLEncoder.encode(musicName, UTF_8);
                musicName = musicName.replaceAll("\\+",  " ");
                singerName = URLEncoder.encode(singerName, UTF_8);
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
            musicName = getFileNameNoEx(musicName);*/
            URL mUrl = null;

            String strUrl = "http://c.m.163.com/nc/article/headline/T1348647909107/"
                    + Integer.toString(begin) + "-" + Integer.toString(begin + 20) + ".html";
            Log.e("david", "url = " + strUrl);

            try {
                mUrl = new URL(strUrl);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            try {
                HttpURLConnection httpConn = (HttpURLConnection) mUrl
                        .openConnection();
                httpConn.setRequestMethod("GET");
                httpConn.setConnectTimeout(10000);
                httpConn.setReadTimeout(5000);
                InputStreamReader in = new InputStreamReader(new InputStream() {
                    @Override
                    public int read() throws IOException {
                        return 0;
                    }
                });
                if (httpConn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e("david", "http  fail");
                    noData = true;
                } else {
                    try {
                        httpConn.connect();
                        Log.e("david", "http  success" + httpConn.getReadTimeout());
                        //test json data
                        in = new InputStreamReader(httpConn.getInputStream());
                    } catch (SocketTimeoutException e) {
                        e.printStackTrace();
                        noData = true;
                    }
                    if (!noData) {
                        BufferedReader bu = new BufferedReader(in);
                        String string;
                        StringBuilder stringBuilder = new StringBuilder();
                        while((string = bu.readLine()) != null) {
                            //Log.e("david", "string" + string + "\n");
                            stringBuilder.append(string);

                        }
                        JSONObject object = new JSONObject(stringBuilder.toString());
                        //Log.e("david", "jasonobject" + object.toString());
                        JSONArray array = object.getJSONArray("T1348647909107");
                        Log.e("david", "return " + httpConn.getErrorStream());
                        if (array == null || array.length() == 0) {
                            noData = true;
                        } else {
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject item = array.getJSONObject(i);
                                String name = item.getString("title");
                                String time = item.getString("mtime");
                                String content = item.getString("digest");
                                String source = item.getString("source");
                                String image = item.getString("imgsrc");
                                String details = item.getString("postid");
                                Log.e("david", "content = " + content);
                                if (content == null || content.equals("")||content.isEmpty()) {
                                    continue;
                                }
                                if (name == null || name.equals("")||name.isEmpty()) {
                                    continue;
                                }
                                if (time == null || time.equals("")||time.isEmpty()) {
                                    continue;
                                }
                                if (details == null || details.equals("")||details.isEmpty()) {
                                    continue;
                                }
                                if (source == null || source.equals("")||source.isEmpty()) {
                                    continue;
                                }

                                Log.e("david", "title" + name );
                                News news = new News();
                                news.setTitle(name);
                                news.setContent(content);
                                news.setTime(time);
                                news.setSource(source);
                                news.setImage(image);
                                news.setDetails(details);
                                newsList.add(news);
                            }
                        }
                    }
                    httpConn.disconnect();
                }
                Observable.create(new ObservableOnSubscribe<Object>() {
                    @Override
                    public void subscribe(ObservableEmitter<Object> e) throws Exception {
                        e.onNext(newsList);
                    }
                }).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<Object>() {
                            @Override
                            public void accept(Object o) throws Exception {
                                if (!update) {
                                    initRecyleView();
                                } else {
                                    if (!noData) {
                                        newsAdapter.updateData(newsList);
                                        newsAdapter.notifyDataSetChanged();
                                    }
                                    swipeToLoadLayout.setLoadingMore(false);
                                    swipeToLoadLayout.setRefreshing(false);
                                    update = false;
                                    noData = false;
                                }
                            }
                        });
            } catch (IOException e1) {
                e1.printStackTrace();
                Log.e("david", "http success , IO exception");
                return;
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("david", "XML parse error");
                return;
            }
            return;
            }
        }).start();
    }

    public static byte[] readParse(InputStream in) {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] data = new byte[1024];
        int len = 0;
        try {
            while ((len = in.read(data)) != -1) {
                outStream.write(data, 0, len);

            }

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.e("david", "readParse" + outStream.toByteArray().toString());

        return outStream.toByteArray();
    }

    public void initRecyleView() {
        Log.e("david", "1");
        swipeToLoadLayout = (SwipeToLoadLayout)getActivity().findViewById(R.id.swipeToLoadLayout);
        mySwipeHeader = (MySwipeHeader) getActivity().findViewById(R.id.swipe_refresh_header);
        mySwipeFooter = (MySwipeFooter) getActivity().findViewById(R.id.swipe_load_more_footer);
        recyclerView = (RecyclerView)getActivity().findViewById(R.id.swipe_target);
        if (recyclerView != null) {
            Log.e("david", "2");
            loadImage.setVisibility(View.GONE);
            layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            newsAdapter = new NewsAdapter(getActivity(), this, newsList);
            recyclerView.setAdapter(newsAdapter);
            recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                    //super.getItemOffsets(outRect, view, parent, state);
                    outRect.top = 5;
                    outRect.bottom = 5;
                }
            });
        }

        // down load more
        if (swipeToLoadLayout != null) {
            swipeToLoadLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    update = true;
                    start += 20;
                    loadData(start);
                }
            });
            // up refresh
            swipeToLoadLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh() {
                    update = true;
                    start += 20;
                    loadData(start);
                }
            });
        }
    }
}
