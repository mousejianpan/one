package zhaoyang.music.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.ListFragment;
import android.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import zhaoyang.music.Activity.MusicListActivity;
import zhaoyang.music.Adapter.PlayListAdapter;
import zhaoyang.music.Dao.PlayListDao;
import zhaoyang.music.Dao.imp.PlayListDaoImp;
import zhaoyang.music.Domain.PlayList;
import zhaoyang.music.interface_.OnFragmentInteractionListener;

import zhaoyang.music.R;

import static android.R.id.list;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddListFragment extends ListFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private PlayListDao playList;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    ArrayList<PlayList> listName = new ArrayList<PlayList>();
    private PlayListAdapter adapter;


    public AddListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddListFragment newInstance(String param1, String param2) {
        AddListFragment fragment = new AddListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setRetainInstance(true);
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_list, container, false);
        setPlayListAdapter();
        return view;
    }

    public void setPlayListAdapter() {
        playList = new PlayListDaoImp(getContext());
        //playList.createPlayList(getResources().getString(R.string.myfavourite));
        /*listName = (ArrayList<PlayList>) playList.getAllPlayList();
        adapter = new PlayListAdapter(getContext(), listName);
        setListAdapter(adapter);*/
        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                listName = (ArrayList<PlayList>) playList.getAllPlayList();
                adapter = new PlayListAdapter(getContext(), listName);
                e.onNext(adapter);
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {
                        setListAdapter(adapter);
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        longClickItem();
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), MusicListActivity.class);
                //Bundle bundle = new Bundle();
                //bundle.putInt("id", (int)listName.get(i).getId());
                intent.putExtra("id", (int)listName.get(i).getId());
                Log.e("david", "form id" + listName.get(i).getId());
                startActivity(intent);
            }
        });
    }

    public void longClickItem() {
        this.getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                final long k = l;
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                alertDialog.setTitle(getResources().getString(R.string.playlist_control));
                alertDialog.setIcon(R.drawable.ic_media);
                String[] items = {getResources().getString(R.string.addlist),
                        getResources().getString(R.string.rename), getResources().getString(R.string.delete)};
                alertDialog.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Log.e("david", "click = " + i);
                        dialogClick(i, k);
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.show();
                return true;
            }
        });
    }

    public void dialogClick(int i, long k) {
        final long j = k;
        switch (i) {
            case 0:
                final AlertDialog.Builder builder =
                        new AlertDialog.Builder(new ContextThemeWrapper(getContext(), android.R.style.Theme_Holo_Dialog));
                final EditText editText = new EditText(getContext());
                builder.setTitle(getResources().getString(R.string.addlist)).setView(editText);
                builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).setPositiveButton(getResources().getString(R.string.sure), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (editText.getText().toString().isEmpty() || editText.getText().toString() == null) {
                            dialogInterface.dismiss();
                            return;
                        }
                        ArrayList<PlayList> listNametest = new ArrayList<PlayList>();
                        PlayListDao playList = new PlayListDaoImp(getContext());
                        playList.createPlayList(editText.getText().toString());
                        listNametest = (ArrayList<PlayList>) playList.getAllPlayList();
                        Log.e("david", "new list size = " + listNametest.size());
                        listName.add(listNametest.get(listName.size()));
                        adapter.notifyDataSetChanged();
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
                break;
            case 1:
                final AlertDialog.Builder builder1 =
                        new AlertDialog.Builder(new ContextThemeWrapper(getContext(), android.R.style.Theme_Holo_Dialog));
                final EditText editText1 = new EditText(getContext());
                builder1.setTitle(getResources().getString(R.string.rename)).setView(editText1);
                builder1.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).setPositiveButton(getResources().getString(R.string.sure), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (editText1.getText().toString().isEmpty() || editText1.getText().toString() == null) {
                            dialogInterface.dismiss();
                            return;
                        }
                        Log.e("david", "edittext = " + editText1.getText().toString());
                        ArrayList<PlayList> listNametest = new ArrayList<PlayList>();
                        PlayListDao playList = new PlayListDaoImp(getContext());
                        playList.updatePlayList((int) listName.get((int) j).getId(), editText1.getText().toString());
                        listNametest = (ArrayList<PlayList>) playList.getAllPlayList();
                        Log.e("david", "new list size = " + listNametest.size());
                        listName.clear();
                        listName.addAll(listNametest);
                        adapter.notifyDataSetChanged();
                        dialogInterface.dismiss();
                    }
                });
                builder1.show();
                break;
            case 2:
                PlayListDao playList = new PlayListDaoImp(getContext());
                Log.e("david", "remove list  = " + k + listName.get((int)k).getName().toString() + "id = " + listName.get((int)k).getId());
                playList.removePlayList((int)listName.get((int)k).getId());
                //listName.remove(k);
                listName.clear();
                ArrayList<PlayList> listNametest = new ArrayList<PlayList>();
                listNametest = (ArrayList<PlayList>) playList.getAllPlayList();
                listName.addAll(listNametest);
                adapter.notifyDataSetChanged();
                break;
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    //change to interface package
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }*/
}
