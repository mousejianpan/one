package zhaoyang.music.Fragment;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import zhaoyang.music.Activity.PictureActivity;
import zhaoyang.music.Adapter.PictureAdapter;
import zhaoyang.music.Bean.Picture;
import zhaoyang.music.R;
import zhaoyang.music.interface_.OnCLickRecyclerViewItemListener;
import zhaoyang.music.interface_.OnFragmentInteractionListener;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PictureFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PictureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PictureFragment extends Fragment implements OnCLickRecyclerViewItemListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private PictureAdapter adapter;
    public List<Picture> pictureList;

    private OnFragmentInteractionListener mListener;
    private OnCLickRecyclerViewItemListener listener;

    public PictureFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PictureFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PictureFragment newInstance(String param1, String param2) {
        PictureFragment fragment = new PictureFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e("david", "picture create");
        setRetainInstance(true);
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.e("david", "picture create view");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_picture, container, false);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        Log.e("david", "picture activity create");
    }

    @Override
    public void onResume() {
        Log.e("david", "picture resume");
        super.onResume();
    }

    @Override
    public void onStart() {
        Log.e("david", "picture start");
        super.onStart();

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("david", "picture pause");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        Log.e("david", "picture attach");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        Log.e("david", "picture Datach");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("david", "picture dastory");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public void init() {
        recyclerView = (RecyclerView)getActivity().findViewById(R.id.recyclerview_picture);
        if (recyclerView != null) {
            layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            ((StaggeredGridLayoutManager)layoutManager).setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    ((StaggeredGridLayoutManager)layoutManager).invalidateSpanAssignments();
                }
            });

            Observable.create(new ObservableOnSubscribe<Object>() {
                @Override
                public void subscribe(ObservableEmitter<Object> e) throws Exception {
                    List<Picture> list = new ArrayList<>();
                    list.addAll(getPicture());
                    e.onNext(list);
                }
            }).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<Object>() {
                        @Override
                        public void accept(Object o) throws Exception {
                            //Log.e("david", "ok === accept" + pictureList.get(0).path + pictureList.get(1).path);
                            adapter = new PictureAdapter(getActivity(), pictureList , PictureFragment.this);
                            if (recyclerView != null) {
                                recyclerView.setLayoutManager(layoutManager);
                                recyclerView.setAdapter(adapter);
                                recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                                    int space = 5;
                                    @Override
                                    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                                        super.getItemOffsets(outRect, view, parent, state);
                                        outRect.left = space;
                                        outRect.right = space;
                                        outRect.bottom = space;
                                        //if (parent.getChildAdapterPosition(view) == 0) {
                                        outRect.top = space;
                                        //}
                                    }
                                });
                            }
                        }
                    });
        }
    }

    public List<Picture> getPicture() {
        pictureList = new ArrayList<Picture>();
        ContentResolver contentResolver = getActivity().getContentResolver();
        //DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        //SQLiteDatabase sqLiteDatabase = dataBaseHelper.getWritableDatabase();;
        //dataBaseHelper.clear(sqLiteDatabase);
        Cursor cursor = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null,
                null, null, MediaStore.Images.Media.DEFAULT_SORT_ORDER);
        Log.e("david", "get image = " + cursor.getCount());
        if (cursor.moveToFirst()) {
            for (int i = 0; i < cursor.getCount(); i++) {
                Picture picture = new Picture();
                cursor.moveToPosition(i);
                /*localList.setId(cursor.getLong(cursor.getColumnIndexOrThrow(
                        MediaStore.Images.Media._ID
                )));
                localList.setAudio_name(cursor.getString(cursor.getColumnIndexOrThrow(
                        MediaStore.Images.Media.DISPLAY_NAME
                )));
                localList.setAudio_path(cursor.getString(cursor.getColumnIndexOrThrow(
                        MediaStore.Images.Media.DATA
                )));
                localList.setAdd_date(new Date().toString());
                localList.setModified_date(new Date().toString());
                music_list.add(localList);*/
                picture.setPath(cursor.getString(cursor.getColumnIndexOrThrow(
                        MediaStore.Images.Media.DATA
                )));
                picture.setName(cursor.getString(cursor.getColumnIndexOrThrow(
                        MediaStore.Images.Media.DISPLAY_NAME
                )));
                picture.setBitmap(cursor.getString(cursor.getColumnIndexOrThrow(
                        MediaStore.Images.Thumbnails.DATA
                )));
                //use glide,can load path,change to bitmap in glide
                //Bitmap bitmap = BitmapFactory.decodeFile(picture.getPath());
                //picture.setBitmap(bitmap);
                pictureList.add(picture);
            }
        }
        cursor.close();
        return pictureList;
    }

    @Override
    public void onCLick(Picture picture) {
        Log.e("david" , picture.getName());
        openPicture(picture);
    }

    @Override
    public void onLongClick(Picture picture) {
        Log.e("david" , picture.getName());
        openPicture(picture);
    }

    public void openPicture(Picture picture) {
        Intent intent = new Intent(getActivity(), PictureActivity.class);
        intent.putExtra("data", picture.getPath());
        intent.putExtra("name", picture.getName());
        intent.putExtra("bitmap", picture.getBitmap());
        startActivity(intent);

    }
}
