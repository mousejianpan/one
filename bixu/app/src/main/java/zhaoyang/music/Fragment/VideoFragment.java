package zhaoyang.music.Fragment;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cn.jzvd.JZVideoPlayer;
import cn.jzvd.JZVideoPlayerStandard;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import zhaoyang.music.Adapter.PictureAdapter;
import zhaoyang.music.Adapter.VideoAdapter;
import zhaoyang.music.Bean.Picture;
import zhaoyang.music.Bean.Video;
import zhaoyang.music.R;
import zhaoyang.music.interface_.OnFragmentInteractionListener;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VideoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VideoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VideoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private VideoAdapter adapter;
    public List<Video> videoList;

    private OnFragmentInteractionListener mListener;

    public VideoFragment() {
        Exception exception = new Exception();
        exception.printStackTrace();
        Log.e("daviddavid", "create" + this.toString());
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VideoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VideoFragment newInstance(String param1, String param2) {
        VideoFragment fragment = new VideoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setRetainInstance(true);
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_net, container, false);
        /*JZVideoPlayerStandard jzVideoPlayerStandard = (JZVideoPlayerStandard) view.findViewById(R.id.videoplayer);
        jzVideoPlayerStandard.setUp("http://jzvd.nathen.cn/c6e3dc12a1154626b3476d9bf3bd7266/6b56c5f0dc31428083757a45764763b0-5287d2089db37e62345123a1be272f8b.mp4"
                , JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL, "饺子闭眼睛");
        //jzVideoPlayerStandard.thumbImageView.setImage("http://p.qpic.cn/videoyun/0/2449_43b6f696980311e59ed467f22794e792_1/640");
        jzVideoPlayerStandard.thumbImageView.setImageURI(Uri.parse("http://p.qpic.cn/videoyun/0/2449_43b6f696980311e59ed467f22794e792_1/640"));
        JZVideoPlayer.setTextureViewRotation(Configuration.ORIENTATION_PORTRAIT);*/
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        Log.e("david", "videofragment attach");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    @Override
    public void onStart() {
        Log.e("david", "videofragment start");
        super.onStart();
    }

    @Override
    public void onPause() {
        JZVideoPlayer.releaseAllVideos();
        super.onPause();
        Log.e("david", "homeFRAGMENT PAUSE");
    }

    public void init() {
        /*Video video = new Video();
        video.setPath("http://jzvd.nathen.cn/c6e3dc12a1154626b3476d9bf3bd7266/6b56c5f0dc31428083757a45764763b0-5287d2089db37e62345123a1be272f8b.mp4");
        video.setName("55555555");
        video.setThumb("http://p.qpic.cn/videoyun/0/2449_43b6f696980311e59ed467f22794e792_1/640");
        videoList.add(video);
        videoList.add(video);
        videoList.add(video);
        videoList.add(video);
        videoList.add(video);
        videoList.add(video);
        videoList.add(video);
        videoList.add(video);*/
        recyclerView = (RecyclerView)getActivity().findViewById(R.id.recyclerview_video);
        layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        /*adapter = new VideoAdapter(getActivity(), videoList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            int space = 5;
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.left = space;
                outRect.right = space;
                outRect.bottom = space;
                //if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = space;
                //}
            }
        });*/
        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                List<Video> list = new ArrayList<>();
                list.addAll(getVideo());
                e.onNext(list);
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {
                        //Log.e("david", "ok === accept" + videoList.get(0).getPath());
                        adapter = new VideoAdapter(getActivity(), videoList);
                        if (recyclerView != null) {
                            recyclerView.setLayoutManager(layoutManager);
                            recyclerView.setAdapter(adapter);
                            recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                                int space = 5;
                                @Override
                                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                                    super.getItemOffsets(outRect, view, parent, state);
                                    outRect.left = space;
                                    outRect.right = space;
                                    outRect.bottom = space;
                                    //if (parent.getChildAdapterPosition(view) == 0) {
                                    outRect.top = space;
                                    //}
                                }
                            });
                        }
                    }
                });
    }
/**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

public List<Video> getVideo() {
    videoList = new ArrayList<Video>();
    ContentResolver contentResolver = getActivity().getContentResolver();
    //DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
    //SQLiteDatabase sqLiteDatabase = dataBaseHelper.getWritableDatabase();;
    //dataBaseHelper.clear(sqLiteDatabase);
    Cursor cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null,
            null, null, MediaStore.Video.Media.DEFAULT_SORT_ORDER);
    Log.e("david", "get video = " + cursor.getCount());
    if (cursor.moveToFirst()) {
        for (int i = 0; i < cursor.getCount(); i++) {
            Video video = new Video();
            cursor.moveToPosition(i);
                /*localList.setId(cursor.getLong(cursor.getColumnIndexOrThrow(
                        MediaStore.Images.Media._ID
                )));
                localList.setAudio_name(cursor.getString(cursor.getColumnIndexOrThrow(
                        MediaStore.Images.Media.DISPLAY_NAME
                )));
                localList.setAudio_path(cursor.getString(cursor.getColumnIndexOrThrow(
                        MediaStore.Images.Media.DATA
                )));
                localList.setAdd_date(new Date().toString());
                localList.setModified_date(new Date().toString());
                music_list.add(localList);*/
            video.setPath(cursor.getString(cursor.getColumnIndexOrThrow(
                    MediaStore.Video.Media.DATA
            )));
            video.setThumb(cursor.getString(cursor.getColumnIndexOrThrow(
                    MediaStore.Video.Thumbnails.DATA
            )));
            video.setName(cursor.getString(cursor.getColumnIndexOrThrow(
                    MediaStore.Video.Media.DISPLAY_NAME
            )));
            video.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(
                    MediaStore.Video.Media.TITLE
            )));
            //use glide,can load path,change to bitmap in glide
            //Bitmap bitmap = BitmapFactory.decodeFile(picture.getPath());
            //picture.setBitmap(bitmap);
            videoList.add(video);
        }
    }
    cursor.close();
    return videoList;
}

}
