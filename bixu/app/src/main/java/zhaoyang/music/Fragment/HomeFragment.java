package zhaoyang.music.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import zhaoyang.music.Activity.MusicListActivity;
import zhaoyang.music.Adapter.HomeAdapter;
import zhaoyang.music.R;
import zhaoyang.music.interface_.OnFragmentInteractionListener;

import static android.R.id.list;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends ListFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ArrayList<String> menu_list;
    ListView listView;

    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        Exception exception = new Exception();
        exception.printStackTrace();
        Log.e("daviddavid", "create homefragment" + this.toString());
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e("daviddavid", "create homefragment" + this.toString());
        setRetainInstance(true);
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        menu_list = new ArrayList<String>();
        menu_list.add("L");
        menu_list.add("P");
        //home_list = (ListView)view.findViewById();
        HomeAdapter adapter = new HomeAdapter(this.getContext(), menu_list);
        setListAdapter(adapter);
        //initAdapter(view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        //registerForContextMenu(getListView());
        super.onResume();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        //super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(R.menu.option, menu);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */


    public void initAdapter(View view) {

    }
    public void initData(View view) {
        //listView = (ListView)view.findViewById(list);

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //super.onListItemClick(l, v, position, id);
        //Toast.makeText(this.getContext(), "" + position, Toast.LENGTH_SHORT).show();
        if (0 == position) {
            Log.e("david", "open songlist");
            Intent intent = new Intent(this.getActivity(), MusicListActivity.class);
            this.getActivity().startActivity(intent);
        } else {
            sendBroadcast();
        }
    }

    private void sendBroadcast() {
        String content = "open_list";
        Intent intent = new Intent();
        intent.setAction("ACTION_OPEN");
        //data
        intent.putExtra("data",content);
        //send
        getActivity().sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        Log.e("david------------", "homefragment destory");
        //unregisterForContextMenu(getListView());
        super.onDestroy();
    }
}
