package zhaoyang.music.Component;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.view.ContextThemeWrapper;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-26.
 */

public class AlertDialogBuilder{
    private static Builder build;
    private static ContextThemeWrapper contextThemeWrapper;
    public static Builder getBuilder(Context context) {
        contextThemeWrapper = new ContextThemeWrapper(context, android.support.design.R.style.Theme_AppCompat_Dialog);
        build = new AlertDialog.Builder(contextThemeWrapper);
        return build;
    }

}
