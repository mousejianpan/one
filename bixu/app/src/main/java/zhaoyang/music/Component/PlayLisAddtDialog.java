package zhaoyang.music.Component;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;

import zhaoyang.music.Dao.PlayListDao;
import zhaoyang.music.Dao.imp.PlayListDaoImp;
import zhaoyang.music.R;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-26.
 */

public class PlayLisAddtDialog extends AlertDialogBuilder {
    private static PlayListDao playListDao;
    public static AlertDialog.Builder getCreatePlayListBUilder(final Context context) {
        AlertDialog.Builder builder;
        builder = AlertDialogBuilder.getBuilder(context);
        final EditText editText = new EditText(context);
        builder.setView(editText);
        builder.setTitle(R.string.playlist_control);
        builder.setPositiveButton(R.string.sure, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                String name = editText.getText().toString();
                playListDao = new PlayListDaoImp(context);
                playListDao.createPlayList(name);
            }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).setCancelable(true);
        return builder;
    }
}
