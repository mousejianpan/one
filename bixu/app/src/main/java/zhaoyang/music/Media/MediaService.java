package zhaoyang.music.Media;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;

import java.io.IOException;

import zhaoyang.music.Activity.HomeActivity;
import zhaoyang.music.Activity.MainActivity;
import zhaoyang.music.Activity.PlayActivity;
import zhaoyang.music.Application.MusicApplication;
import zhaoyang.music.IMediaAidlInterface;
import zhaoyang.music.R;

public class MediaService extends Service {
    private static MediaService instance;
    private RemoteViews remoteViews;
    private ServiceReceiver onClickReceiver;
    private Notification notification;
    private NotificationManager notificationManager;
    private int NOTIFICATION_ID = 0x1;
    private final IBinder myBinder = new ServiceStub();
    protected IBinder binder = new TestBinder();
    private MusicPlayer musicPlayer = MusicPlayer.getInstance();
    public int playPosition;
    PendingIntent pendingIntent;
    public MediaService() {
    }
    public static MediaService getInstance() {
        if (instance == null) {
            instance = new MediaService();
        }
        return instance;
    }

    public class TestBinder extends Binder {
        public MediaService getService() {
            return MediaService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        //return myBinder;
        startNotification();
        return binder;
    }
    public void play(String path) {
        updateNotification(1);
        musicPlayer.play(path);
    }
    public void start() {
        updateNotification(1);
        musicPlayer.start();
    }
    public void stop() {
        //cancelNotification();
        musicPlayer.stop();
    }
    public void pause() throws IllegalStateException {
        updateNotification(2);
        musicPlayer.pause();
    }
    public void reset() {
        //cancelNotification();
        musicPlayer.reset();
    }
    public int getCurrentPosition() {
        return musicPlayer.getCurrentPosition();
    }
    public boolean isPlaying() {
        return musicPlayer.isPlaying();
    }
    public void seekTo(int i) {
        Log.e("david", "seek" + musicPlayer.toString());
        musicPlayer.seekTo(i);
    }
    public int getDuration() {
        return musicPlayer.getDuration();
    }
    public void setOnSeekCompleteListener(MediaPlayer.OnSeekCompleteListener listener) {
        musicPlayer.setOnSeekCompleteListener(listener);
    }

    public void setDataSource(String path) throws IOException {
       musicPlayer.setDataSource(path);
    }
    public void setOnCompletionListener(MediaPlayer.OnCompletionListener listener) {
        musicPlayer.setOnCompletionListener(listener);
    }
    public void setOnErrorListener(MediaPlayer.OnErrorListener listener) {
        musicPlayer.setOnErrorListener(listener);
    }
    public void setOnPreparedListener(MediaPlayer.OnPreparedListener listener) {
        musicPlayer.setOnPreparedListener(listener);
    }
    public boolean isPause() {
        return musicPlayer.isPause();
    }
    public String getPlayingPath() {
        return musicPlayer.getPlayingPath();
    }
    public void setPlayingPath(String path) {
        musicPlayer.setPlayingPath(path);
    }
    public void setPlayingName(String name) {
        musicPlayer.setPlayingName(name);
    };
    public String getPlayingName() {
        return musicPlayer.getPlayingName();
    }

    @Override
    public void onCreate() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("CLICK");
        onClickReceiver = new ServiceReceiver();
        registerReceiver(onClickReceiver, filter);
        super.onCreate();
    }

    public void startNotification() {
        Log.e("david", "this" + this.toString());
        Log.e("david", "context" + Context.class.toString());
        Intent intent = new Intent(getApplicationContext(),
                PlayActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews = new RemoteViews(this.getPackageName(), R.layout.notification_layout);
        remoteViews.setImageViewResource(R.id.iv_image, R.drawable.icon);
    }

    public void updateNotification(int from) {
        Intent buttonIntent = new Intent();
        buttonIntent.setAction("CLICK");
        PendingIntent pendButtonIntent;
        if (from == 1) {
            Log.e("david", "update media to playing");
            remoteViews.setImageViewResource(R.id.iv_play, R.drawable.list_playing_indicator);
        } else if (from == 2) {
            Log.e("david", "update media to pause");
            remoteViews.setImageViewResource(R.id.iv_play, R.drawable.list_pause_indicator);
        }
        pendButtonIntent = PendingIntent.getBroadcast(this, 1, buttonIntent, 0);
        remoteViews.setOnClickPendingIntent(R.id.iv_play, pendButtonIntent);
        remoteViews.setTextViewText(R.id.title, musicPlayer.getPlayingName());


        // 3.0 -  notification use builder
        final Notification.Builder builder = new Notification.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.splash)
                .setContentTitle("test")
                .setContentText("test music")
                .setContentIntent(pendingIntent)
                .setContent(remoteViews);
        notification = builder.build();
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notification.flags |= Notification.FLAG_NO_CLEAR;
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notification);
        startForeground(NOTIFICATION_ID, notification);
    }

    public class ServiceReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("CLICK")) {
                //send broadcast to musiclistactivity
                if (musicPlayer.isPlaying()) {
                    Log.e("david", "receive click media playing to pause");
                    Intent pauseIntent = new Intent();
                    pauseIntent.setAction("ACTION_PAUSE");
                    musicPlayer.pause();
                    sendBroadcast(pauseIntent);
                    remoteViews.setImageViewResource(R.id.iv_play, R.drawable.list_pause_indicator);

                } else if (musicPlayer.isPause()) {
                    Log.e("david", "receive click media pause to play");
                    Intent playIntent = new Intent();
                    playIntent.setAction("ACTION_PLAY");
                    musicPlayer.start();
                    sendBroadcast(playIntent);
                    remoteViews.setImageViewResource(R.id.iv_play, R.drawable.list_playing_indicator);
                }
                Intent in = new Intent(getApplicationContext(),
                        PlayActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),
                        0, in, PendingIntent.FLAG_UPDATE_CURRENT);
                final Notification.Builder builder = new Notification.Builder(getApplicationContext())
                        .setSmallIcon(R.drawable.splash)
                        .setContentTitle("test")
                        .setContentText("test music")
                        .setContentIntent(pi)
                        .setContent(remoteViews);
                builder.setContent(remoteViews);
                notification = builder.build();
                notificationManager.notify(NOTIFICATION_ID, notification);
            }
        }
    }

    public void cancelNotification() {
        stopForeground(true);
        Log.e("david", "notification cancel");
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }

    private class ServiceStub extends IMediaAidlInterface.Stub {

        @Override
        public void play(String path) throws RemoteException {
            musicPlayer.play(path);
        }

        @Override
        public void start() throws RemoteException {
            musicPlayer.start();
        }

        @Override
        public void stop() throws RemoteException {
            musicPlayer.stop();
        }

        @Override
        public void pause() throws RemoteException {
            musicPlayer.pause();
        }

        @Override
        public void reset() throws RemoteException {
            musicPlayer.reset();
        }

        @Override
        public int getCurrentPosition() throws RemoteException {
            return musicPlayer.getCurrentPosition();
        }

        @Override
        public boolean isPlaying() throws RemoteException {
            return musicPlayer.isPlaying();
        }

        @Override
        public void seekTo(int i) throws RemoteException {
            musicPlayer.seekTo(i);
        }

        @Override
        public int getDuration() throws RemoteException {
            return musicPlayer.getDuration();
        }

        @Override
        public void setOnSeekCompleteListener() throws RemoteException {

        }

        @Override
        public void setDataSource(String path)throws RemoteException{
            try {
                musicPlayer.setDataSource(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void setOnCompletionListener() throws RemoteException {

        }

        @Override
        public boolean isPause() throws RemoteException {
            return musicPlayer.isPause();
        }

        @Override
        public String getPlayingPath() throws RemoteException {
            return musicPlayer.getPlayingPath();
        }

        @Override
        public void setPlayingPath(String path) throws RemoteException {
            musicPlayer.setPlayingPath(path);
        }

        @Override
        public void setPlayingName(String name) throws RemoteException {
            musicPlayer.setPlayingName(name);
        }

        @Override
        public String getPlayingName() throws RemoteException {
            return getPlayingName();
        }
    }

    @Override
    public void onDestroy() {
        Log.e("david", "music service destory");
        if (musicPlayer != null) {
            Log.e("david", "music player null");
            musicPlayer.stop();
            musicPlayer.release();
            musicPlayer = null;
        }
        if (MusicApplication.mediaService != null) {
            MusicApplication.mediaService = null;
            if (musicPlayer != null) {
                Log.e("david", "music service palyer null");
                musicPlayer.stop();
                musicPlayer.release();
                musicPlayer = null;
            }
        }
        unregisterReceiver(onClickReceiver);
        cancelNotification();
        super.onDestroy();
    }
}
