package zhaoyang.music.Media;

import android.media.MediaPlayer;
import android.util.Log;

import java.io.IOException;

import zhaoyang.music.Application.MusicApplication;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-11.
 */

public class MusicPlayer extends MediaPlayer {
    private  boolean isPause = false;
    private String currentPath = null;
    private String songName = null;
    private static MusicPlayer instance;

    private MusicPlayer() {
    }
    public static MusicPlayer getInstance() {
        if (instance == null) {
            instance = new MusicPlayer();
        }
        return instance;
    }

    @Override
    public void start() throws IllegalStateException {
        if (isPlaying()) {
            stop();
        }
        isPause = false;
        super.start();
    }

    @Override
    public void stop() throws IllegalStateException {
        isPause = false;
        super.stop();
    }

    @Override
    public void pause() throws IllegalStateException {
        isPause = true;
        super.pause();
    }

    @Override
    public void reset() {
        isPause = false;
        super.reset();
    }

    @Override
    public int getCurrentPosition() {
        return super.getCurrentPosition();
    }

    @Override
    public boolean isPlaying() {
        return super.isPlaying();
    }

    @Override
    public void seekTo(int i) throws IllegalStateException {
        super.seekTo(i);
    }

    @Override
    public int getDuration() {
        return super.getDuration();
    }

    @Override
    public void setOnSeekCompleteListener(OnSeekCompleteListener listener) {
        super.setOnSeekCompleteListener(listener);
    }

    @Override
    public void setDataSource(String path) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        super.setDataSource(path);
    }

    @Override
    public void setOnCompletionListener(OnCompletionListener listener) {
        super.setOnCompletionListener(listener);
    }

    public void play(String path) {
        //Log.e("david play", path);
        MusicApplication.playingName = getPlayingName();
        //Log.e("david play", MusicApplication.playingName);
        currentPath = path;
        try {
            super.setDataSource(path);
            if (!isPause) {
                super.prepare();
            }
            start();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isPause() {
        return isPause;
    }

    public String getPlayingPath() {
        return this.currentPath;
    }

    public void setPlayingPath(String path) {
        currentPath = path;
    }

    public void setPlayingName(String name) {songName = name;};

    public String getPlayingName() {
        return songName;
    }
}
