package zhaoyang.music.interface_;

import android.net.Uri;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-7.
 */

public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    void onFragmentInteraction(Uri uri);
}
