package zhaoyang.music.interface_;

import zhaoyang.music.Bean.News;

/**
 * Created by SPREADTRUM\david.zhao on 18-4-9.
 */

public interface OnClickNewsItemListener {
    public void onCLick(News news);
    public void onLongClick(News news);
}
