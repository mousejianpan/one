package zhaoyang.music.interface_;

import android.view.View;

import zhaoyang.music.Bean.Picture;

/**
 * Created by SPREADTRUM\david.zhao on 18-3-27.
 */

public interface OnCLickRecyclerViewItemListener {
    public void onCLick(Picture picture);
    public void onLongClick(Picture picture);
}
