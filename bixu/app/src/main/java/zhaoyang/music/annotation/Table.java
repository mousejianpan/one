package zhaoyang.music.annotation;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-9.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
public @interface Table {
    String name() default "";
}

