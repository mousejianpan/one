package zhaoyang.music.lyric;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Url;
import zhaoyang.music.Activity.PlayActivity;
import zhaoyang.music.Application.MusicApplication;
import zhaoyang.music.R;
import zhaoyang.music.utils.Constants;

/**
 * Created by SPREADTRUM\david.zhao on 18-3-8.
 */

public class LyricDownloadManager {
    private static final String TAG = LyricDownloadManager.class
            .getSimpleName();
    public static final String GB2312 = "GB2312";
    public static final String UTF_8 = "utf-8";
    private final int mTimeOut = 10 * 1000;
    private LyricXMLParser mLyricXMLParser = new LyricXMLParser();
    private URL mUrl = null;
    private int mDownloadLyricId = -1;
    private String lricAddress;
    private LoadLyricListener loadLyricListener;

//	private Context mContext = null;

    public LyricDownloadManager(Context c, LoadLyricListener load) {
//		mContext = c;
        this.loadLyricListener = load;
    }

    //get extension name
    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot >-1) && (dot < (filename.length() - 1))) {
                return filename.substring(dot + 1);
            }
        }
        return filename;
    }

    // delete extension name
    public static String getFileNameNoEx(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot >-1) && (dot < (filename.length()))) {
                return filename.substring(0, dot);
            }
        }
        return filename;
    }

    /*
     * get lrc from web
     */
    /*public String searchLyricFromWeb(String musicName, String singerName, String oldMusicName) {
        // UTF_8 for chinese
        try {
            musicName = URLEncoder.encode(musicName, UTF_8);
            musicName = musicName.replaceAll("\\+",  " ");
            singerName = URLEncoder.encode(singerName, UTF_8);
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        musicName = getFileNameNoEx(musicName);

        String strUrl = "http://geci.me/api/lyric/" + musicName;
        Log.e("david", "url = " + strUrl);

        try {
            mUrl = new URL(strUrl);
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        try {
            HttpURLConnection httpConn = (HttpURLConnection) mUrl
                    .openConnection();
            //httpConn.setRequestMethod("get");
            httpConn.setReadTimeout(mTimeOut);
            if (httpConn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.e(TAG, "http  fail");
                return null;
            }
            httpConn.connect();
            Log.e(TAG, "http  success");

            lricAddress = LyricJsonParser.getLriList(httpConn.getInputStream());

            httpConn.disconnect();
        } catch (IOException e1) {
            e1.printStackTrace();
            Log.e(TAG, "http success , IO exception");
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "XML parse error");
            return null;
        }
        return fetchLyricContent(musicName, singerName, oldMusicName);
    }*/

    public String searchLyricFromWeb(String musicName, String singerName, String oldMusicName) {
        //http://geci.me/api/lyric/%E4%B8%BA%E7%88%B1%E7%97%B4%E7%8B%82
        final String name = musicName;
        final StringBuilder lyricPath = new StringBuilder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://geci.me/api/lyric/")
                //.client(getOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        Lyric lyric = retrofit.create(Lyric.class);
        //Call<ResponseBody> call = lyric.getLyric("Sailing");
        musicName = getFileNameNoEx(musicName);
        Observable<ResponseBody> observable = lyric.getLyric(musicName);
        observable.doOnNext(new Consumer<ResponseBody>() {
            @Override
            public void accept(ResponseBody responseBody) throws Exception {
                StringBuilder builder = new StringBuilder();
                try {
                    builder.append(responseBody.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("david", "content = " + builder);
                try {
                    JSONObject jsonObject = new JSONObject(builder.toString());
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = jsonObject.getJSONArray("result");
                    if (jsonArray.length() == 0) {
                        return;
                    }
                    jsonObject = jsonArray.getJSONObject(0);
                    final StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(jsonObject.getString("lrc"));
                    Log.e("david", "lyric  = " + stringBuilder);
                    lyricPath.append(downLyric(stringBuilder.toString(), name));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                responseBody.close();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(ResponseBody responseBody) throws Exception {
                        loadLyricListener.loadedLyric();
                    }
                });
        /*call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("david = ", response.code() + response.message() + response.headers() + "///" + response.body().toString());

                if (response.body() == null || response.body().contentLength() == 0) {
                    Log.e("david", "body is null");
                    return;
                }
                ResponseBody responseBody = response.body();
                StringBuilder builder = new StringBuilder();
                try {
                    builder.append(responseBody.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("david", "content = " + builder);
                try {
                    JSONObject jsonObject = new JSONObject(builder.toString());
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = jsonObject.getJSONArray("result");
                    jsonObject = jsonArray.getJSONObject(0);
                    final StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(jsonObject.getString("lrc"));
                    Log.e("david", "lyric  = " + stringBuilder);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            downLyric(stringBuilder.toString());
                        }
                    }).start();

                } catch (JSONException e) {
                    e.printStackTrace();
                }



                responseBody.close();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("david", "failure" + t.toString());

            }
        });*/
        return lyricPath.toString();
    }

    public String downLyric(String stringBuilder, String name) {
        try {
            StringBuilder string = new StringBuilder();
            String s = null;
            URL url = new URL(stringBuilder);
            InputStreamReader inputStreamReader = new InputStreamReader(url.openStream());
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            while ((s = bufferedReader.readLine()) != null) {
                string.append(s);
            }
            Log.e("david" , "lyric " + string);
            inputStreamReader.close();
            bufferedReader.close();
            File file = new File(Constants.lrcPath);
            if (!file.exists()) {
                file.mkdirs();
            }
            StringBuilder fileName = new StringBuilder();
            fileName.append(Constants.lrcPath + File.separator + name + ".lrc");
            Log.e("david", fileName.toString());
            File lyricFile = new File(fileName.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(lyricFile));
            outputStreamWriter.write(string.toString());
            outputStreamWriter.close();
            return fileName.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public interface Lyric {
        @GET
        Observable<ResponseBody> getLyric(@Url String s);
    }

    /**
     * download lric
     * */
    private String fetchLyricContent(String musicName, String singerName, String oldMusicName) {
        if (lricAddress == null) {
            Log.e("david", "there no lric");
            return null;
        }
        BufferedReader br = null;
        StringBuilder content = null;
        String temp = null;
        String lyricURL = lricAddress;
        Log.e(TAG, "down lric address:" + lyricURL);

        try {
            mUrl = new URL(lyricURL);
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        }

        try {
            br = new BufferedReader(new InputStreamReader(mUrl.openStream()));
            if (br != null) {
                content = new StringBuilder();
                while ((temp = br.readLine()) != null) {
                    content.append(temp);
                    Log.e(TAG, "<Lyric>" + temp);
                }
                br.close();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            Log.e(TAG, " get lric fail");
        }

        try {
            musicName = URLDecoder.decode(musicName, UTF_8);
            singerName = URLDecoder.decode(singerName, UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (content != null) {
//			String folderPath = PreferenceManager.getDefaultSharedPreferences(
//					mContext).getString(SettingFragment.KEY_LYRIC_SAVE_PATH,
//					Constant.LYRIC_SAVE_FOLDER_PATH);
            String folderPath = Constants.lrcPath;

            File savefolder = new File(folderPath);
            if (!savefolder.exists()) {
                savefolder.mkdirs();
            }
            String savePath = folderPath + File.separator + oldMusicName
                    + ".lrc";
//			String savePath = folderPath + File.separator + musicName + ".lrc";
            Log.e(TAG, "lric path:" + savePath);

            saveLyric(content.toString(), savePath);

            return savePath;
        } else {
            return null;
        }

    }

    /** save lric */
    private void saveLyric(String content, String filePath) {
        File file = new File(filePath);
        try {
            OutputStream outstream = new FileOutputStream(file);
            OutputStreamWriter out = new OutputStreamWriter(outstream);
            out.write(content);
            out.close();
            loadLyricListener.loadedLyric();
        } catch (java.io.IOException e) {
            e.printStackTrace();
            Log.i(TAG, " io error");
        }
        Log.i(TAG, "save success");
    }
    public interface LoadLyricListener {
        public void loadedLyric();
    }
}
