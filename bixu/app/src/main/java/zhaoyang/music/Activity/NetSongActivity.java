package zhaoyang.music.Activity;

import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.schedulers.RxThreadFactory;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;
import zhaoyang.music.Bean.MyConverter;
import zhaoyang.music.Fragment.HomeFragment;
import zhaoyang.music.Fragment.NewsFragment;
import zhaoyang.music.Fragment.PictureFragment;
import zhaoyang.music.Fragment.VideoFragment;
import zhaoyang.music.R;
import zhaoyang.music.interface_.OnFragmentInteractionListener;

import static java.nio.charset.StandardCharsets.UTF_8;

public class NetSongActivity extends AppCompatActivity implements OnFragmentInteractionListener {
    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;
    private PagerAdapter mFragmentPagerAdapter;
    private PagerAdapter mFragmentStatePagerAdapter;
    private List<Fragment> mFragments = new ArrayList<Fragment>();
    //标题
    TextView tv_title;
    //四个TextView控件
    private RadioButton[] mTabTVs = new RadioButton[4];
    //四个控件【未】按下时的图片id
    private int[] mTabTVIdsNormal;
    //四个控件按下时的图片id
    private int[] mTabTVIdsPress;

    TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_net_song2);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        //ImageView imageView = (ImageView)findViewById(R.id.imageview);
        //Glide.with(this).load(R.drawable.splash).preload();
        //Glide.with(this).load(R.drawable.splash).into(imageView);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/


        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                e.onNext("1");
                e.onNext("2");
                e.onNext("3");
                e.onNext("4");
            }
        })/*.subscribe(new Observer<Object>() {
            private int i;
            private Disposable disposable;
            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;

            }

            @Override
            public void onNext(Object o) {
                i++;
                Log.e("david", "value = " + o.toString());
                if (i == 2) {
                    disposable.dispose();
                }

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });*/
        .subscribeOn(Schedulers.newThread()).doOnNext(new Consumer<Object>() {
            @Override
            public void accept(Object o) throws Exception {
                Log.e("david", "thread" + Thread.currentThread().getName());
            }
        }).observeOn(Schedulers.io()).subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object o) throws Exception {
                Log.e("david", "thread - ob" + Thread.currentThread().getName());
            }
        });


        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
            }
        }).doOnNext(new Consumer<Object>() {
            @Override
            public void accept(Object o) throws Exception {

            }
        });


        //Retrofit
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://fanyi.youdao.com/")
                        .addConverterFactory(MyConverter.create())
                        .build();
                GetRequest_Interface service = retrofit.create(GetRequest_Interface.class);
                Call<ResponseBody> call = service.getCall(new Gson());
                //"?keyfrom=Yanzhikai&key=2032414398&type=data&doctype=json&version=1.1&q=car"
                try {
                    Response<ResponseBody> response = call.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Call<ResponseBody> clone = call.clone();
                clone.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.e("david = ", response.code() + response.message() + response.headers());

                        if (response.body() == null || response.body().contentLength() == 0) {
                            Log.e("david", "body is null");
                            return;
                        }
                        ResponseBody responseBody = response.body();
                        //Log.e("david", "response = " + response.body().toString());
                        BufferedSource source = responseBody.source();
                        try {
                            source.request(Long.MAX_VALUE); // Buffer the entire body.
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Buffer buffer = source.buffer();

                        Charset charset = UTF_8;
                        MediaType contentType = responseBody.contentType();
                        if (contentType != null) {
                            try {
                                charset = contentType.charset(charset);
                            } catch (UnsupportedCharsetException e) {
                                Log.e("david", "");
                                Log.e("david", "Couldn't decode the response body; charset is likely malformed.");
                                Log.e("david", "<-- END HTTP");
                            }
                        }
                        Log.e("david", "content = " + buffer.clone().readString(charset));
                        Gson gson = new Gson();
                        JsonParser jsonParser = new JsonParser();
                        JsonElement jsonElement = jsonParser.parse(buffer.clone().readString(charset));
                        JsonObject jsonObject = jsonElement.getAsJsonObject();
                        //JsonArray jsonArray = jsonElement.getAsJsonArray();
                        JsonArray jsonArray = jsonObject.getAsJsonArray("web");
                        //JsonElement a = jsonObject.get("query");
                        for (JsonElement a : jsonArray) {
                            Log.e("david", "everyone" + a.toString());
                        }
                        responseBody.close();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("david", "fail" + t.getMessage());

                    }
                });
            }
        }).start();*/
        //OkHttpClient okHttpClient = new OkHttpClient();

        // it is wrong, add returns an immutable list of interceptors
        /*okHttpClient.interceptors().add(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Log.e("david", "it is maine, stop");
                okhttp3.Response response = chain.proceed(chain.request());
                return response;
            }
        });*/

        /*okhttp3 update addinterceptor,can't through add()
          But why? source code :
          public Builder addInterceptor(Interceptor interceptor) {
              interceptors.add(interceptor);
              return this;
          }
          alse is add(), i can't understand

          sorry, i know it.  interceptors in Builder is editadble
          so two ways is ok      **/

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                /*.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Log.e("david", "it is maine, stop");
                okhttp3.Response response = chain.proceed(chain.request());
                return null;
            }
        })*/.build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://fanyi.youdao.com/")
                .addConverterFactory(MyConverter.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
        GetRequest_Interface rxjava = retrofit.create(GetRequest_Interface.class);

        rxjava.getMessage(new Gson()).subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .doOnNext(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(ResponseBody responseBody) throws Exception {
                        Log.e("david", "currentthread1" + Thread.currentThread());
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        Log.e("david", "currentthread2" + Thread.currentThread());
                        Buffer buffer = responseBody.source().buffer();
                        JsonParser jsonParser = new JsonParser();
                        JsonElement jsonElement = jsonParser.parse(buffer.readString(UTF_8));
                        JsonObject jsonObject = jsonElement.getAsJsonObject();
                        JsonArray jsonArray = jsonObject.getAsJsonArray("web");
                        for (JsonElement j : jsonArray) {
                            Log.e("david", "everyone = " + j.toString());
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
                /*.subscribe(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(ResponseBody responseBody) throws Exception {
                        Buffer buffer = responseBody.source().buffer();
                        JsonParser jsonParser = new JsonParser();
                        JsonElement jsonElement = jsonParser.parse(buffer.readString(UTF_8));
                        JsonObject jsonObject = jsonElement.getAsJsonObject();
                        JsonArray jsonArray = jsonObject.getAsJsonArray("web");
                        for (JsonElement j : jsonArray) {
                            Log.e("david", "everyone = " + j.toString());
                        }
                    }
                });*/



        // = (TextView) findViewById(R.id.tv_title);
        //tv_title.setOnClickListener(this);
        //initTabs();
        initViewPage();
        initPagerAdapter();
        setTabSelection(mTabTVs[0]);

    }

    private void initViewPage() {
        HomeFragment wx = HomeFragment.newInstance("微信 Arguments", null);
        VideoFragment friend = VideoFragment.newInstance("通讯录 Arguments", null);
        PictureFragment find = PictureFragment.newInstance("发现 Arguments", null);
        NewsFragment mine = NewsFragment.newInstance("我 Arguments", null);
        mFragments.add(wx);
        mFragments.add(friend);
        mFragments.add(find);
        mFragments.add(mine);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mTabLayout = (TabLayout) findViewById(R.id.pagertablayout);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                Log.e("david", "position" + position + mTabTVs[position]);
                //setTabSelection(mTabTVs[position]);
            }
            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                Log.e("david", " scroll position" + arg0 + arg1+ arg2);
            }
        });
    }
    private void initTabs() {
        /*mTabTVs[0] = (RadioButton) findViewById(R.id.music);
        mTabTVs[1] = (RadioButton) findViewById(R.id.video);
        mTabTVs[2] = (RadioButton) findViewById(R.id.picture);
        mTabTVs[3] = (RadioButton) findViewById(R.id.news);
        //mTabTVIdsNormal = new int[] { R.drawable.tab_weixin_normal, R.drawable.tab_find_frd_normal, R.drawable.tab_address_normal,
        //        R.drawable.tab_settings_normal };
        //mTabTVIdsPress = new int[] { R.drawable.tab_weixin_pressed, R.drawable.tab_find_frd_pressed, R.drawable.tab_address_pressed,
        //        R.drawable.tab_settings_pressed };
        //给四个控件设置一个Tag，当我们点击某个控件时可以根据这个Tag来识别此控件，当然我们也可以根据v.getid()来识别，但在这里setTag还有其他妙用
        for (int i = 0; i < mTabTVs.length; i++) {
            //mTabTVs[i].setOnClickListener(this);
            mTabTVs[i].setTag(i);
        }*/
    }

    /*@Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.music:
            case R.id.video:
            case R.id.picture:
            case R.id.news:
                setTabSelection(v);
                mViewPager.setCurrentItem((Integer) v.getTag());
                break;
            default:
                break;

    }*/

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public interface GetRequest_Interface {
        //@GET("openapi.do?keyfrom=Yanzhikai&key=2032414398&type=data&doctype=json&version=1.1&q=car")
        //@Url("openapi.do?keyfrom=Yanzhikai&key=2032414398&type=data&doctype=json&version=1.1&q=car")
        //@POST()

        @HTTP(method = "POST", path = "openapi.do?keyfrom=Yanzhikai&key=2032414398&type=data&doctype=json&version=1.1&q=car", hasBody = true)

        //Call<ResponseBody> getCall(@Body Gson gson);
        Observable<ResponseBody> getMessage(@Body Gson gson);
    }

    private void initPagerAdapter() {
        mPagerAdapter = new PagerAdapter() {
            @Override
            public boolean isViewFromObject(View view, Object object) { // 判断是否是同一条目
                return view == object;
            }
            @Override
            public int getCount() {
                return mFragments.size();
            }
            public Object instantiateItem(ViewGroup container, int position) {// 初始化要显示的条目
                ImageView imageView = new ImageView(NetSongActivity.this);
                imageView.setImageResource(R.drawable.splash);
                container.addView(imageView);//一定将要显示的条目加入到ViewGroup的缓存容器中才可以显示
                return imageView;
            }
            public void destroyItem(ViewGroup container, int position, Object object) {//将要销毁的条目从ViewGroup的缓存容器中移除
                container.removeView((View) object);
            }
        };
        mFragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return mFragments.size();
            }
            @Override
            public Fragment getItem(int position) {//只有在【新】生成 Fragment 对象时调用。注意：在调用notifyDataSetChanged() 后不会被调用
                Log.i("bqt", "【getItem】被调用了－" + position);
                return mFragments.get(position);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                //return super.getPageTitle(position);
                switch (position) {
                    case 0:
                        return getResources().getString(R.string.music);
                    case 1:
                        return getResources().getString(R.string.video);
                    case 2:
                        return getResources().getString(R.string.picture);
                    default:
                        return getResources().getString(R.string.music);
                }
            }
        };
        mFragmentStatePagerAdapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return mFragments.size();
            }
            @Override
            public Fragment getItem(int position) {//尼玛尼玛尼玛：在调用notifyDataSetChanged() 后也会被调用啊！
                Log.i("bqt", "【getItem】被调用了－" + position);
                return mFragments.get(position);
            }
        };
        mViewPager.setAdapter(mFragmentPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

    }

    private void setTabSelection(View v) {
        //清除掉所有的选中状态
        /*for (int i = 0; i < mTabTVs.length; i++) {
            //mTabTVs[i].setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.splash, 0, 0);
            mTabTVs[i].setSelected(false);
        }
        // 改变控件的图片，这里的setSelected是为了演示通过selector来改变文字颜色
        int index = (Integer) v.getTag();
        //((TextView) v).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.splash, 0, 0);
        v.setSelected(true);*/
    }
}
