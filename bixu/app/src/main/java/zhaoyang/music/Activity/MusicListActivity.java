package zhaoyang.music.Activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.support.annotation.MainThread;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import zhaoyang.music.Adapter.LocalMusicListAdapter;
import zhaoyang.music.Application.MusicApplication;
import zhaoyang.music.Dao.LocalListDao;
import zhaoyang.music.Dao.imp.LocalListDaoImp;
import zhaoyang.music.Domain.ControlEvent;
import zhaoyang.music.Domain.LocalList;
import zhaoyang.music.Domain.PlayEvent;
import zhaoyang.music.Domain.Song;
import zhaoyang.music.Fragment.HomeFragment;
import zhaoyang.music.IMediaAidlInterface;
import zhaoyang.music.Media.MediaService;
import zhaoyang.music.Media.MusicPlayer;
import zhaoyang.music.R;
import zhaoyang.music.interface_.OnFragmentInteractionListener;
import zhaoyang.music.utils.MusicUtils;

public class MusicListActivity extends FragmentActivity implements LocalMusicListAdapter.MyListener,
        OnFragmentInteractionListener {

    private ImageButton imageButton;
    public ArrayList<LocalList> music_list;
    private ListView local_list;
    private static Handler handler;
    public MusicApplication musicApplication;
    public static MusicPlayer musicPlayer;
    public static MusicUtils musicUtils;
    private static TextView play_time, song_time, display_song;
    private static SeekBar seekbar;
    private ImageButton playButton, preButton, nextButton;
    private ImageButton backButton, playingButton;
    private TextView textView;
    private LocalMusicListAdapter adapter;
    private int from;
    private static Song song;
    private ControlEvent controlEvent;
    private PlayEvent playEvent;
    private LocalListDao localListDao;
    private IBinder binder;
    private MediaService mediaService;
    private ServiceConnection mServiceConnection;
    private MediaReceiver mediaReceiver;
    private MyThread myThread;
    private MultiModeCallback mCallback;

    private static class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            //super.handleMessage(msg);
            //Log.e("david", "handler update");
            if (MusicApplication.mediaService != null) {
                synchronized (MusicApplication.mediaService) {
                    MediaService mediaService = MusicApplication.mediaService;
                    if (mediaService != null ) {
                        Log.e("david", "musicPlayer is not null");
                        try {
                            if (mediaService.isPlaying() || mediaService.isPause()) {
                                seekbar.setProgress(mediaService.getCurrentPosition());
                                play_time.setText(musicUtils.StringformatTime(mediaService.getCurrentPosition()));
                                song_time.setText(musicUtils.StringformatTime(mediaService.getDuration()));
                                seekbar.setMax(mediaService.getDuration());
                                song = new Song();
                                song.setPath(mediaService.getPlayingPath());
                                song.setName(mediaService.getPlayingName());
                                song.setNowTime(mediaService.getCurrentPosition());
                                song.setLongTime(mediaService.getDuration());
                                EventBus.getDefault().postSticky(song);
                                //Log.e("david", Integer.toString(musicPlayer.getCurrentPosition()));
                            }
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                            musicPlayer = null;
                            MusicPlayer.getInstance();
                        }

                    }
                }
            }
        }

        @Override
        public boolean sendMessageAtTime(Message msg, long uptimeMillis) {
            return super.sendMessageAtTime(msg, uptimeMillis);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_list);
        local_list = (ListView)findViewById(R.id.local_music_list);
        Intent intent = getIntent();
        from = intent.getIntExtra("id", 0);
        Log.e("david", "get-id = " + from);
        musicApplication = MusicApplication.getInstance();
        musicPlayer = MusicApplication.musicPlayer;
        musicUtils = MusicApplication.musicUtils;
        mediaService = MusicApplication.mediaService;
        Log.e("david", "musiclist = " + mediaService);
        //mediaService = new MediaService();
        MediaService.TestBinder testBinder = mediaService.new TestBinder();
        mediaService = testBinder.getService();
        if (mediaService != null) {
            Log.e("david", "mediaservice is not null");
        } else {
            Log.e("david", "mediaservice is null");
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("ACTION_PLAY");
        intentFilter.addAction("ACTION_PAUSE");
        mediaReceiver = new MediaReceiver();
        registerReceiver(mediaReceiver, intentFilter);
        initData();
        /*handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                //super.handleMessage(msg);
                //Log.e("david", "handler update");
                if (musicPlayer != null ) {
                    //Log.e("david", "musicPlayer is not null");
                    if (musicPlayer.isPlaying() || musicPlayer.isPause()) {
                        seekbar.setProgress(musicPlayer.getCurrentPosition());
                        play_time.setText(musicUtils.StringformatTime(musicPlayer.getCurrentPosition()));
                    }
                }
            }

            @Override
            public boolean sendMessageAtTime(Message msg, long uptimeMillis) {
                return super.sendMessageAtTime(msg, uptimeMillis);
            }
        };*/
        //setDefaultFragment();
        //static for handler maybe leak
        handler = new MyHandler();
        MyThread updateThread = new MyThread();
        controlEvent = new ControlEvent();
        playEvent = new PlayEvent();
        new Thread(updateThread).start();
        EventBus.getDefault().register(this);

        registerForContextMenu(local_list);
        mCallback = new MultiModeCallback();

        //display action mode
        //local_list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        //local_list.setMultiChoiceModeListener(mCallback);
    }

    public class MultiModeCallback implements AbsListView.MultiChoiceModeListener {

        @Override
        public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {

        }

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            //return false;
            actionMode.getMenuInflater().inflate(R.menu.actionmode, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {

        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        //super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.option, menu);
        menu.setHeaderTitle(R.string.control);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    class MyThread implements Runnable {
        @Override
        public void run() {
            Message msg = new Message();
            //chang thread life, not good
            while(MusicApplication.start){
                handler.sendEmptyMessage(1);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void clickListener() {
        if (mediaService.isPlaying() || mediaService.isPause()) {
            Log.e("david", "click - getDuration" + mediaService.getDuration());
            song_time.setText(musicUtils.StringformatTime(mediaService.getDuration()));
            if (mediaService.isPause()) {
                play_time.setText(musicUtils.StringformatTime(mediaService.getCurrentPosition()));
                seekbar.setProgress(mediaService.getCurrentPosition());
            } else {
                //play_time.setText(musicUtils.StringformatTime(0));
                //seekbar.setProgress(0);
            }
            seekbar.setMax(mediaService.getDuration());
            if (mediaService.isPlaying()) {
                playButton.setBackgroundResource(R.drawable.play_button);
                display_song.setText(mediaService.getPlayingName());
            } else if (mediaService.isPause()) {
                playButton.setBackgroundResource(R.drawable.pause_button);
                display_song.setText(mediaService.getPlayingName());
            }
        }
    }

    public void initData() {

        imageButton = (ImageButton)findViewById(R.id.main_back_button);
        play_time = (TextView)findViewById(R.id.button_play).findViewById(R.id.song_playtime);
        song_time = (TextView)findViewById(R.id.button_play).findViewById(R.id.song_long);
        seekbar = (SeekBar)findViewById(R.id.button_play).findViewById(R.id.seekbar);
        playButton = (ImageButton)findViewById(R.id.button_play).findViewById(R.id.play_or_pause);
        preButton = (ImageButton)findViewById(R.id.button_play).findViewById(R.id.play_pre);
        nextButton = (ImageButton)findViewById(R.id.button_play).findViewById(R.id.play_next);
        display_song = (TextView)findViewById(R.id.button_play).findViewById(R.id.playing_song);
        initConnection();
        /*if (mediaService.isPlaying()) {
            playButton.setBackgroundResource(R.drawable.play_button);
        } else if (mediaService.isPause()) {
            playButton.setBackgroundResource(R.drawable.pause_button);
        }*/

        localListDao = new LocalListDaoImp(getApplicationContext());
        music_list = new ArrayList<LocalList>();
        Log.e("david", "initData from = " + from);
        /*if (0 == from) {
            music_list = (ArrayList)localListDao.getLocalMusicList();
        } else {
            music_list = (ArrayList)localListDao.getMusicListByPlayListId(from);
        }*/
        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception {
                if (0 == from) {
                    music_list = (ArrayList)localListDao.getLocalMusicList();
                } else {
                    music_list = (ArrayList)localListDao.getMusicListByPlayListId(from);
                    if (mediaService != null) {
                        if (mediaService.isPlaying() || mediaService.isPause()) {
                            display_song.setText(mediaService.getPlayingName());
                        }
                    }
                }
                e.onNext(music_list);
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object o) throws Exception {
                adapter = new LocalMusicListAdapter(MusicListActivity.this, music_list,
                        MusicListActivity.this, mediaService);
                local_list.setAdapter(adapter);
                initseekAndbottom();
                initTopBar();
            }
        });
        /*local_list = (ListView)findViewById(R.id.local_music_list);
        adapter = new LocalMusicListAdapter(this, music_list, this);
        local_list.setAdapter(adapter);
        initseekAndbottom();
        initTopBar();*/

    }

    public void initseekAndbottom() {
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            boolean pause = mediaService.isPause();
            int position;
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                //must add if ,otherwise will kadun
                if (b) {
                    position = i;
                    //seekBar.setProgress(i);
                    //if not seekto, it will go back when not stop touch
                    mediaService.seekTo(position);
                    play_time.setText(musicUtils.StringformatTime(i));

                }
            }

            @Override

            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //Log.e("david", "status1 : " + musicPlayer.isPause() + musicPlayer.isPlaying());
                mediaService.seekTo(position);
                Log.e("david", "status2 : " + musicPlayer.isPause() + musicPlayer.isPlaying());
                //musicPlayer.pause();

            }
        });

        preButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (music_list.size() == 0){
                    return;
                }
                mediaService.reset();
                adapter.previous();
                playButton.setBackgroundResource(R.drawable.play_button);
                adapter.notifyDataSetChanged();
                display_song.setText(mediaService.getPlayingName());
                controlEvent.setStatus(ControlEvent.PREVIOUS);
                EventBus.getDefault().postSticky(controlEvent);
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (music_list.size() == 0){
                    return;
                }
                mediaService.reset();
                //Log.e("david", "-- next");
                adapter.next();
                playButton.setBackgroundResource(R.drawable.play_button);
                adapter.notifyDataSetChanged();
                display_song.setText(mediaService.getPlayingName());
                controlEvent.setStatus(ControlEvent.NEXT);
                EventBus.getDefault().postSticky(controlEvent);
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.e("david", "playButton is click");
                if (music_list.size() == 0){
                    return;
                }
                if (mediaService.isPlaying()) {
                    mediaService.pause();
                    playButton.setBackgroundResource(R.drawable.pause_button);
                    controlEvent.setStatus(ControlEvent.PAUSE);
                    EventBus.getDefault().postSticky(controlEvent);
                } else if (mediaService.isPause()) {
                    mediaService.start();
                    playButton.setBackgroundResource(R.drawable.play_button);
                    controlEvent.setStatus(ControlEvent.PLAY);
                    EventBus.getDefault().postSticky(controlEvent);
                }
                else {
                    adapter.playFirst();
                    display_song.setText(mediaService.getPlayingName());
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void initTopBar() {
        backButton = (ImageButton)findViewById(R.id.top_bar).findViewById(R.id.main_back_button);
        textView = (TextView)findViewById(R.id.top_bar).findViewById(R.id.play_list);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MusicListActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        playingButton = (ImageButton)findViewById(R.id.top_bar).findViewById(R.id.play_now_button);
        playingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //nowSong();
                //EventBus.getDefault().postSticky(song);
                Intent intent = new Intent(MusicListActivity.this, PlayActivity.class);
                startActivity(intent);
            }
        });
    }

    public void nowSong() {
        song = new Song();
        song.setPath(mediaService.getPlayingPath());
        song.setName(mediaService.getPlayingName());
        song.setNowTime(mediaService.getCurrentPosition());
        song.setLongTime(mediaService.getDuration());
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //moveTaskToBack(true);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("david", "destory");
        unbindService(mServiceConnection);
        unregisterReceiver(mediaReceiver);
        EventBus.getDefault().unregister(this);
        unregisterForContextMenu(local_list);
    }

    @Subscribe(threadMode = ThreadMode.ASYNC, sticky = true, priority = 1)
    public void onControlEvent(ControlEvent controlEvent) {}

    @Subscribe (threadMode = ThreadMode.POSTING, sticky = true, priority = 1)
    public void onEvent(PlayEvent playEvent) {
        //playingButton.setBackgroundResource(R.drawable.splash);
        int i = playEvent.getStatus();
        switch (i) {
            case 0:
                //mediaService.start();
                playButton.setBackgroundResource(R.drawable.play_button);
                adapter.notifyDataSetChanged();
                break;
            case 1:
                //mediaService.pause();
                playButton.setBackgroundResource(R.drawable.pause_button);
                adapter.notifyDataSetChanged();
                break;
            case 2:
                mediaService.reset();
                adapter.previous();
                display_song.setText(mediaService.getPlayingName());
                playButton.setBackgroundResource(R.drawable.play_button);
                adapter.notifyDataSetChanged();
                break;
            case 3:
                mediaService.reset();
                //Log.e("david", "event -- next");
                adapter.next();
                display_song.setText(mediaService.getPlayingName());
                playButton.setBackgroundResource(R.drawable.play_button);
                adapter.notifyDataSetChanged();
                break;
            case 4:
                adapter.playFirst();
                display_song.setText(mediaService.getPlayingName());
                adapter.notifyDataSetChanged();
            default:
                break;
        }
    }
    private void initConnection() {
        //mediaService = new MediaService();
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                //mediaService = MediaService.getInstance();
                mediaService = ((MediaService.TestBinder)service).getService();
                if (mediaService == null) {
                    new AlertDialog.Builder(new ContextThemeWrapper(getApplicationContext(), android.R.style.Theme_Holo_Dialog))
                            .setMessage("MediaService fail")
                            .setCancelable(true)
                            .show();
                    finish();
                }
            }
        };

        Intent intent = new Intent(getApplicationContext(), MediaService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }
    public class MediaReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("david", "receive notification" + intent.getAction());
            if ("ACTION_PLAY".equals(intent.getAction())) {
                Log.e("david" , "receive play");
                if (mediaService.isPause()) {
                    mediaService.start();
                }
                playButton.setBackgroundResource(R.drawable.play_button);
                controlEvent.setStatus(ControlEvent.PLAY);
                EventBus.getDefault().postSticky(controlEvent);
                adapter.notifyDataSetChanged();
            } else if ("ACTION_PAUSE".equals(intent.getAction())) {
                Log.e("david" , "receive pause");
                if (mediaService.isPlaying()) {
                    mediaService.pause();
                }
                playButton.setBackgroundResource(R.drawable.pause_button);
                controlEvent.setStatus(ControlEvent.PAUSE);
                EventBus.getDefault().postSticky(controlEvent);
                adapter.notifyDataSetChanged();
            }

        }
    }

    @Override
    protected void onResume() {
        Log.e("david", "resume");
        super.onResume();
    }
}
