package zhaoyang.music.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Movie;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.BoringLayout;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import java.io.ByteArrayOutputStream;
import java.security.PrivilegedAction;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageColorDodgeBlendFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageExposureFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageGrayscaleFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSepiaFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSharpenFilter;
import uk.co.senab.photoview.DefaultOnDoubleTapListener;
import uk.co.senab.photoview.PhotoView;
import zhaoyang.music.R;
import zhaoyang.music.utils.FileUtils;

public class PictureActivity extends AppCompatActivity {
    private TextView topText;
    private ImageButton now;
    private static SurfaceHolder msurfaceHolder;
    private static String path, name, thum;
    private static PictureSurfaceView pictureSurfaceView;
    private SurfaceHolder.Callback callback;
    private static int wmWidth, wmHeight, picWidth, picHeight;
    private static Movie movie;
    private static Handler handler;
    private static Canvas canvas;
    private static boolean stop;
    private static float x, y;
    private static float bottomx, bottomy;
    private ScaleGestureDetector scaleGestureDetector;
    private static Bitmap bitmap;
    private GPUImage gpuImage;
    private Button grayButton, sharpButton, edgeButton, sepiaButton;
    private static int  change = 0;
    private float scale;
    private float preScale = 0;
    private static boolean draw;
    private PhotoView photoView;
    private PhotoViewAttacher mAttacher;

    private static GifRunnable run;
    private class GifRunnable implements Runnable {
        @Override
        public void run() {
            Log.e("david", "picture run");
            synchronized (PictureActivity.class) {
                if (draw) {
                    canvas = msurfaceHolder.lockCanvas();
                    if (canvas == null) {
                        return;
                    }
                    canvas.save();
                    Log.e("david" , "x=" + x + "y=" + y);
                    movie.draw(canvas, x, y);

                    canvas.restore();
                    Log.e("david", "currentTime = " + System.currentTimeMillis() + "dur = " + movie.duration());

                    movie.setTime((int) (System.currentTimeMillis() % movie.duration()));
                    msurfaceHolder.unlockCanvasAndPost(canvas);
                }
            }

            handler.removeCallbacks(run);
            if (stop) {
                handler.postDelayed(run, 30);
            }
        }

        public void stop() {
            stop = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        stop = true;
        run = new GifRunnable();
        scaleGestureDetector = new ScaleGestureDetector(this, new ScaleGetector());
        Log.e("david", "pictureActivity oncreate 0");
        super.onCreate(savedInstanceState);
        Log.e("david", "pictureActivity oncreate 1");
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        // enable status bar tint
        tintManager.setStatusBarTintEnabled(true);
        // enable navigation bar tint
        tintManager.setNavigationBarTintEnabled(true);
        // set a custom tint color for all system bars
        //tintManager.setStatusBarTintColor(Color.TRANSPARENT);
        // set a custom navigation bar resource
        //tintManager.setNavigationBarTintResource(R.drawable.my_tint);
        // set a custom status bar drawable
        //tintManager.setStatusBarTintDrawable(MyDrawable);
        tintManager.setStatusBarAlpha(255);
        setContentView(R.layout.activity_picture);
        handler = new Handler();
        Log.e("david", "pictureActivity oncreate 3");
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
        initBitmapMerics();
        Log.e("david", "pictureActivity oncreate 4");
    }

    public void init() {
        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        wmWidth = displayMetrics.widthPixels;
        wmHeight = displayMetrics.heightPixels;
        //topText = (TextView)findViewById(R.id.picture_top).findViewById(R.id.play_list);
        //now = (ImageButton)findViewById(R.id.picture_top).findViewById(R.id.play_now_button);
        pictureSurfaceView = (PictureSurfaceView)findViewById(R.id.surface_picture);
        grayButton = (Button)findViewById(R.id.gray_button);
        sharpButton = (Button)findViewById(R.id.sharp_button);
        edgeButton = (Button)findViewById(R.id.edge_button);
        sepiaButton = (Button)findViewById(R.id.sepia_button);
        photoView = (PhotoView)findViewById(R.id.photoview);
        Intent intent = getIntent();
        path = intent.getStringExtra("data");
        name = intent.getStringExtra("name");
        thum = intent.getStringExtra("bitmap");
        //topText.setText(name);
        //now.setVisibility(View.GONE);
        msurfaceHolder = pictureSurfaceView.getHolder();
        msurfaceHolder.addCallback(pictureSurfaceView);
        pictureSurfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Log.e("david", "touch view");
                return scaleGestureDetector.onTouchEvent(motionEvent);
                //return false;
            }
        });
        gpuImage = new GPUImage(this);
        mAttacher = new PhotoViewAttacher(photoView);
        Glide.with(this).load(path).into(photoView);
        mAttacher.setMaximumScale(5f);
        mAttacher.setMinimumScale(0.3f);
        mAttacher.update();
    }

    private void click() {
        grayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                change = 1;
                preScale = 0;
                String type = FileUtils.getType(path);
                Log.e("david", "picture type = " + type);
                if ("gif".equals(type)) {
                    showDialog();
                    return;
                }
                Bitmap b = photoView.getVisibleRectangleBitmap();
                gpuImage.setImage(b);
                gpuImage.setFilter(new GPUImageGrayscaleFilter());
                b = gpuImage.getBitmapWithFilterApplied();
                //changeDraw(b);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                b.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] bytes=baos.toByteArray();
                Glide.with(getApplicationContext()).load(bytes).into(photoView);
            }
        });
        sharpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                change = 2;
                preScale = 0;
                String type = FileUtils.getType(path);
                Log.e("david", "picture type = " + type);
                if ("gif".equals(type)) {
                    showDialog();
                    return;
                }
                Bitmap b = photoView.getVisibleRectangleBitmap();
                gpuImage.setImage(b);
                gpuImage.setFilter(new GPUImageColorDodgeBlendFilter());
                b = gpuImage.getBitmapWithFilterApplied();
                //changeDraw(b);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                b.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] bytes=baos.toByteArray();
                Glide.with(getApplicationContext()).load(bytes).into(photoView);
            }
        });
        edgeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                change = 3;
                preScale = 0;
                String type = FileUtils.getType(path);
                Log.e("david", "picture type = " + type);
                if ("gif".equals(type)) {
                    showDialog();
                    return;
                }
                Bitmap b = photoView.getVisibleRectangleBitmap();
                gpuImage.setImage(b);
                gpuImage.setFilter(new GPUImageExposureFilter());
                b = gpuImage.getBitmapWithFilterApplied();
                //changeDraw(b);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                b.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] bytes=baos.toByteArray();
                Glide.with(getApplicationContext()).load(bytes).into(photoView);
            }
        });
        sepiaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                change = 4;
                preScale = 0;
                String type = FileUtils.getType(path);
                Log.e("david", "picture type = " + type);
                if ("gif".equals(type)) {
                    showDialog();
                    return;
                }
                Bitmap b = photoView.getVisibleRectangleBitmap();
                gpuImage.setImage(b);
                gpuImage.setFilter(new GPUImageSepiaFilter());
                b = gpuImage.getBitmapWithFilterApplied();
                //changeDraw(b);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                b.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] bytes=baos.toByteArray();
                Glide.with(getApplicationContext()).load(bytes).into(photoView);
            }
        });
    }

    private void showDialog() {
        TextView title = new TextView(PictureActivity.this);
        title.setText("Don't support gif!");
        title.setPadding(10, 10, 10, 10);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.WHITE);
        title.setTextSize(23);
        new AlertDialog.Builder(new ContextThemeWrapper(PictureActivity.this, android.R.style.Theme_Holo_Dialog))
                .setCustomTitle(title)
                .setCancelable(true)
                .create()
                .show();
    }

    public void initBitmapMerics() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        Log.e("david", "path = " + path);
        picWidth = options.outWidth;
        picHeight = options.outHeight;
        click();
    }

    public static class PictureSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

        public PictureSurfaceView(Context context) {
            super(context);
        }

        public PictureSurfaceView(Context context, AttributeSet attrs) {
            super(context, attrs);
            msurfaceHolder = getHolder();
        }

        public PictureSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            msurfaceHolder = getHolder();
        }

        public PictureSurfaceView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
            super(context, attrs, defStyleAttr, defStyleRes);
            msurfaceHolder = getHolder();
        }

        @Override
        public boolean performClick() {
            return super.performClick();
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            //super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            int width = 0;
            int height = 0;
            int mode = MeasureSpec.UNSPECIFIED;
            mode = MeasureSpec.getMode(widthMeasureSpec);
            if (mode == MeasureSpec.AT_MOST) {
                width = MeasureSpec.getSize(widthMeasureSpec);
            } else if (mode == MeasureSpec.EXACTLY) {
                width = widthMeasureSpec;
            } else if (mode == MeasureSpec.UNSPECIFIED) {
                Log.e("david", "picWidth = " + picWidth);
                //width = picWidth;
                //width = wmWidth;
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    //width = picWidth;
                    if (wmWidth > wmHeight) {
                        width = wmWidth;
                    } else {
                        width = wmHeight;
                    }
                } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    //width = wmWidth;
                    if (wmWidth > wmHeight) {
                        width = wmHeight;
                    } else {
                        width = wmWidth;
                    }
                }
                //width = Math.max(width, picWidth);
            }
            mode = MeasureSpec.getMode(heightMeasureSpec);
            if (mode == MeasureSpec.AT_MOST) {
                height = MeasureSpec.getSize(heightMeasureSpec);
            } else if (mode == MeasureSpec.EXACTLY) {
                height = heightMeasureSpec;
            } else if (mode == MeasureSpec.UNSPECIFIED) {
                //height = picHeight;
                //height = wmHeight;
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    //height = picHeight;
                    if (wmWidth > wmHeight) {
                        height = wmHeight;
                    } else {
                        height = wmWidth;
                    }
                } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    //height = wmHeight;
                    if (wmWidth > wmHeight) {
                        height = wmWidth;
                    } else {
                        height = wmHeight;
                    }
                }
                //height = Math.max(height, picHeight);
                Log.e("david", "picHeight = " + picHeight);
            }

            setMeasuredDimension(width, height);
        }

        @Override
        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            draw = true;
        }

        @Override
        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
            Log.e("david", "surface change" + i + "/" + i1 + "/" + i2);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                        //setContentView(R.layout.play_land);
                        //initData();
                        Log.e("david", "surface change" + wmWidth + "/" + wmHeight);
                        //drawBitmap(wmHeight, wmWidth);
                        if (wmWidth < wmHeight) {
                            drawBitmap(wmHeight, wmWidth);
                        } else {
                            drawBitmap(wmWidth, wmHeight);
                        }
                    } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                        //setContentView(R.layout.activity_play);
                        //initData();
                        //drawBitmap(wmWidth, wmHeight);
                        if (wmWidth < wmHeight) {
                            drawBitmap(wmWidth, wmHeight);
                        } else {
                            drawBitmap(wmHeight, wmWidth);
                        }
                    }
                    Log.e("david", "surface change" + wmWidth + "/" + wmHeight);
                }
            }).start();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            Log.e("david", "surface destory");
            synchronized (PictureActivity.class) {
                draw = false;
            }

        }
    }

    public void changeDraw(final Bitmap bitmap) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    //setContentView(R.layout.play_land);
                    //initData();
                    Log.e("david", "surface change" + wmWidth + "/" + wmHeight);
                    //drawBitmap(wmHeight, wmWidth);
                    if (wmWidth < wmHeight) {
                        drawChangeBitmap(wmHeight, wmWidth, bitmap);
                    } else {
                        drawChangeBitmap(wmWidth, wmHeight, bitmap);
                    }
                } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    //setContentView(R.layout.activity_play);
                    //initData();
                    //drawBitmap(wmWidth, wmHeight);
                    if (wmWidth < wmHeight) {
                        drawChangeBitmap(wmWidth, wmHeight, bitmap);
                    } else {
                        drawChangeBitmap(wmHeight, wmWidth, bitmap);
                    }
                }
                Log.e("david", "surface change" + wmWidth + "/" + wmHeight);
            }
        }).start();
    }

    public void drawChangeBitmap(int w, int h, Bitmap bitm) {
        int scale = 1;
        if (msurfaceHolder == null) {
            return;
        }
        BitmapFactory.Options scaleOptions = new BitmapFactory.Options();
        Rect rect;
        if (h < picHeight && w >= picWidth) {
            Log.e("david", "drawbitmap 1");
            //rect = new Rect(w/2 - picWidth/2, 0, w/2 + picWidth/2, picHeight);
            //x = w/2 - picWidth/2;
            //y = 0;
            int newW, newH;
            newH = h;
            newW = (int)((h * picWidth)/(float)picHeight);
            rect = new Rect(w/2 - newW/2, h/2 - newH/2,
                    w/2 + newW/2, h/2 + newH/2);
            x = w/2 - newW/2;
            y = h/2 - newH/2;
            bottomx = w/2 + newW/2;
            bottomy = h/2 + newH/2;
        } else if (h < picHeight && w < picWidth) {
            Log.e("david", "drawbitmap 2");
            int oldw = picWidth;
            int oldh = picHeight;
            while (oldw > w || oldh > h) {
                oldw /= 2;
                oldh /= 2;
                scale += 1;
            }
            Log.e("david", "scale" + scale);
            scaleOptions.inSampleSize = scale;
            int newW, newH;
            newW = (int)((h * picWidth)/(float)picHeight);
            newH = (int)((w * picHeight)/(float)picWidth);
            if (newH <= h) {
                newW = w;
            } else {
                newH = h;
            }
            //rect = new Rect(0, 0, picWidth, picHeight);
            //rect = new Rect(0, 0, newW, newH);
            //x = 0;
            //y = 0;
            rect = new Rect(w/2 - newW/2, h/2 - newH/2,
                    w/2 + newW/2, h/2 + newH/2);
            x = w/2 - newW/2;
            y = h/2 - newH/2;
            bottomx = w/2 + newW/2;
            bottomy = h/2 + newH/2;
        } else if (h >= picHeight && w < picWidth) {
            Log.e("david", "drawbitmap 3");
            //rect = new Rect(0, h/2 - picHeight/2, picWidth, h/2 + picHeight/2);
            //x = 0;
            //y = h/2 - picHeight/2;
            int newW, newH;
            newW = w;
            newH = (int)((w * picHeight)/(float)picWidth);
            rect = new Rect(w/2 - newW/2, h/2 - newH/2,
                    w/2 + newW/2, h/2 + newH/2);
            x = w/2 - newW/2;
            y = h/2 - newH/2;
            bottomx = w/2 + newW/2;
            bottomy = h/2 + newH/2;
        } else {
            Log.e("david", "drawbitmap 4");
            rect = new Rect(w/2 - picWidth/2, h/2 - picHeight/2,
                    w/2 + picWidth/2, h/2 + picHeight/2);
            x = w/2 - picWidth/2;
            y = h/2 - picHeight/2;
            bottomx = w/2 + picWidth/2;
            bottomy = h/2 + picHeight/2;
        }

        synchronized (PictureActivity.class) {
            if (draw) {
                canvas = msurfaceHolder.lockCanvas();
                Log.e("david", "bitmap decode");
                //Bitmap bitmap1 = bitm;
                if (bitm != null) {
                    Paint paint = new Paint();
                    paint.setAntiAlias(true);
                    paint.setStyle(Paint.Style.FILL);

                    Log.e("david", "draw begin");
                    canvas.drawColor(Color.BLACK);
                    canvas.drawBitmap(bitm, null, rect, paint);
                    Log.e("david", "draw end");
                }
                msurfaceHolder.unlockCanvasAndPost(canvas);
            }

        }

        //if (bitmap1 != null) {
        //    bitmap1.recycle();
        //}
    }

    public static void drawBitmap(int w, int h) {
        String type = FileUtils.getType(path);
        Log.e("david", "picture type = " + type);
        int scale = 1;
        if (msurfaceHolder == null) {
            return;
        }
        BitmapFactory.Options scaleOptions = new BitmapFactory.Options();
        Rect rect;
        if (h < picHeight && w >= picWidth) {
            Log.e("david", "drawbitmap 1");
            //rect = new Rect(w/2 - picWidth/2, 0, w/2 + picWidth/2, picHeight);
            //x = w/2 - picWidth/2;
            //y = 0;
            int newW, newH;
            newH = h;
            newW = (int)((h * picWidth)/(float)picHeight);
            rect = new Rect(w/2 - newW/2, h/2 - newH/2,
                    w/2 + newW/2, h/2 + newH/2);
            x = w/2 - newW/2;
            y = h/2 - newH/2;
            bottomx = w/2 + newW/2;
            bottomy = h/2 + newH/2;
        } else if (h < picHeight && w < picWidth) {
            Log.e("david", "drawbitmap 2");
            int oldw = picWidth;
            int oldh = picHeight;
            while (oldw > w || oldh > h) {
                oldw /= 2;
                oldh /= 2;
                scale += 1;
            }
            Log.e("david", "scale" + scale);
            scaleOptions.inSampleSize = scale;
            int newW, newH;
            newW = (int)((h * picWidth)/(float)picHeight);
            newH = (int)((w * picHeight)/(float)picWidth);
            if (newH <= h) {
                newW = w;
            } else {
                newH = h;
            }
            //rect = new Rect(0, 0, picWidth, picHeight);
            //rect = new Rect(0, 0, newW, newH);
            //x = 0;
            //y = 0;
            rect = new Rect(w/2 - newW/2, h/2 - newH/2,
                    w/2 + newW/2, h/2 + newH/2);
            x = w/2 - newW/2;
            y = h/2 - newH/2;
            bottomx = w/2 + newW/2;
            bottomy = h/2 + newH/2;
        } else if (h >= picHeight && w < picWidth) {
            Log.e("david", "drawbitmap 3");
            //rect = new Rect(0, h/2 - picHeight/2, picWidth, h/2 + picHeight/2);
            //x = 0;
            //y = h/2 - picHeight/2;
            int newW, newH;
            newW = w;
            newH = (int)((w * picHeight)/(float)picWidth);
            rect = new Rect(w/2 - newW/2, h/2 - newH/2,
                    w/2 + newW/2, h/2 + newH/2);
            x = w/2 - newW/2;
            y = h/2 - newH/2;
            bottomx = w/2 + newW/2;
            bottomy = h/2 + newH/2;
        } else {
            Log.e("david", "drawbitmap 4");
            rect = new Rect(w/2 - picWidth/2, h/2 - picHeight/2,
                    w/2 + picWidth/2, h/2 + picHeight/2);
            x = w/2 - picWidth/2;
            y = h/2 - picHeight/2;
            bottomx = w/2 + picWidth/2;
            bottomy = h/2 + picHeight/2;
        }
        if ("gif".equals(type)) {
            Log.e("david", "picture gif ");
            movie = Movie.decodeFile(path);
            /*if (movie != null) {
                Log.e("david", "picture gif " + movie.toString());
                Paint paint = new Paint();
                paint.setAntiAlias(true);
                paint.setStyle(Paint.Style.FILL);
                movie.draw(canvas, 0, 0, paint);
                movie.setTime((int) (System.currentTimeMillis() % movie.duration()));
            }
            msurfaceHolder.unlockCanvasAndPost(canvas);*/
            handler.post(run);

            //unlock not   oom
            Log.e("david", "gif unlock");

        } else {
            synchronized (PictureActivity.class) {
                if (draw) {
                    canvas = msurfaceHolder.lockCanvas();
                    Log.e("david", "bitmap decode");
                    bitmap = BitmapFactory.decodeFile(path, scaleOptions);
                    if (bitmap != null) {
                        Paint paint = new Paint();
                        paint.setAntiAlias(true);
                        paint.setStyle(Paint.Style.FILL);
                        Log.e("david", "draw begin");
                        canvas.drawBitmap(bitmap, null, rect, paint);
                        Log.e("david", "draw end");
                    }
                    msurfaceHolder.unlockCanvasAndPost(canvas);
                }

            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //setContentView(R.layout.play_land);
            //initData();
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            //setContentView(R.layout.activity_play);
            //initData();
        }
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onStop() {
        super.onStop();

        //if (msurfaceHolder.lockCanvas())
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        run.stop();
        if (bitmap != null) {
            bitmap.recycle();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //return super.onTouchEvent(event);
        Log.e("david", "onTouch");
        return scaleGestureDetector.onTouchEvent(event);
    }

    public class ScaleGetector implements ScaleGestureDetector.OnScaleGestureListener {
        private float tx, ty, bx, by;
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            Log.e("david", "onScale");
            float preSpan = scaleGestureDetector.getPreviousSpan();
            float curSpan = scaleGestureDetector.getCurrentSpan();
            float distance = preSpan - curSpan;
            final Rect rect;
            if (Math.abs(distance) > 0) {
                if (preSpan < curSpan) {
                    //small
                    //scale = preScale - (preSpan - curSpan)/1000;
                    scale = preScale - 1;
                    tx = x + scale * 50;
                    ty = y + scale *50;
                    bx = bottomx - scale *50;
                    by = bottomy - scale *50;
                    rect = new Rect((int)tx, (int)ty, (int)bx, (int)by);

                } else {
                    //big
                    //scale = preScale + (curSpan - preSpan)/1000;
                    scale = preScale + 1;
                    tx = x + scale * 50;
                    ty = y + scale *50;
                    bx = bottomx - scale *50;
                    by = bottomy - scale *50;
                    rect = new Rect((int)tx, (int)ty, (int)bx, (int)by);
                }
                final Matrix matrix = new Matrix();
                Log.e("david", "scale = " + scale);
                matrix.preScale(scale, scale);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (PictureActivity.class) {
                            if (draw) {
                                Canvas mCanvas = msurfaceHolder.lockCanvas();
                                // clear
                                if (mCanvas != null) {
                                    mCanvas.drawColor(Color.BLACK);
                                    // draw
                                    //mCanvas.drawBitmap(bitmap, matrix, null);
                                    Bitmap b = null;
                                    switch (change) {
                                        case 1:
                                            Log.e("david", "scale begin");
                                            gpuImage.setImage(bitmap);
                                            gpuImage.setFilter(new GPUImageGrayscaleFilter());
                                            b = gpuImage.getBitmapWithFilterApplied();
                                            mCanvas.drawBitmap(b, null, rect, null);
                                            Log.e("david", "scale end");
                                            break;
                                        case 2:
                                            gpuImage.setImage(bitmap);
                                            gpuImage.setFilter(new GPUImageColorDodgeBlendFilter());
                                            b = gpuImage.getBitmapWithFilterApplied();
                                            mCanvas.drawBitmap(b, null, rect, null);
                                            break;
                                        case 3:
                                            gpuImage.setImage(bitmap);
                                            gpuImage.setFilter(new GPUImageExposureFilter());
                                            b = gpuImage.getBitmapWithFilterApplied();
                                            mCanvas.drawBitmap(b, null, rect, null);
                                            break;
                                        case 4:
                                            gpuImage.setImage(bitmap);
                                            gpuImage.setFilter(new GPUImageSepiaFilter());
                                            b = gpuImage.getBitmapWithFilterApplied();
                                            mCanvas.drawBitmap(b, null, rect, null);
                                            break;
                                        default:
                                            mCanvas.drawBitmap(bitmap, null, rect, null);
                                            break;
                                    }
                                    //mCanvas.drawBitmap(bitmap, null, rect, null);
                                    // post
                                    msurfaceHolder.unlockCanvasAndPost(mCanvas);
                                    if (b != null) {
                                        b.recycle();
                                    }
                                }

                                // again
                                msurfaceHolder.lockCanvas(new Rect(0, 0, 0, 0));
                                msurfaceHolder.unlockCanvasAndPost(mCanvas);
                            }
                        }

                        Log.e("david", "surface change" + wmWidth + "/" + wmHeight);
                    }
                }).start();
            }

            return false;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
            Log.e("david", "onScaleBegin");
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
            preScale = scale;
        }
    }
}
