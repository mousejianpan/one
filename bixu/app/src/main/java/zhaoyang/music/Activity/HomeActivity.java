package zhaoyang.music.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import android.support.design.internal.BottomNavigationMenu;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.view.menu.MenuBuilder;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.List;

import zhaoyang.music.Application.MusicApplication;
import zhaoyang.music.Dao.PlayListDao;
import zhaoyang.music.Dao.imp.PlayListDaoImp;
import zhaoyang.music.Domain.PlayList;
import zhaoyang.music.Fragment.AddListFragment;
import zhaoyang.music.Fragment.LocalFragment;
import zhaoyang.music.Fragment.NewsFragment;
import zhaoyang.music.Fragment.PictureFragment;
import zhaoyang.music.Fragment.VideoFragment;
import zhaoyang.music.Fragment.HomeFragment;
import zhaoyang.music.Media.MediaService;
import zhaoyang.music.R;
import zhaoyang.music.interface_.OnFragmentInteractionListener;
import zhaoyang.music.Bean.Tab;
import zhaoyang.music.utils.RequestPermission;

import static android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP;

public class HomeActivity extends FragmentActivity implements OnFragmentInteractionListener {
    private Context mContext = HomeActivity.this;
    private FragmentTabHost mTabHost ;
    private LayoutInflater mInflater ;
    private List<Tab> mTabs = new ArrayList<>();
    private Tab tab_home, tab_address, tab_find_friend, tab_setting;
    private AddListFragment listFragment;
    private HomeFragment homeFragment;
    private LocalFragment localFragment;
    private NewsFragment newsFragment;
    private VideoFragment videoFragment;
    private PictureFragment pictureFragment;
    private ImageButton backButton, playButton;
    private MyReceiver myReceiver;
    private TextView textView;
    private FragmentManager fm;
    private RequestPermission requestPermission;
    private MediaService mediaService;
    private ServiceConnection mServiceConnection;
    private int lastTab;
    private Fragment currentFragment;
    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;
    private ViewFragmentPagerAdapter mFragmentPagerAdapter;
    private PagerAdapter mFragmentStatePagerAdapter;
    private List<Fragment> mFragments;
    private TabLayout mTabLayout;
    private boolean blag=false;//标记现在切换显示的是哪个fragment
    private ViewGroup viewGroup;
    private String firstTag;
    private Button settingButton, exitButton;
    private ImageView drawerImageView;
    private TextView drawerTextView;
    private String photo_path, user_name;
    private SharedPreferences sharedPreferences;
    private static int REQUEST_CODE = 1024;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 42;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 24;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //requestPermission = new RequestPermission(getApplicationContext());
        //requestPermission.requestPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
        //        RequestPermission.CODE_FOR_WRITE_PERMISSION);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("ACTION_OPEN");
        registerReceiver(myReceiver, intentFilter);
        mFragments = new ArrayList<Fragment>();
        homeFragment = HomeFragment.newInstance(null, null);
        //localFragment = new LocalFragment();
        newsFragment = NewsFragment.newInstance(null, null);
        videoFragment = VideoFragment.newInstance(null, null);
        pictureFragment = PictureFragment.newInstance(null, null);
        currentFragment = new Fragment();
        fm = getSupportFragmentManager();
        listFragment = new AddListFragment();
        drawerTextView = (TextView)findViewById(R.id.drawer_name);
        drawerImageView = (ImageView)findViewById(R.id.drawer_image);
        settingButton = (Button)findViewById(R.id.drawer_setting);
        exitButton = (Button)findViewById(R.id.drawer_exit);
        textView = (TextView) findViewById(R.id.top_bar).findViewById(R.id.play_list);
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            return;
        }
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            return;
        }
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        //initTab();
        initTopBar();
        initConnection();
        initViewPage();
        initPagerAdapter();
        initDrawer();
    }

    public void initDrawer() {
        sharedPreferences = getSharedPreferences("user_photo", MODE_PRIVATE);
        photo_path = sharedPreferences.getString("path", null);
        user_name = sharedPreferences.getString("name", null);
        if (photo_path == null) {
            Glide.with(this)
                    .load(R.drawable.ic_loading)
                    .into(drawerImageView);
        } else {
            Glide.with(this)
                    .load(photo_path)
                    .into(drawerImageView);
        }
        if (user_name != null) {
            drawerTextView.setText(user_name);
        } else {
            drawerTextView.setText("your name");
        }
        drawerImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //open select picture
                photo_path = sharedPreferences.getString("path", "null");
                Log.e("david", "path =" + photo_path );
                Intent intent = new Intent(HomeActivity.this, PickPhotoActivity.class);
                intent.putExtra("path", photo_path);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
        drawerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 =
                        new AlertDialog.Builder(new ContextThemeWrapper(HomeActivity.this, android.R.style.Theme_Holo_Dialog));
                final EditText editText1 = new EditText(getApplicationContext());
                editText1.setTextColor(Color.WHITE);
                builder1.setTitle(getResources().getString(R.string.rename)).setView(editText1);
                builder1.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).setPositiveButton(getResources().getString(R.string.sure), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        drawerTextView.setText(editText1.getText());
                        sharedPreferences.edit()
                                .putString("name", editText1.getText().toString()).apply();
                        dialogInterface.dismiss();
                    }
                });
                builder1.show();
            }
        });
        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //setting activity
            }
        });
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String path = data.getExtras().getString("path");
                if (path != null) {
                    drawerImageView.setBackgroundColor(Color.parseColor("#DEDEDE"));
                    Glide.with(this).load(path)
                            .into(drawerImageView);
                    photo_path = path;
                    sharedPreferences.edit().putString("path", photo_path).apply();
                }
            } else if (resultCode == RESULT_CANCELED) {
                //not sucess
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        //super.onCreateContextMenu(menu, v, menuInfo);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    finish();
                    return;
                }
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    finish();
                    return;
                }
                initTopBar();
                initConnection();
                initViewPage();
                initPagerAdapter();
                break;
            default:
                break;
        }
    }

    private void initViewPage() {
        textView.setText(R.string.music);
        mFragments.add(homeFragment);
        mFragments.add(videoFragment);
        mFragments.add(pictureFragment);
        mFragments.add(newsFragment);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mTabLayout = (TabLayout) findViewById(R.id.pagertablayout);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                mViewPager.clearAnimation();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                //setTabSelection(mTabTVs[position]);
                switch(position) {
                    case 0:
                        textView.setText(R.string.music);
                        break;
                    case 1:
                        textView.setText(R.string.video);
                        break;
                    case 2:
                        textView.setText(R.string.picture);
                        break;
                    case 3:
                        textView.setText(R.string.news);
                        break;
                    default:
                        break;
                }
            }
            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                //Log.e("david", " scroll position" + arg0 + arg1+ arg2);
            }
        });
    }

    private void initPagerAdapter() {
        mPagerAdapter = new PagerAdapter() {
            @Override
            public boolean isViewFromObject(View view, Object object) { // 判断是否是同一条目
                return view == object;
            }
            @Override
            public int getCount() {
                return mFragments.size();
            }
            public Object instantiateItem(ViewGroup container, int position) {// 初始化要显示的条目
                ImageView imageView = new ImageView(HomeActivity.this);
                imageView.setImageResource(R.drawable.splash);
                container.addView(imageView);//一定将要显示的条目加入到ViewGroup的缓存容器中才可以显示
                return imageView;
            }
            public void destroyItem(ViewGroup container, int position, Object object) {//将要销毁的条目从ViewGroup的缓存容器中移除
                container.removeView((View) object);
            }
        };
        /*mFragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            boolean[]flags=new boolean[mFragments.size()];
            @Override
            public int getCount() {
                return mFragments.size();
            }
            @Override
            public Fragment getItem(int position) {//只有在【新】生成 Fragment 对象时调用。注意：在调用notifyDataSetChanged() 后不会被调用
                Log.i("bqt", "【getItem】被调用了－" + position);
                return mFragments.get(position);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                //return super.getPageTitle(position);
                switch (position) {
                    case 0:
                        return getResources().getString(R.string.music);
                    case 1:
                        return getResources().getString(R.string.video);
                    case 2:
                        return getResources().getString(R.string.picture);
                    case 3:
                        return getResources().getString(R.string.music);
                    default:
                        return null;
                }
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                Fragment fragment=(Fragment)super.instantiateItem(container, position);
                String tag=fragment.getTag();// 得到Tag
                if(flags[position%flags.length]){
                    FragmentTransaction ft=fm.beginTransaction();// 如果这个Fragment需要更新
                    ft.remove(fragment);// 移除旧的Fragment
                    fragment=mFragments.get(position%mFragments.size());// 换成新的Frgment
                    ft.add(container.getId(),fragment,tag);// 用之前的Tab添加新的Frgament
                    ft.attach(fragment);
                    ft.commitAllowingStateLoss();
                    flags[position%flags.length]=false;// 复位更新标志
                }
                return fragment;
            }

            public void refresh(int...arg){
                for(int i=0;i<arg.length;i++){
                    flags[arg[i]]=true;
                }
                notifyDataSetChanged();;
            }
        };*/
        mFragmentStatePagerAdapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return mFragments.size();
            }
            @Override
            public Fragment getItem(int position) {//尼玛尼玛尼玛：在调用notifyDataSetChanged() 后也会被调用啊！
                Log.i("bqt", "【getItem】被调用了－" + position);
                return mFragments.get(position);
            }
        };
        mFragmentPagerAdapter = new ViewFragmentPagerAdapter(fm);
        mViewPager.setAdapter(mFragmentPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

    }
    public void initTopBar() {
        backButton = (ImageButton)findViewById(R.id.top_bar).findViewById(R.id.main_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //FragmentManager fm = getSupportFragmentManager();
                if (listFragment.isAdded()) {
                    FragmentTransaction transaction = fm.beginTransaction();
                    transaction.setCustomAnimations(R.anim.slide_left_in,
                            R.anim.slide_right_out);
                    transaction.remove(listFragment);
                    if (!homeFragment.isAdded()) {
                        Log.e("david", "add home " + firstTag);
                        transaction.add(viewGroup.getId(), homeFragment, firstTag);
                    }
                    mFragments.set(0, homeFragment);
                    //transaction.attach(homeFragment);
                    //changeTab2();
                    //transaction.show(homeFragment);
                    transaction.commit();
                    mFragmentPagerAdapter.notifyDataSetChanged();
                } else {
                    onBackPressed();
                }
                textView.setText(getResources().getString(R.string.music));
            }
        });
        backButton.setVisibility(View.VISIBLE);
        playButton = (ImageButton)findViewById(R.id.top_bar).findViewById(R.id.play_now_button);
        /*playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, PlayActivity.class);
                startActivity(intent);
            }
        });*/
        playButton.setVisibility(View.INVISIBLE);
    }

    /*private void initTab() {
        lastTab = 1;
        mInflater = LayoutInflater.from(this);

        mTabHost = (FragmentTabHost) findViewById(R.id.tabhost);
        mTabHost.setup(this, fm, R.id.realfragment);

        tab_home = new Tab("1", R.string.music, HomeFragment.class) ;
        tab_address = new Tab("2", R.string.video, VideoFragment.class) ;
        tab_find_friend = new Tab("3", R.string.picture, PictureFragment.class) ;
        tab_setting = new Tab("4", R.string.news, NewsFragment.class) ;

        //homeFragment = (HomeFragment) fm.findFragmentByTag(HomeFragment.class.getName());
        //videoFragment = (VideoFragment)fm.findFragmentByTag(VideoFragment.class.getName());
        //newsFragment = (NewsFragment)fm.findFragmentByTag(NewsFragment.class.getName());
        //pictureFragment = (PictureFragment)fm.findFragmentByTag(PictureFragment.class.getName());

        mTabs.add(tab_home);
        mTabs.add(tab_address);
        mTabs.add(tab_find_friend);
        mTabs.add(tab_setting);

        for (int i = 0 ; i < mTabs.size(); i++ ){
            View view = mInflater.inflate(R.layout.framment_tab, null) ;
            ImageView img = (ImageView)view.findViewById(R.id.icon_tab);

            mTabHost.addTab(mTabHost.newTabSpec(mTabs.get(i).getTitle()).
                            setIndicator(getResources().getString(mTabs.get(i).getIcon())),
                    mTabs.get(i).getFragment(), null);
            final int j = i;
            //setIndicator will use setCurrentTab(),and change fragment will auto
            mTabHost.getTabWidget().getChildTabViewAt(j).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("david", "onclick " + j);
                    int newTab = j;
                    FragmentTransaction transaction = fm.beginTransaction();
                    // animation
                    if (lastTab > newTab) {
                        transaction.setCustomAnimations(R.anim.slide_right_in,
                                R.anim.slide_left_out);
                    } else if (lastTab < newTab) {
                        transaction.setCustomAnimations(R.anim.slide_left_in,
                                R.anim.slide_right_out);
                    }
                    lastTab = newTab;
                    if (j == 1) {
                        //showFragment(videoFragment);
                        transaction.replace(R.id.realfragment, videoFragment);
                        textView.setText(getResources().getString(R.string.music));
                    } else if (j == 2) {
                        //showFragment(pictureFragment);
                        transaction.replace(R.id.realfragment, pictureFragment);
                        textView.setText(getResources().getString(R.string.picture));
                    } else if (j == 3) {
                        //showFragment(newsFragment);
                        transaction.replace(R.id.realfragment, newsFragment);
                        textView.setText(getResources().getString(R.string.news));

                    } else if (j == 0) {
                        if (fm.findFragmentByTag("1") == null) {
                            Log.e("david", "1 null");
                            homeFragment = HomeFragment.newInstance(null, null);
                            transaction.replace(R.id.realfragment, homeFragment);
                            textView.setText(getResources().getString(R.string.music));
                        }
                        //showFragment(homeFragment);

                    }

                    transaction.commit();*/

                    /*//FragmentManager fm = getSupportFragmentManager();
                    FragmentTransaction transaction = fm.beginTransaction();
                    //transaction.hide(test);
                    if (j == 1) {
                        mTabHost.setCurrentTab(1);
                        playButton.setVisibility(View.INVISIBLE);
                        backButton.setVisibility(View.INVISIBLE);
                        textView.setText(getResources().getString(R.string.video));
                        if (!videoFragment.isAdded()) {
                            transaction.replace(R.id.realfragment, videoFragment);
                        }
                    } else if (j == 2) {
                        mTabHost.setCurrentTab(2);
                        backButton.setVisibility(View.INVISIBLE);
                        playButton.setVisibility(View.INVISIBLE);
                        textView.setText(getResources().getString(R.string.pictureFragment));
                        if (!pictureFragment.isAdded()) {
                            transaction.replace(R.id.realfragment, pictureFragment);
                        }


                    } else if (j == 3) {
                        mTabHost.setCurrentTab(3);
                        backButton.setVisibility(View.INVISIBLE);
                        playButton.setVisibility(View.INVISIBLE);
                        textView.setText(getResources().getString(R.string.news));
                        if (!newsFragment.isAdded()) {
                            transaction.replace(R.id.realfragment, newsFragment);
                        }
                    } else if (j == 0) {
                        mTabHost.setCurrentTab(0);
                        backButton.setVisibility(View.INVISIBLE);
                        textView.setText(getResources().getString(R.string.music));
                        if (!homeFragment.isAdded()) {
                            transaction.replace(R.id.realfragment, homeFragment);
                        }
                    }
                    transaction.commit();
                }
            });
        }*/

        /*mTabHost.getTabWidget().setShowDividers(LinearLayout.SHOW_DIVIDER_NONE);
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String s) {
                Log.e("david", "click" + s + Integer.parseInt(s) + lastTab);
                int newTab = Integer.parseInt(s);
                FragmentTransaction ft = fm.beginTransaction();
                // animation
                if (lastTab > newTab) {
                    ft.setCustomAnimations(R.anim.slide_right_in,
                            R.anim.slide_left_out);
                } else if (lastTab < newTab) {
                    ft.setCustomAnimations(R.anim.slide_left_in,
                            R.anim.slide_right_out);
                }
                lastTab = newTab;
                ft.commit();
            }
        });
        //mTabHost.setCurrentTab(0);
    }*/

    private void showFragment(Fragment fragment) {
        if (currentFragment != fragment) {
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.hide(currentFragment);
            currentFragment = fragment;
            if (!fragment.isAdded()) {
                transaction.add(R.id.viewpager, fragment).show(fragment).commit();
            } else {
                transaction.show(fragment).commit();
                }
            }
        }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void changeTab2() {
        Fragment fragmenttag = null;
        if (!blag) {//由Tab2Fragment切换至Tab3Fragment
            //fragmenttag=new Tab3Fragment();
            blag = true;
            //button.setText("点击替换Tab3为Tab2");
        } else {//由Tab3Fragment切换至Tab2Fragment
            //fragmenttag=new Tab2Fragment();
            blag = false;
            //button.setText("点击替换Tab2为Tab3");
        }
        mFragments.set(0, listFragment);//将list中Fragment资源替换
        //mFragmentPagerAdapter.refresh(0);
    }

    public void openList() {
        //because use glide, so first fragment is( #0: SupportRequestManagerFragment{1d83d33 #2 com.bumptech.glide.manager})
        //then we must get 1, it is home fragment
        if (fm.getFragments().size() > 1) {
            //homeFragment = (HomeFragment) fm.getFragments().get(1);
            if (homeFragment == null) {
                homeFragment = (HomeFragment) fm.findFragmentByTag("1");
            }

        } else if (homeFragment == null){
            homeFragment = (HomeFragment) fm.getFragments().get(0);
        }
        firstTag = homeFragment.getTag();
        Log.e("david", "tag " + firstTag);
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_right_in,
                R.anim.slide_left_out);
        //transaction.hide(homeFragment);
        transaction.remove(homeFragment);
        if (!listFragment.isAdded()) {
            Log.e("david", "add list " + firstTag);
            transaction.add(viewGroup.getId(), listFragment, firstTag);
        }

        mFragments.set(0, listFragment);

        //transaction.attach(listFragment);
        //transaction.show(listFragment);
        //homeFragment.setMenuVisibility(false);
        //homeFragment.setUserVisibleHint(false);
        //listFragment.setMenuVisibility(true);
        //listFragment.setUserVisibleHint(true);
        transaction.commit();
        mFragmentPagerAdapter.notifyDataSetChanged();
        backButton.setVisibility(View.VISIBLE);
        textView.setText(getResources().getString(R.string.playlist));
        //tab_home = new Tab(1, R.drawable.tab_main, AddListFragment.class);
        //mTabs.add(tab_home);
        //mFragmentPagerAdapter.notifyDataSetChanged();
        //changeTab2();

    }


    /*public static class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if("ACTION_OPEN".equals(intent.getAction())){
                String daa = intent.getStringExtra("data");
                //Log.e("test","01号接收了"+ daa);
                //openList();

            }
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();

    }

    public class MyReceiver extends BroadcastReceiver {
        public MyReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO: This method is called when the BroadcastReceiver is receiving
            // an Intent broadcast.
            if("ACTION_OPEN".equals(intent.getAction())){
                String daa = intent.getStringExtra("data");
                Log.e("test","01号接收了"+ daa);
                //openList();
                openList();

            }
        }
    }

    @Override
    protected void onStop() {
        Log.e("david", "homeactivity stop");
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        //moveTaskToBack(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_Holo_Dialog));
        AlertDialog alertDialog = builder.setTitle(getResources().getString(R.string.goexit))
                .setPositiveButton(R.string.sure, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        System.exit(0);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                }).create();
        alertDialog.getWindow().setGravity(Gravity.BOTTOM);
        alertDialog.show();
        //super.onBackPressed();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("david", "homeactivity destory");
        unbindService(mServiceConnection);
        unregisterReceiver(myReceiver);
        //must ui thread
        Glide.get(this).clearMemory();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Glide.get(HomeActivity.this).clearDiskCache();
            }
        }).start();
    }

    private void initConnection() {
        //mediaService = new MediaService();
        //mediaService = MediaService.getInstance();
        mediaService = MusicApplication.mediaService;
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                //mediaService = MediaService.getInstance();
                mediaService = ((MediaService.TestBinder)service).getService();
                Log.e("david", "home onServiceConnected = " + mediaService);
                if (mediaService == null) {
                    new AlertDialog.Builder(HomeActivity.this)
                            .setMessage("MediaService fail")
                            .setCancelable(true)
                            .show();
                    finish();
                }
            }
        };

        Intent intent = new Intent(getApplicationContext(), MediaService.class);
        Log.e("david", "bindservice = " + getApplicationContext());
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        Log.e("david", "bindservice-- = " + mediaService);
    }

    public class ViewFragmentPagerAdapter extends FragmentPagerAdapter {
        boolean[]flags = new boolean[mFragments.size()];

        public ViewFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
        @Override
        public Fragment getItem(int position) {//只有在【新】生成 Fragment 对象时调用。注意：在调用notifyDataSetChanged() 后不会被调用
            Log.i("david", "【getItem】被调用了－" + position);
            return mFragments.get(position);
        }

        @Override
        public long getItemId(int position) {

            //Log.e("david", "get POSITION_NONE");
            Log.e("david", "getitemid" + super.getItemId(position) + getItem(position).toString());
            //return FragmentPagerAdapter.POSITION_NONE;
            return super.getItemId(position);
        }

        @Override
        public int getItemPosition(Object object) {
            //return super.getItemPosition(object);
            Log.e("david", "getitemposition" +  + super.getItemPosition(object) + object.toString());
            return FragmentPagerAdapter.POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            //return super.getPageTitle(position);
            switch (position) {
                case 0:
                    return getResources().getString(R.string.music);
                case 1:
                    return getResources().getString(R.string.video);
                case 2:
                    return getResources().getString(R.string.picture);
                case 3:
                    return getResources().getString(R.string.news);
                default:
                    return null;
            }
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Log.e("david", "instantiateItem");
            viewGroup = container;
            Fragment fragment=(Fragment)super.instantiateItem(container, position);
            /*String tag = fragment.getTag();// 得到Tag
            Log.e("david", "position = " + position + "falglength = " + flags.length + fragment.toString() + flags[position % flags.length]);
            if(flags[position % flags.length]){
                FragmentTransaction ft = fm.beginTransaction();// 如果这个Fragment需要更新
                ft.remove(fragment);// 移除旧的Fragment
                fragment = mFragments.get(position%mFragments.size());// 换成新的Frgment
                ft.add(container.getId(),fragment,tag);// 用之前的Tab添加新的Frgament
                ft.attach(fragment);
                ft.commitAllowingStateLoss();
                flags[position % flags.length]=false;// 复位更新标志
            }*/
            fragment = mFragments.get(position);
            /*if (!fragment.isAdded()) {
                FragmentTransaction ft = fm.beginTransaction();
                ft.add(container.getId(), fragment, fragment.getTag());
                ft.commit();
            }*/
            Log.e("david", "now fragment" + fragment.toString());
            return fragment;
        }

        /*public void refresh(int...arg){
            for(int i = 0; i < arg.length; i++) {
                flags[arg[i]]=true;
            }
            Log.e("david", "refresf");
            notifyDataSetChanged();
        }*/
    };


}
