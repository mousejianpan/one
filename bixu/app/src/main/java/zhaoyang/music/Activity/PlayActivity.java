package zhaoyang.music.Activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.Looper;
import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.lang.reflect.Field;
import java.util.List;

import zhaoyang.music.Application.MusicApplication;
import zhaoyang.music.Component.AlertDialogBuilder;
import zhaoyang.music.Domain.ControlEvent;
import zhaoyang.music.Domain.PlayEvent;
import zhaoyang.music.Domain.Song;
import zhaoyang.music.IMediaAidlInterface;
import zhaoyang.music.Media.MediaService;
import zhaoyang.music.Media.MusicPlayer;
import zhaoyang.music.R;
import zhaoyang.music.lyric.LyricAdapter;
import zhaoyang.music.lyric.LyricDownloadManager;
import zhaoyang.music.lyric.LyricLoadHelper;
import zhaoyang.music.lyric.LyricSentence;
import zhaoyang.music.utils.Constants;
import zhaoyang.music.utils.MusicUtils;

public class PlayActivity extends AppCompatActivity implements LyricDownloadManager.LoadLyricListener {

    private ImageButton backButton,modeButton;
    private ImageButton preButton, playButton, nextButton;
    private TextView title, songPlayTime, songLong;
    private ListView lyricList;
    private TextView lyricText;
    private SeekBar seekBar;
    private MusicUtils musicUtils;
    private MusicPlayer musicPlayer;
    private ControlEvent controlEvent;
    private PlayEvent playEvent;
    private LyricAdapter lyricAdapter;
    private LyricDownloadManager lyricDownloadManager;
    private LyricLoadHelper lyricLoadHelper;
    private String songName, newSong;
    private View lyricContent;
    private SlidingDrawer slidingDrawer;
    private IBinder binder;
    private MediaService mediaService;
    private MediaReceiver mediaReceiver;
    private ServiceConnection mServiceConnection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.play_land);
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_play);
        }
        initData();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("ACTION_PLAY");
        intentFilter.addAction("ACTION_PAUSE");
        mediaReceiver = new MediaReceiver();
        registerReceiver(mediaReceiver, intentFilter);
        EventBus.getDefault().register(this);
        IMediaAidlInterface iMediaAidlInterface;
        //mediaService = iMediaAidlInterface
        //mService = IMediaAidlInterface.Stub.asInterface(service);
        //mediaService = MediaService.getInstance();
        //mediaService = MusicApplication.mediaService;
        Log.e("david", "playactivity = " + mediaService);
        //mediaService = new MediaService();
        /*MediaService.TestBinder testBinder = mediaService.new TestBinder();
        mediaService = testBinder.getService();
        if (mediaService != null) {
            Log.e("david", "mediaservice is not null");
        } else {
            Log.e("david", "mediaservice is null");
        }*/
        initConnection();
        if (musicPlayer.isPlaying()) {
            playButton.setBackgroundResource(R.drawable.play_button);
        } else if (musicPlayer.isPause()) {
            playButton.setBackgroundResource(R.drawable.pause_button);
        }
    }
    private void initData() {
        //Log.e("david play111", MusicApplication.playingName);
        songName = MusicApplication.playingName;
        playEvent = new PlayEvent();
        controlEvent = new ControlEvent();
        musicUtils = new MusicUtils();
        //musicPlayer = MusicPlayer.getInstance();
        musicPlayer = MusicApplication.musicPlayer;
        lyricAdapter = new LyricAdapter(getApplicationContext());
        lyricDownloadManager = new LyricDownloadManager(getApplicationContext(), this);
        lyricLoadHelper = new LyricLoadHelper();
        backButton = (ImageButton)findViewById(R.id.top_bar).findViewById(R.id.main_back_button);
        modeButton = (ImageButton)findViewById(R.id.top_bar).findViewById(R.id.play_now_button);
        preButton = (ImageButton)findViewById(R.id.button_play).findViewById(R.id.play_pre);
        playButton = (ImageButton)findViewById(R.id.button_play).findViewById(R.id.play_or_pause);
        nextButton = (ImageButton)findViewById(R.id.button_play).findViewById(R.id.play_next);
        title = (TextView)findViewById(R.id.top_bar).findViewById(R.id.play_list);
        songPlayTime = (TextView)findViewById(R.id.button_play).findViewById(R.id.song_playtime);
        songLong = (TextView)findViewById(R.id.button_play).findViewById(R.id.song_long);
        seekBar = (SeekBar)findViewById(R.id.button_play).findViewById(R.id.seekbar);
        slidingDrawer = (SlidingDrawer)findViewById(R.id.sliding);
        lyricContent = findViewById(R.id.lyric_content);
        lyricList = (ListView)findViewById(R.id.lyric_list);
        lyricText = (TextView)findViewById(R.id.lyric_empty);
        modeButton.setBackgroundResource(R.drawable.playmode_repeate_random_pressed);
        lyricList.setAdapter(lyricAdapter);
        lyricLoadHelper.setLyricListener(mLyricListener);
        slidingDrawer.open();
        songPlayTime.setText(getResources().getString(R.string.play_time));
        songLong.setText(getResources().getString(R.string.song_long));
        seekBar.setMax(0);
        seekBar.setProgress(0);
        modeButton.setVisibility(View.GONE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaService.isPlaying()) {
                    Log.e("david", "playing now");
                    mediaService.pause();
                    playEvent.setStatus(ControlEvent.PAUSE);
                    EventBus.getDefault().postSticky(playEvent);
                    playButton.setBackgroundResource(R.drawable.pause_button);
                } else if (mediaService.isPause()) {
                    mediaService.start();
                    Log.e("david", "pause now");
                    playEvent.setStatus(ControlEvent.PLAY);
                    EventBus.getDefault().postSticky(playEvent);
                    playButton.setBackgroundResource(R.drawable.play_button);
                } else {
                    Log.e("david", "play first one");
                    playEvent.setStatus(ControlEvent.FIRST_PLAY);
                    EventBus.getDefault().postSticky(playEvent);
                    playButton.setBackgroundResource(R.drawable.play_button);
                }
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playEvent.setStatus(ControlEvent.NEXT);
                EventBus.getDefault().postSticky(playEvent);
                playButton.setBackgroundResource(R.drawable.play_button);
            }
        });
        preButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playEvent.setStatus(ControlEvent.PREVIOUS);
                EventBus.getDefault().postSticky(playEvent);
                playButton.setBackgroundResource(R.drawable.play_button);
            }
        });
        modeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), testActivity.class);
                startActivity(intent);
            }
        });
        lyricContent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (lyricList.isShown()) {
                        return true;
                    } else {
                        AlertDialog.Builder builder =
                                new AlertDialog.Builder(new ContextThemeWrapper(getApplicationContext(), android.R.style.Theme_Holo_Dialog));
                        builder.setTitle(R.string.note)
                                .setMessage(R.string.down_lyric)
                                .setPositiveButton(R.string.sure, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        //download
                                        dialogInterface.dismiss();
                                        //new LyricDownloadAsyncTask().execute(songName, songName);
                                        String lyricFilePath = lyricDownloadManager.searchLyricFromWeb(
                                                songName, songName, songName);
                                    }
                                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
                    }
                }

                return true;
            }
        });
        loadLyric();

    }

    private void initConnection() {
        //mediaService = new MediaService();
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                //mediaService = MediaService.getInstance();
                mediaService = ((MediaService.TestBinder)service).getService();
                if (mediaService == null) {
                    new android.app.AlertDialog.Builder(new ContextThemeWrapper(getApplicationContext(), android.R.style.Theme_Holo_Dialog))
                            .setMessage("MediaService fail")
                            .setCancelable(true)
                            .show();
                    finish();
                }
            }
        };

        Intent intent = new Intent(getApplicationContext(), MediaService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Subscribe (threadMode = ThreadMode.POSTING, sticky = true, priority = 100)
    public void onEvent(Song song){
        //lyrcText.setText(test);
        //Log.e("david", "onEvent");
        lyricLoadHelper.notifyTime(song.getNowTime());
        newSong = song.getName();
        title.setText(song.getName());
        songPlayTime.setText(musicUtils.StringformatTime(song.getNowTime()));
        songLong.setText(musicUtils.StringformatTime(song.getLongTime()));
        seekBar.setMax(song.getLongTime());
        seekBar.setProgress(song.getNowTime());
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int position;
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b) {
                    position = i;
                    //seekBar.setProgress(i);
                    //if not seekto, it will go back when not stop touch
                    mediaService.seekTo(position);
                    songPlayTime.setText(musicUtils.StringformatTime(i));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaService.seekTo(position);
                Log.e("david", "status2 : " + musicPlayer.isPause() + musicPlayer.isPlaying());

            }
        });
        //loadLyric();
        if (newSong.equals(songName)) {
            return;
        } else {
            songName = newSong;
            loadLyric();
        }
    }

    @Subscribe (threadMode = ThreadMode.MAIN, sticky = true, priority = 100)
    public void Event(ControlEvent controlEvent) {
        //Log.e("david", "playactivity event");
        int i = controlEvent.getStatus();
        switch (i) {
            case 0:
                playButton.setBackgroundResource(R.drawable.play_button);
                break;
            case 1:
                playButton.setBackgroundResource(R.drawable.pause_button);
                break;
            case 2:
                playButton.setBackgroundResource(R.drawable.play_button);
                break;
            case 3:
                playButton.setBackgroundResource(R.drawable.play_button);
                break;
            default:
                break;
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mediaReceiver);
        unbindService(mServiceConnection);
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    private void loadLyric() {
        if (songName == null) {
            Log.e("david", "song is null");
            return;
        }
        // 取得歌曲同目录下的歌词文件绝对路径
        String lyricFilePath = Constants.lrcPath + "/" + songName
                + ".lrc";
        File lyricfile = new File(lyricFilePath);

        if (lyricfile.exists()) {
            // 本地有歌词，直接读取
            // Log.i(TAG, "loadLyric()--->本地有歌词，直接读取");
            Log.e("david", lyricFilePath + " exist");
            lyricList.setVisibility(View.VISIBLE);
            lyricText.setVisibility(View.GONE);
            lyricLoadHelper.loadLyric(lyricFilePath);
        } else {
            Log.e("david", lyricFilePath + "not exist");
            //download lyric
            lyricList.setVisibility(View.GONE);
            lyricText.setVisibility(View.VISIBLE);
            lyricText.setText(R.string.click_down);
            lyricText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog dialog =
                            new AlertDialog.Builder(new ContextThemeWrapper(PlayActivity.this, android.R.style.Theme_Holo_Dialog))
                            .setTitle(R.string.down_lyric)
                            .setPositiveButton(R.string.sure, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //download
                                    dialogInterface.dismiss();
                                    //new LyricDownloadAsyncTask().execute(songName, songName);
                                    String lyricFilePath = lyricDownloadManager.searchLyricFromWeb(
                                            songName, songName, songName);
                                }
                            }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).create();
                    dialog.show();
                    //reject to change dialog style
                    try {
                        Field mAlert = AlertDialog.class.getDeclaredField("mAlert");
                        mAlert.setAccessible(true);
                        Object mAlertController = mAlert.get(dialog);
                        Field mMessage = mAlertController.getClass().getDeclaredField("mTitleView");
                        mMessage.setAccessible(true);
                        TextView mMessageView = (TextView) mMessage.get(mAlertController);
                        mMessageView.setTextColor(Color.BLUE);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                    }
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED);
                }
            });
        }
    }

    @Override
    public void loadedLyric() {
        loadLyric();
    }

    class LyricDownloadAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            // 从网络获取歌词，然后保存到本地
            String lyricFilePath = lyricDownloadManager.searchLyricFromWeb(
                    params[0], params[1], songName);
            // 返回本地歌词路径
            if (lyricFilePath == null) {
                Looper.prepare();
                Toast.makeText(getApplicationContext(), R.string.net_fail, Toast.LENGTH_SHORT).show();
                Looper.loop();
            }
            return lyricFilePath;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            // Log.i(TAG, "网络获取歌词完毕，歌词保存路径:" + result);
            // 读取保存到本地的歌曲
            if(!lyricLoadHelper.loadLyric(result)) {
                Looper.prepare();
                Toast.makeText(PlayActivity.this, R.string.no_lyric, Toast.LENGTH_SHORT).show();
                Looper.loop();
            }
        };
    };

    private LyricLoadHelper.LyricListener mLyricListener = new LyricLoadHelper.LyricListener() {

        @Override
        public void onLyricLoaded(List<LyricSentence> lyricSentences, int index) {
            // Log.i(TAG, "onLyricLoaded");
            if (lyricSentences != null) {
                // Log.i(TAG, "onLyricLoaded--->歌词句子数目=" + lyricSentences.size()
                // + ",当前句子索引=" + index);
                lyricAdapter.setLyric(lyricSentences);
                lyricAdapter.setCurrentSentenceIndex(index);
                lyricAdapter.notifyDataSetChanged();
                // 本方法执行时，lyricshow的控件还没有加载完成，所以延迟下再执行相关命令
                // mHandler.sendMessageDelayed(
                // Message.obtain(null, MSG_SET_LYRIC_INDEX, index, 0),
                // 100);
            }
        }

        @Override
        public void onLyricSentenceChanged(int indexOfCurSentence) {
            // Log.i(TAG, "onLyricSentenceChanged--->当前句子索引=" +
            // indexOfCurSentence);
            lyricAdapter.setCurrentSentenceIndex(indexOfCurSentence);
            lyricAdapter.notifyDataSetChanged();
            lyricList.smoothScrollToPositionFromTop(indexOfCurSentence,
                    lyricList.getHeight() / 2, 500);
        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.play_land);
            initData();
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_play);
            initData();
        }
        super.onConfigurationChanged(newConfig);
    }

    public class MediaReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("david", "receive notification playactivity" + intent.getAction());
            if ("ACTION_PLAY".equals(intent.getAction())) {
                Log.e("david" , "receive play playactivity");
                if (mediaService.isPause()) {
                    mediaService.start();
                }
                playEvent.setStatus(ControlEvent.PLAY);
                EventBus.getDefault().postSticky(playEvent);
                playButton.setBackgroundResource(R.drawable.play_button);
            } else if ("ACTION_PAUSE".equals(intent.getAction())) {
                Log.e("david" , "receive pause playactivity");
                if (mediaService.isPlaying()) {
                    mediaService.pause();
                }
                playEvent.setStatus(ControlEvent.PAUSE);
                EventBus.getDefault().postSticky(playEvent);
                playButton.setBackgroundResource(R.drawable.pause_button);
            }
        }
    }
}
