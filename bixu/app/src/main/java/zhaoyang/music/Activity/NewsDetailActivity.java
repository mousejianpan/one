package zhaoyang.music.Activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import zhaoyang.music.Bean.News;
import zhaoyang.music.R;
import zhaoyang.music.View.MyLoadingView;

public class NewsDetailActivity extends AppCompatActivity {
    private News news;
    private TextView text_title;
    private TextView text_content;
    private ImageView imageView;
    private MyLoadingView loadImage;
    private AnimatorSet animatorSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadImage = (MyLoadingView)findViewById(R.id.detail_loadimage);
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(loadImage, "rotation", 0, 360);
        objectAnimator.setRepeatCount(ValueAnimator.INFINITE);
        objectAnimator.setInterpolator(new LinearInterpolator());
        objectAnimator.setDuration(2000);
        animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator);
        animatorSet.start();
        //loadImage.setImageResource(R.drawable.ic_loading);
        /*Glide.with(this)
                .load(R.drawable.loading_now)
                .into(loadImage);*/
        text_title = (TextView)findViewById(R.id.news_title);
        text_content = (TextView)findViewById(R.id.news_content);
        imageView = (ImageView)findViewById(R.id.news_image);
        news = new News();
        Intent intent = getIntent();
        news.setDetails(intent.getStringExtra("detail"));
        news.setTitle(intent.getStringExtra("title"));
        news.setTime(intent.getStringExtra("time"));
        news.setSource(intent.getStringExtra("source"));
        news.setImage(intent.getStringExtra("image"));
        //webView = (WebView)findViewById(R.id.webview);
        //webView.loadUrl(url);
        String strUrl = "http://c.m.163.com/nc/article/" + news.getDetails() + "/full.html";
        Log.e("david", "url = " + strUrl);
        init();
    }

    public void init() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // UTF_8 for chinese
            /*try {
                musicName = URLEncoder.encode(musicName, UTF_8);
                musicName = musicName.replaceAll("\\+",  " ");
                singerName = URLEncoder.encode(singerName, UTF_8);
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
            musicName = getFileNameNoEx(musicName);*/
            URL mUrl = null;

            String strUrl = "http://c.m.163.com/nc/article/" + news.getDetails() + "/full.html";
            Log.e("david", "url = " + strUrl);

            try {
                mUrl = new URL(strUrl);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            try {
                HttpURLConnection httpConn = (HttpURLConnection) mUrl
                        .openConnection();
                //httpConn.setRequestMethod("get");
                httpConn.setReadTimeout(8000);
                if (httpConn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e("david", "http  fail");
                    return;
                }
                httpConn.connect();
                Log.e("david", "http  success");

                //test json data
                InputStreamReader in = new InputStreamReader(httpConn.getInputStream());
                BufferedReader bu = new BufferedReader(in);
                String string;
                StringBuilder stringBuilder = new StringBuilder();
                while((string = bu.readLine()) != null) {
                    Log.e("david", "string" + string + "\n");
                    stringBuilder.append(string);

                }

                JSONObject object = new JSONObject(stringBuilder.toString());
                JSONObject array = object.getJSONObject(news.getDetails());
                Log.e("david", "jasonobject" + array.toString());
                //JSONArray content = array.getJSONArray("body");
                String content = Html.fromHtml(array.getString("body"), Html.FROM_HTML_MODE_COMPACT).toString();
                Log.e("david", "content" + content);
                news.setContent(content);

                httpConn.disconnect();
                Observable.create(new ObservableOnSubscribe<Object>() {
                    @Override
                    public void subscribe(ObservableEmitter<Object> e) throws Exception {
                        e.onNext(news);
                    }
                }).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<Object>() {
                            @Override
                            public void accept(Object o) throws Exception {
                                initView();

                            }
                        });
                } catch (IOException e1) {
                    e1.printStackTrace();
                    Log.e("david", "http success , IO exception");
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("david", "XML parse error");
                    return;
                }
                return;
            }
        }).start();
    }

    public void initView() {
        loadImage.setVisibility(View.GONE);
        text_title.setText(news.getTitle());
        Glide.with(getApplicationContext())
                .load(news.getImage())
                .into(imageView);
        text_content.setText(news.getContent());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (animatorSet != null && animatorSet.isStarted()) {
            animatorSet.end();
        }
    }
}
