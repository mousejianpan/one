package zhaoyang.music.Activity;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.foamtrace.photopicker.ImageCaptureManager;
import com.foamtrace.photopicker.PhotoPickerActivity;
import com.foamtrace.photopicker.SelectModel;
import com.foamtrace.photopicker.intent.PhotoPickerIntent;

import java.io.IOException;
import java.security.PrivilegedAction;
import java.util.ArrayList;

import zhaoyang.music.R;

public class PickPhotoActivity extends AppCompatActivity {
    private ImageView pickImage;
    private ImageButton pickBack, pickMenu;
    private String currentPath;
    private ImageCaptureManager imageCaptureManager;
    private static final int REQUEST_PICTURE = 188;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_photo);
        init();
    }

    private void init() {
        pickBack = (ImageButton)findViewById(R.id.pick_back);
        pickMenu = (ImageButton)findViewById(R.id.pick_menu);
        pickImage = (ImageView)findViewById(R.id.pick_image);
        Intent intent = getIntent();
        String path = intent.getExtras().getString("path");
        Log.e("david", "path = " + path);
        if ((path.isEmpty() && path == null) || "null".equals(path)) {
            Glide.with(this).load(R.drawable.ic_loading).into(pickImage);
        } else {
            Glide.with(this).load(path).into(pickImage);
        }
        initViewCLick();
    }

    private void initViewCLick() {
        pickBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        pickMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Snackbar snackbar = Snackbar.make(findViewById(R.id.pick_image),
                        " ", Snackbar.LENGTH_INDEFINITE);
                final View snack = snackbar.getView();
                Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout)snack;
                View addView = LayoutInflater.from(PickPhotoActivity.this)
                        .inflate(R.layout.snackbar_layout, null);
                (addView.findViewById(R.id.from_camera)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imageCaptureManager = new ImageCaptureManager(PickPhotoActivity.this);
                        try {
                            Intent intent = imageCaptureManager.dispatchTakePictureIntent();
                            startActivityForResult(intent, ImageCaptureManager.REQUEST_TAKE_PHOTO);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        snackbar.dismiss();

                    }
                });
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER_VERTICAL;
                snackbar.setAction("相册", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PhotoPickerIntent photoPickerIntent = new PhotoPickerIntent(PickPhotoActivity.this);
                        photoPickerIntent.setSelectModel(SelectModel.SINGLE);
                        photoPickerIntent.setShowCarema(true);
                        startActivityForResult(photoPickerIntent, REQUEST_PICTURE);
                        snackbar.dismiss();

                    }
                });
                snackbarLayout.addView(addView, 0, params);
                snackbar.show();
            }
        });
    }

    public void pickOver(String path) {
        Intent intent = new Intent();
        intent.putExtra("path", currentPath);
        setResult(RESULT_OK, intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ImageCaptureManager.REQUEST_TAKE_PHOTO :
                    currentPath = imageCaptureManager.getCurrentPhotoPath();
                    if (currentPath != null) {
                        Glide.with(this).load(currentPath).into(pickImage);
                    }
                    break;
                case REQUEST_PICTURE:
                    ArrayList<String> path = data.getStringArrayListExtra(PhotoPickerActivity.EXTRA_RESULT);
                    currentPath = path.get(0);
                    if (currentPath != null) {
                        Glide.with(this).load(currentPath).into(pickImage);
                    }

            }
        }
        pickOver(currentPath);
    }
}
