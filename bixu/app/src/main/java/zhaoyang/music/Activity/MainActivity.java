package zhaoyang.music.Activity;

import android.Manifest;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.TabHost;

import zhaoyang.music.Application.MusicApplication;
import zhaoyang.music.Media.MusicPlayer;
import zhaoyang.music.R;
import zhaoyang.music.utils.Constants;
import zhaoyang.music.utils.RequestPermission;

public class MainActivity extends TabActivity {
    ImageButton mainButton;
    ImageButton netButton;
    ImageButton searchButtom;
    MusicPlayer musicPlayer;
    private RequestPermission requestPermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init_BottomButton();
        mainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //LocalListDao localListDao = new LocalListDaoImp(getApplicationContext());
                //localListDao.getLocalMusicList();
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
        requestPermission = new RequestPermission(getApplicationContext());
        requestPermission.requestPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                RequestPermission.CODE_FOR_WRITE_PERMISSION);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void init_TabSpec() {
        Intent intent = new Intent(this, MainActivity.class);
        TabHost tabHost = getTabHost();
        TabHost.TabSpec tabSpec = tabHost.newTabSpec(Constants.TAB_SPEC.MAIN_SPEC.getId())
                .setIndicator(Constants.TAB_SPEC.MAIN_SPEC.getId());
        tabSpec.setContent(intent);
        tabHost.addTab(tabSpec);
    }

    public void init_BottomButton() {
        mainButton = (ImageButton)findViewById(R.id.tab_songs);
        netButton = (ImageButton)findViewById(R.id.tab_net);
        searchButtom = (ImageButton)findViewById(R.id.tab_search);

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.e("david", "finish");
            MusicApplication.start = false;
            Log.e("david", "onterminate - 0");
            musicPlayer = MusicApplication.musicPlayer;
            if (musicPlayer != null) {
                musicPlayer.stop();
                musicPlayer.release();
                musicPlayer = null;
                Log.e("david", "musicPlayer is null");
                Log.e("david", "onterminate - 1");
            }
            this.finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
