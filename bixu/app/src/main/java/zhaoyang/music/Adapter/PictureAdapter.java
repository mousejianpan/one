package zhaoyang.music.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.zip.Inflater;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import zhaoyang.music.Bean.Picture;
import zhaoyang.music.Fragment.PictureFragment;
import zhaoyang.music.R;
import zhaoyang.music.interface_.OnCLickRecyclerViewItemListener;

/**
 * Created by SPREADTRUM\david.zhao on 18-3-16.
 */

public class PictureAdapter extends RecyclerView.Adapter {
    private Context context;
    private List<Picture> pictureList;
    private OnCLickRecyclerViewItemListener onCLickRecyclerViewItemListener;
    public PictureAdapter(Context context, List<Picture> list, OnCLickRecyclerViewItemListener listener) {
        this.context = context;
        this.pictureList = list;
        onCLickRecyclerViewItemListener = listener;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return null;
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_item_layout, null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final int i = position;
        MyViewHolder myViewHolder = (MyViewHolder)holder;
        final Picture picture = pictureList.get(position);
        Glide.with(context).load(picture.getBitmap())
                //glide can edit picture
                //.bitmapTransform(new RoundedCornersTransformation(context, 24, 0, RoundedCornersTransformation.CornerType.ALL))
                .placeholder(R.mipmap.ic_launcher)
                .skipMemoryCache(false)
                .thumbnail(0.01f)
                .into(myViewHolder.imageView);
        //myViewHolder.imageView.setImageBitmap(picture.bitmap);
        myViewHolder.textView.setText(picture.getName());
        myViewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCLickRecyclerViewItemListener.onCLick(picture);
            }
        });
        myViewHolder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCLickRecyclerViewItemListener.onCLick(picture);

            }
        });
    }

    @Override
    public int getItemCount() {
        //return 0;
        if (pictureList != null) {
            return pictureList.size();
        } else {
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView)itemView.findViewById(R.id.pic_image);
            textView = (TextView)itemView.findViewById(R.id.pic_path);
        }
    }
}
