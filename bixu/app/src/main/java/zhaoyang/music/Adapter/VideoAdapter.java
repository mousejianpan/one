package zhaoyang.music.Adapter;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.SensorEvent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.zip.Inflater;

import cn.jzvd.JZResizeTextureView;
import cn.jzvd.JZVideoPlayer;
import cn.jzvd.JZVideoPlayerManager;
import cn.jzvd.JZVideoPlayerStandard;
import zhaoyang.music.Bean.Picture;
import zhaoyang.music.Bean.Video;
import zhaoyang.music.R;

/**
 * Created by SPREADTRUM\david.zhao on 18-3-16.
 */

public class VideoAdapter extends RecyclerView.Adapter {
    private Context context;
    private List<Video> videoList;
    public VideoAdapter(Context context, List<Video> list) {
        this.context = context;
        videoList = list;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return null;
        View view = LayoutInflater.from(context).inflate(R.layout.video_recycleview_item, null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MyViewHolder myViewHolder = (MyViewHolder)holder;
        Video video = videoList.get(position);
        //JZVideoPlayerStandard jzVideoPlayerStandard = (JZVideoPlayerStandard) view.findViewById(R.id.videoplayer);
        myViewHolder.jzVideoPlayerStandard.setUp(video.getPath(), JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL, video.getName());
        //jzVideoPlayerStandard.thumbImageView.setImage("http://p.qpic.cn/videoyun/0/2449_43b6f696980311e59ed467f22794e792_1/640");
        //myViewHolder.jzVideoPlayerStandard.thumbImageView.setImageURI(Uri.parse(video.getThumb()));
        //Bitmap bitmap = BitmapFactory.decodeFile(video.getThumb());
        //myViewHolder.jzVideoPlayerStandard.thumbImageView.setImageBitmap(bitmap);
        Log.e("david", "thumb = " + video.getThumb());
        Glide.with(context).load(video.getThumb()).into(myViewHolder.jzVideoPlayerStandard.thumbImageView);
        //JZVideoPlayer.setTextureViewRotation(Configuration.ORIENTATION_PORTRAIT);
        //myViewHolder.jzVideoPlayerStandard.setRotation(Configuration.ORIENTATION_PORTRAIT);


    }

    @Override
    public int getItemCount() {
        //return 0;
        if (videoList != null) {
            return videoList.size();
        } else {
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public JZVideoPlayerStandard jzVideoPlayerStandard;

        public MyViewHolder(View itemView) {
            super(itemView);
            jzVideoPlayerStandard = (JZVideoPlayerStandard) itemView.findViewById(R.id.videoplayer);
        }
    }
}