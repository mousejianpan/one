package zhaoyang.music.Adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.net.URI;
import java.util.List;

import zhaoyang.music.Bean.News;
import zhaoyang.music.R;
import zhaoyang.music.interface_.OnCLickRecyclerViewItemListener;
import zhaoyang.music.interface_.OnClickNewsItemListener;

/**
 * Created by SPREADTRUM\david.zhao on 18-4-8.
 */

public class NewsAdapter extends RecyclerView.Adapter {
    Context context;
    OnClickNewsItemListener listener;
    List<News> news;

    public NewsAdapter(Context context, OnClickNewsItemListener listener, List<News> news) {
        this.context = context;
        this.listener = listener;
        this.news = news;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return null;
        View view = LayoutInflater.from(context).inflate(R.layout.news_recycleview_item, null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (news.size() > 0) {
            MyViewHolder myViewHolder = (MyViewHolder)holder;
            myViewHolder.text_title.setText(news.get(position).getTitle());
            myViewHolder.text_content.setText(news.get(position).getContent());
            myViewHolder.text_time.setText(news.get(position).getTime());
            if (news.get(position).getSource() != null) {
                myViewHolder.text_source.setText("来源：" + news.get(position).getSource());
            } else {
                myViewHolder.text_source.setText("来源：未知");
            }
            if (news.get(position).getImage() != null) {
                Glide.with(context)
                        .load(Uri.parse(news.get(position).getImage()))
                        .into(myViewHolder.imageView);
            } else {
                myViewHolder.imageView.setImageResource(R.drawable.ic_loading);
            }
            myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onCLick(news.get(position));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView text_title, text_content, text_time, text_source;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.news_image);
            text_title = (TextView)itemView.findViewById(R.id.news_title);
            text_content = (TextView)itemView.findViewById(R.id.news_short_content);
            text_time = (TextView)itemView.findViewById(R.id.news_time);
            text_source = (TextView)itemView.findViewById(R.id.news_source);
        }
    }

    public void updateData(List<News> news) {
        this.news = news;
    }
}
