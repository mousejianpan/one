package zhaoyang.music.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import zhaoyang.music.R;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-7.
 */

public class HomeAdapter extends BaseAdapter {
    private Context context;
    List<String> menu_list;

    public HomeAdapter(Context context, ArrayList<String> menu_list) {
        this.context = context;
        this.menu_list = menu_list;
    }

    @Override
    public int getCount() {
        return menu_list.size();
    }

    @Override
    public Object getItem(int i) {
        return menu_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.home_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.left_image = (ImageView) view.findViewById(R.id.home_left_image);
            viewHolder.textView = (TextView) view.findViewById(R.id.home_text);
            viewHolder.right_image = (ImageView) view.findViewById(R.id.home_right_image);
            viewHolder.song_num = (TextView) view.findViewById(R.id.num_song);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        if ("L".equals(getItem(i).toString())) {
            viewHolder.left_image.setBackgroundResource(R.drawable.ic_media);
            viewHolder.textView.setText(context.getString(R.string.localmusic));
        } else if ("P".equals(getItem(i).toString())){
            viewHolder.left_image.setBackgroundResource(R.drawable.ic_playlist);
            viewHolder.textView.setText(context.getString(R.string.playlist));
        } else if ("LIST".equals(getItem(i).toString())) {
            viewHolder.song_num.setVisibility(View.VISIBLE);
            viewHolder.song_num.setText(R.string.num_song);
            viewHolder.textView.setText(context.getString(R.string.myfavourite));
        }
        viewHolder.right_image.setBackgroundResource(R.drawable.right_arrows);
        return view;
    }

    static class ViewHolder{
        private ImageView left_image;
        private TextView textView;
        private TextView song_num;
        private ImageView right_image;

    }
}
