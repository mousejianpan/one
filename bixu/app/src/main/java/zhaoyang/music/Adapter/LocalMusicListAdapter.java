package zhaoyang.music.Adapter;

import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.app.AlertDialog;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import zhaoyang.music.Application.MusicApplication;
import zhaoyang.music.Dao.LocalListDao;
import zhaoyang.music.Dao.PlayListDao;
import zhaoyang.music.Dao.imp.LocalListDaoImp;
import zhaoyang.music.Dao.imp.PlayListDaoImp;
import zhaoyang.music.Domain.LocalList;
import zhaoyang.music.Domain.PlayList;
import zhaoyang.music.Domain.Song;
import zhaoyang.music.Media.MediaService;
import zhaoyang.music.Media.MusicPlayer;
import zhaoyang.music.R;
/**
 * Created by SPREADTRUM\david.zhao on 18-2-9.
 */

public class LocalMusicListAdapter extends BaseAdapter {
    private Context context;
    ArrayList<LocalList> menulist, addList;
    ArrayList<PlayList> listName = new ArrayList<PlayList>();
    MusicPlayer musicPlayer;
    public MyListener myListener;
    public String currentPath;
    private int currentPlayPosition;
    public ViewHolder viewHolder;
    private boolean move = false;
    private boolean visible = false;
    private long lasttime, nowtime;
    private Song song;
    private MediaService mediaService;
    private boolean[] checks;
    private boolean updateList;

    public interface MyListener{
        public void clickListener();
    }

    public LocalMusicListAdapter(Context context, ArrayList<LocalList> menulist,
                                 MyListener myListener, MediaService mediaService) {
        this.context = context;
        this.menulist = menulist;
        this.myListener = myListener;
        addList = new ArrayList<>();
        this.mediaService = mediaService;
        musicPlayer = MusicApplication.musicPlayer;
        checks = new boolean[this.menulist.size()];
        initPlayer();
    }

    @Override
    public int getCount() {
        return menulist.size();
    }

    @Override
    public Object getItem(int i) {
        return menulist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        updateList = true;

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.local_song_list, null);
            viewHolder = new ViewHolder();
            viewHolder.play_button = (ImageView)view.findViewById(R.id.play_button);
            viewHolder.song_num = (TextView)view.findViewById(R.id.song_num);
            viewHolder.song_name = (TextView)view.findViewById(R.id.song_name);
            viewHolder.song_check = (CheckBox)view.findViewById(R.id.song_check);
            viewHolder.song_name.setSelected(true);
            //viewHolder.song_check.setOnCheckedChangeListener(null);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        /*if (addList.contains(menu_list.get(i))) {
            viewHolder.song_check.setChecked(true);
        } else {
            viewHolder.song_check.setChecked(false);
        }*/
        if (visible) {
            viewHolder.song_check.setVisibility(View.VISIBLE);
        } else {
            viewHolder.song_check.setVisibility(View.GONE);
        }
        if (checks[i]) {
            Log.e("test", Integer.toString(i));
            viewHolder.song_check.setChecked(true);
        } else {
            Log.e("test fail", Integer.toString(i));
            viewHolder.song_check.setChecked(false);
        }
        viewHolder.song_num.setText(Integer.toString(i) + ".");
        viewHolder.song_name.setText(menulist.get(i).getAudio_name());
        final int k = i;
        viewHolder.song_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.e("test", "updatelist" + Boolean.toString(updateList));
                if (!updateList) {
                    if (b) {
                        Log.e("david", "check = " + menulist.get(k).getAudio_name());
                        addList.add(menulist.get(k));
                        checks[k] = true;
                        Log.e("test", k + "set true");
                    } else {
                        Log.e("david", "uncheck = " + menulist.get(k).getAudio_name());
                        if (addList.contains(menulist.get(k))) {
                            addList.remove(menulist.get(k));
                            checks[k] = false;
                            Log.e("test", k + "set false");
                        }
                    }
                }
            }
        });

        final int currentPosition = i;
        /*Log.e("david", "currentPosition" + currentPosition);Log.e("daivd", "long click "+view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("daivd", "click");
                RuntimeException e = new RuntimeException("click run is here");
                e.fillInStackTrace();
                //Log.e("david", "xxx", e);
                currentPlayPosition = currentPosition;
                //Log.e("david", "currentPlayPosition" + currentPlayPosition);
                currentPath = menu_list.get(currentPosition).getAudio_path();
                if (musicPlayer.isPlaying()) {
                    if (musicPlayer.getPlayingPath().equals(currentPath)) {
                        musicPlayer.pause();
                        Toast.makeText(context, currentPath + "play to pause", Toast.LENGTH_SHORT).show();
                    } else {
                        musicPlayer.reset();
                        musicPlayer.play(currentPath);
                        musicPlayer.setPlayingPath(currentPath);
                        Toast.makeText(context, currentPath + "play to stop/play", Toast.LENGTH_SHORT).show();
                    }
                    myListener.clickListener();
                } else if (musicPlayer.isPause()) {
                    if (musicPlayer.getPlayingPath().equals(currentPath)) {
                        musicPlayer.start();
                        Toast.makeText(context, currentPath + "pause to play", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, currentPath + "puase to stop/play", Toast.LENGTH_SHORT).show();
                        musicPlayer.reset();
                        musicPlayer.release();
                        //musicPlayer = new MusicPlayer();
                        musicPlayer.setPlayingPath(currentPath);
                        musicPlayer.play(currentPath);
                    }
                } else {
                    Toast.makeText(context, currentPath + "to play", Toast.LENGTH_SHORT).show();
                    musicPlayer.play(currentPath);
                    musicPlayer.setPlayingPath(currentPath);
                    myListener.clickListener();
                }
                notifyDataSetChanged();
                //view.requestFocus();
            }

        });

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Log.e("daivd", "long click");
                RuntimeException e = new RuntimeException("long click run is here");
                e.fillInStackTrace();
                //Log.e("david", "xxx", e);
                final long k = currentPosition;
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle(context.getResources().getString(R.string.control));
                alertDialog.setIcon(R.drawable.ic_media);
                String[] items = new String[]{context.getResources().getString(R.string.play),
                        context.getResources().getString(R.string.delete),
                                context.getResources().getString(R.string.addlist)};
                alertDialog.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        PlayListDao playList = new PlayListDaoImp(context);
                        currentPlayPosition = currentPosition;
                        switch (i) {
                            case 0:
                                Log.e("david", "currentPlayPosition" + currentPlayPosition);
                                currentPath = menu_list.get(currentPosition).getAudio_path();
                                if (musicPlayer.isPlaying()) {
                                    if (musicPlayer.getPlayingPath().equals(currentPath)) {
                                        musicPlayer.pause();
                                        Toast.makeText(context, currentPath + "play to pause", Toast.LENGTH_SHORT).show();
                                    } else {
                                        musicPlayer.reset();
                                        musicPlayer.play(currentPath);
                                        musicPlayer.setPlayingPath(currentPath);
                                        Toast.makeText(context, currentPath + "play to stop/play", Toast.LENGTH_SHORT).show();
                                    }
                                    myListener.clickListener();
                                } else if (musicPlayer.isPause()) {
                                    if (musicPlayer.getPlayingPath().equals(currentPath)) {
                                        musicPlayer.start();
                                        Toast.makeText(context, currentPath + "pause to play", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(context, currentPath + "puase to stop/play", Toast.LENGTH_SHORT).show();
                                        musicPlayer.reset();
                                        //musicPlayer.release();
                                        //musicPlayer = new MusicPlayer();
                                        musicPlayer.setPlayingPath(currentPath);
                                        musicPlayer.play(currentPath);
                                    }
                                } else {
                                    Toast.makeText(context, currentPath + "to play", Toast.LENGTH_SHORT).show();
                                    musicPlayer.play(currentPath);
                                    musicPlayer.setPlayingPath(currentPath);
                                    myListener.clickListener();
                                }
                                notifyDataSetChanged();
                                break;
                            case 1:
                                alertDialog.setTitle(context.getResources().getString(R.string.delete));
                                alertDialog.setIcon(R.drawable.ic_media);
                                //listName = (ArrayList<PlayList>) playList.getAllPlayList();
                                alertDialog.setPositiveButton(R.string.sure, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        File file = new File(menu_list.get(currentPlayPosition).getAudio_path());
                                        //Log.e("david", "position = " + currentPlayPosition + "file = " + file.toString());
                                        //Log.e("david", "exist = " + file.exists() + "file = " + file.isFile());
                                        if (file.exists() && file.isFile()) {
                                            Log.e("david", "file delete");
                                            file.delete();
                                            LocalList locallist = new LocalList();
                                            locallist = menu_list.get(currentPlayPosition);
                                            menu_list.remove(currentPlayPosition);
                                            notifyDataSetChanged();
                                            LocalListDao playlist = new LocalListDaoImp(context);
                                            playlist.deleteSong(locallist);
                                        } else {Log.e("david", "file delete fail");}
                                    }
                                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                alertDialog.create().show();
                                break;
                            case 2:
                                alertDialog.setTitle(context.getResources().getString(R.string.addlist));
                                alertDialog.setIcon(R.drawable.ic_media);
                                listName = (ArrayList<PlayList>) playList.getAllPlayList();
                                //Log.e("david", "listname size" + listName.size());
                                final String[] items = new String[listName.size()];
                                for (int j = 0; j < listName.size(); j++) {
                                    //Log.e("david", "listname size" + listName.get(j).getName());
                                    items[j] = listName.get(j).getName();
                                }
                                alertDialog.setItems(items, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        //Log.e("david", "click = " + i);
                                        //dialogClick(i, k);
                                        Log.e("david", "add to " + items[i] + listName.get(i).getName());
                                        LocalListDao playlist = new LocalListDaoImp(context);
                                        playlist.addSongToPlayList(addList, (int)listName.get(i).getId());
                                        addList.clear();
                                        notifyDataSetChanged();
                                        dialogInterface.dismiss();
                                    }
                                });
                                alertDialog.create().show();
                                break;
                        }
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.create().show();
                return true;
            }
        });*/
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(final View view, MotionEvent motionEvent) {
                Log.e("david", "on touch " + view);
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.e("david", "on touch down" + view + "time:" + lasttime);
                    lasttime = new Date().getTime();
                    move = false;

                } else if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    Log.e("david", "on touch move" + view);
                    move = true;
                    //return true;
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    Log.e("david", "on touch up" + view + "time:" + nowtime);
                    nowtime = new Date().getTime();
                    long time = nowtime - lasttime;
                    Log.e("david", "on touch up" + view + "time:" + time);

                    if (time > 500) {
                        Log.e("david", "long touch up for down" + view);
                        if (viewHolder.song_check.getVisibility() == View.GONE) {
                            viewHolder.song_check.setVisibility(View.VISIBLE);
                            visible = true;
                            notifyDataSetChanged();
                        } else {
                            final long k = currentPosition;
                            AlertDialog.Builder alertDialog =
                                    new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Dialog));
                            alertDialog.setTitle(context.getResources().getString(R.string.control));
                            alertDialog.setIcon(R.drawable.ic_media);
                            alertDialog.setCancelable(true);
                            String[] items = new String[]{context.getResources().getString(R.string.play),
                                    context.getResources().getString(R.string.delete),
                                    context.getResources().getString(R.string.addlist)};
                            alertDialog.setItems(items, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    AlertDialog.Builder alertDialog =
                                            new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Dialog));
                                    PlayListDao playList = new PlayListDaoImp(context);
                                    currentPlayPosition = currentPosition;
                                    mediaService.playPosition = currentPlayPosition;
                                    switch (i) {
                                        case 0:
                                            Log.e("david", "currentPlayPosition" + currentPlayPosition);
                                            currentPath = menulist.get(currentPosition).getAudio_path();
                                            if (mediaService.isPlaying()) {
                                                if (mediaService.getPlayingPath().equals(currentPath)) {
                                                    mediaService.pause();
                                                    //Toast.makeText(context, currentPath + "play to pause", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    mediaService.reset();
                                                    mediaService.setPlayingName(menulist.get(currentPosition).getAudio_name());
                                                    mediaService.play(currentPath);

                                                    mediaService.setPlayingPath(currentPath);
                                                    //Toast.makeText(context, currentPath + "play to stop/play", Toast.LENGTH_SHORT).show();
                                                }
                                                //myListener.clickListener();
                                            } else if (mediaService.isPause()) {
                                                if (mediaService.getPlayingPath().equals(currentPath)) {
                                                    mediaService.start();
                                                    //Toast.makeText(context, currentPath + "pause to play", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    //Toast.makeText(context, currentPath + "puase to stop/play", Toast.LENGTH_SHORT).show();
                                                    mediaService.reset();
                                                    //musicPlayer.release();
                                                    //musicPlayer = new MusicPlayer();
                                                    mediaService.setPlayingPath(currentPath);
                                                    mediaService.setPlayingName(menulist.get(currentPosition).getAudio_name());
                                                    mediaService.play(currentPath);
                                                }
                                            } else {
                                                //Toast.makeText(context, currentPath + "to play", Toast.LENGTH_SHORT).show();
                                                mediaService.setPlayingName(menulist.get(currentPosition).getAudio_name());
                                                mediaService.play(currentPath);
                                                mediaService.setPlayingPath(currentPath);

                                                //myListener.clickListener();
                                            }
                                            visible = false;
                                            myListener.clickListener();
                                            notifyDataSetChanged();
                                            break;
                                        case 1:
                                            alertDialog.setTitle(context.getResources().getString(R.string.delete));
                                            alertDialog.setIcon(R.drawable.ic_media);
                                            //listName = (ArrayList<PlayList>) playList.getAllPlayList();
                                            alertDialog.setPositiveButton(R.string.sure, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    File file = new File(menulist.get(currentPlayPosition).getAudio_path());
                                                    //Log.e("david", "position = " + currentPlayPosition + "file = " + file.toString());
                                                    //Log.e("david", "exist = " + file.exists() + "file = " + file.isFile());
                                                    if (file.exists() && file.isFile()) {
                                                        Log.e("david", "file delete");
                                                        file.delete();
                                                        LocalList locallist = new LocalList();
                                                        locallist = menulist.get(currentPlayPosition);
                                                        menulist.remove(currentPlayPosition);
                                                        visible = false;
                                                        notifyDataSetChanged();
                                                        LocalListDao playlist = new LocalListDaoImp(context);
                                                        playlist.deleteSong(locallist);
                                                    } else {
                                                        Log.e("david", "file delete fail");
                                                    }
                                                }
                                            }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                    visible = false;
                                                    notifyDataSetChanged();
                                                }
                                            });
                                            alertDialog.create().show();
                                            break;
                                        case 2:
                                            alertDialog.setTitle(context.getResources().getString(R.string.addlist));
                                            alertDialog.setIcon(R.drawable.ic_media);
                                            listName = (ArrayList<PlayList>) playList.getAllPlayList();
                                            //Log.e("david", "listname size" + listName.size());
                                            final String[] items = new String[listName.size()];
                                            for (int j = 0; j < listName.size(); j++) {
                                                //Log.e("david", "listname size" + listName.get(j).getName());
                                                items[j] = listName.get(j).getName();
                                            }
                                            alertDialog.setItems(items, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    //Log.e("david", "click = " + i);
                                                    //dialogClick(i, k);
                                                    Log.e("david", "add to " + items[i] + listName.get(i).getName());
                                                    LocalListDao playlist = new LocalListDaoImp(context);
                                                    playlist.addSongToPlayList(addList, (int) listName.get(i).getId());
                                                    addList.clear();
                                                    visible = false;
                                                    notifyDataSetChanged();
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                            alertDialog.create().show();
                                            break;
                                    }
                                    dialogInterface.dismiss();
                                }
                            });
                            alertDialog.create().show();
                            viewHolder.song_check.setVisibility(View.GONE);
                        }
                    } else {
                        Log.e("david", "short touch up for down" + view);
                        currentPlayPosition = currentPosition;
                        mediaService.playPosition = currentPlayPosition;
                        //Log.e("david", "currentPlayPosition" + currentPlayPosition);
                        currentPath = menulist.get(currentPosition).getAudio_path();
                        if (mediaService.isPlaying()) {
                            if (mediaService.getPlayingPath().equals(currentPath)) {
                                mediaService.pause();
                                //Toast.makeText(context, currentPath + "play to pause", Toast.LENGTH_SHORT).show();
                            } else {
                                mediaService.reset();
                                mediaService.setPlayingName(menulist.get(currentPosition).getAudio_name());
                                mediaService.play(currentPath);

                                mediaService.setPlayingPath(currentPath);
                                //Toast.makeText(context, currentPath + "play to stop/play", Toast.LENGTH_SHORT).show();
                            }
                            myListener.clickListener();
                        } else if (mediaService.isPause()) {
                            if (mediaService.getPlayingPath().equals(currentPath)) {
                                mediaService.start();
                                //Toast.makeText(context, currentPath + "pause to play", Toast.LENGTH_SHORT).show();
                            } else {
                                //Toast.makeText(context, currentPath + "puase to stop/play", Toast.LENGTH_SHORT).show();
                                mediaService.reset();
                                //musicPlayer.release();
                                //musicPlayer = new MusicPlayer();
                                mediaService.setPlayingPath(currentPath);
                                mediaService.setPlayingName(menulist.get(currentPosition).getAudio_name());
                                mediaService.play(currentPath);
                            }
                        } else {
                            //Toast.makeText(context, currentPath + "to play", Toast.LENGTH_SHORT).show();
                            mediaService.setPlayingName(menulist.get(currentPosition).getAudio_name());
                            //musicPlayer.play(currentPath);
                            Log.e("david", "mediaservice test");
                            mediaService.play(currentPath);

                            mediaService.setPlayingPath(currentPath);
                            //myListener.clickListener();
                        }
                        notifyDataSetChanged();
                        myListener.clickListener();
                    }
                    move = false;
                    //return true;
                }
                /*Log.e("david", "on touch" + view);
                //view.requestFocusFromTouch();
                RuntimeException e = new RuntimeException("on touch run is here");
                e.fillInStackTrace();
                Log.e("david", "xxx", e);
                return false;*/
                return true;
            }
        });
        updatePlayIcon(i,viewHolder);
        updateList = false;
        return view;
    }

    public void updatePlayIcon(int i, ViewHolder viewHolder) {
        String playPath = mediaService.getPlayingPath();
        String path = menulist.get(i).getAudio_path();
        if (mediaService.isPlaying() && playPath.equals(path)) {
            viewHolder.play_button.setVisibility(View.VISIBLE);
            viewHolder.play_button.setBackgroundResource(R.drawable.list_playing_indicator);
            viewHolder.song_num.setVisibility(View.INVISIBLE);
        } else if (mediaService.isPause() && playPath.equals(path)) {
            viewHolder.play_button.setVisibility(View.VISIBLE);
            viewHolder.play_button.setBackgroundResource(R.drawable.list_pause_indicator);
            viewHolder.song_num.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.play_button.setVisibility(View.INVISIBLE);
            viewHolder.song_num.setVisibility(View.VISIBLE);
        }

    }

    static class ViewHolder{
        private ImageView play_button;
        private TextView song_num;
        private TextView song_name;
        private CheckBox song_check;

    }

    public void next() {
        currentPlayPosition = mediaService.playPosition;
        Log.e("david", "" + currentPlayPosition);
        currentPlayPosition++;
        if (currentPlayPosition >= menulist.size()) {
            currentPlayPosition = 0;
        }
        if (menulist.size() == 0){
            return;
        }
        Log.e("david", menulist.get(currentPlayPosition).getAudio_path());
        mediaService.setPlayingName(menulist.get(currentPlayPosition).getAudio_name());
        mediaService.play(menulist.get(currentPlayPosition).getAudio_path());

        nowSong();
        EventBus.getDefault().postSticky(song);
        viewHolder.play_button.setVisibility(View.VISIBLE);
        viewHolder.play_button.setBackgroundResource(R.drawable.list_playing_indicator);
        viewHolder.song_num.setVisibility(View.INVISIBLE);
        notifyDataSetChanged();
        //myListener.clickListener();
        mediaService.playPosition = currentPlayPosition;
    }

    public void previous() {
        currentPlayPosition = mediaService.playPosition;
        Log.e("david", "" + currentPlayPosition);
        currentPlayPosition--;
        if (currentPlayPosition < 0) {
            currentPlayPosition = menulist.size() - 1;
        }
        if (menulist.size() == 0){
            return;
        }
        Log.e("david", menulist.get(currentPlayPosition).getAudio_path());
        mediaService.setPlayingName(menulist.get(currentPlayPosition).getAudio_name());
        mediaService.play(menulist.get(currentPlayPosition).getAudio_path());

        nowSong();
        EventBus.getDefault().postSticky(song);
        viewHolder.play_button.setVisibility(View.VISIBLE);
        viewHolder.play_button.setBackgroundResource(R.drawable.list_playing_indicator);
        viewHolder.song_num.setVisibility(View.INVISIBLE);
        notifyDataSetChanged();
        //myListener.clickListener();
        mediaService.playPosition = currentPlayPosition;
    }

    public void initPlayer() {
        mediaService.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mediaPlayer) {
                //mediaPlayer.start();

            }
        });
        mediaService.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Log.e("david", "completed");
                if (mediaService.isPlaying()|| mediaService.isPause()){
                    Log.e("david", "maybe inormal complete");
                } else {
                    Log.e("david", "not play or pause");
                    RuntimeException e = new RuntimeException("run is here");
                    e.fillInStackTrace();
                    //Log.e("david", "xxx", e);
                    mediaService.reset();
                    next();
                }

            }
        });

        mediaService.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                Log.e("david", "onError");
                return true;
            }
        });

        mediaService.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                Log.e("david", "prepareddddddddddd");
                myListener.clickListener();
            }
        });
    }

    public void nowSong() {
        song = new Song();
        song.setPath(mediaService.getPlayingPath());
        song.setName(mediaService.getPlayingName());
        song.setNowTime(mediaService.getCurrentPosition());
        song.setLongTime(mediaService.getDuration());
    }

    public void playFirst() {
        mediaService.setPlayingName(menulist.get(currentPlayPosition).getAudio_name());
        mediaService.play(menulist.get(currentPlayPosition).getAudio_path());

        nowSong();
        EventBus.getDefault().postSticky(song);
        viewHolder.play_button.setVisibility(View.VISIBLE);
        viewHolder.play_button.setBackgroundResource(R.drawable.list_playing_indicator);
        viewHolder.song_num.setVisibility(View.INVISIBLE);
        notifyDataSetChanged();
        mediaService.playPosition = 0;
    }
}