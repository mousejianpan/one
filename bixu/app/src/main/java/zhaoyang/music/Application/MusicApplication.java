package zhaoyang.music.Application;

import android.Manifest;
import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import zhaoyang.music.Media.MediaService;
import zhaoyang.music.Media.MusicPlayer;
import zhaoyang.music.utils.MusicUtils;
import zhaoyang.music.utils.RequestPermission;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-11.
 */

public class MusicApplication extends Application {
    public static MusicApplication instance;
    public static MusicPlayer musicPlayer;
    public static MusicUtils musicUtils;
    public static boolean start;
    public static boolean firstStart;
    public static String playingName;
    public static MediaService mediaService;

    @Override
    public void onCreate() {
        //resolve :FileUriExposedException
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
        super.onCreate();
    }

    public static MusicApplication getInstance() {
        Log.e("david", "app getInstance");
        musicPlayer = MusicPlayer.getInstance();
        //musicPlayer = new MusicPlayer();
        mediaService = MediaService.getInstance();
        musicUtils = new MusicUtils();
        start = true;
        firstStart = true;
        playingName = null;
        return instance;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Toast.makeText(instance, "low memory ,I will die, byebye", Toast.LENGTH_SHORT).show();
    }

    public String getVersionInfo() {
        String version ="0.0.0";
        PackageManager packagemanager = getPackageManager();
        try {
            PackageInfo packageInfo = packagemanager.getPackageInfo(getPackageName(), 0);
            version = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    @Override
    public void onTerminate() {
        Log.e("david", "onterminate - 0");
        if (musicPlayer != null && musicPlayer.isPlaying()) {
            musicPlayer.stop();
            musicPlayer.release();
            musicPlayer = null;
            Log.e("david", "onterminate - 1");
        }
        super.onTerminate();
    }
}
