package zhaoyang.music.Dao;

import java.util.List;

import zhaoyang.music.Domain.PlayList;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-26.
 */

public interface PlayListDao {
    public void createPlayList(String name);
    public void removePlayList(Integer id);
    public List<PlayList> getAllPlayList();
    public void updatePlayList(Integer id, String name);
}
