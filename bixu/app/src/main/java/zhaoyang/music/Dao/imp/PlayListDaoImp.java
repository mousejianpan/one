package zhaoyang.music.Dao.imp;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import zhaoyang.music.DB.Common;
import zhaoyang.music.Dao.PlayListDao;
import zhaoyang.music.Domain.PlayList;
import zhaoyang.music.utils.ContentValuesBuilder;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-26.
 */

public class PlayListDaoImp extends ContextWrapper implements PlayListDao {
    private ContentResolver contentResolver;
    private Uri uri = Uri.parse(Common.PLAY_LIST_URI);
    private ContentValues contentValues = new ContentValues();
    private PlayList playList;

    public PlayListDaoImp(Context base) {
        super(base);
    }

    @Override
    public void createPlayList(String name) {
        if (name.equals(null)){
            return;
        }
        contentResolver = getContentResolver();
        playList = new PlayList();
        playList.setName(name);
        playList.setAdd_date(new Date());
        playList.setmodified_date(new Date());
        try {
            contentValues = ContentValuesBuilder.getInstance().builder(playList);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        contentResolver.insert(uri, contentValues);

    }

    @Override
    public void removePlayList(Integer id) {
        contentResolver = getContentResolver();
        contentResolver.delete(uri, "id = ?", new String[]{id.toString()});
    }

    @Override
    public void updatePlayList(Integer id, String name) {
        if (name.equals(null)){
            return;
        }
        contentResolver = getContentResolver();
        playList = new PlayList();
        playList.setName(name);
        playList.setAdd_date(new Date());
        playList.setmodified_date(new Date());
        try {
            contentValues = ContentValuesBuilder.getInstance().builder(playList);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        contentResolver.update(uri, contentValues, "id = ?", new String[]{id.toString()});
    }

    @Override
    public List<PlayList> getAllPlayList() {
        List<PlayList> list = new ArrayList<PlayList>();
        contentResolver = getContentResolver();
        String[] projection = {"id", "name"};
        Cursor cursor = contentResolver.query(uri, projection, null, null, null);
        //Log.e("david", "getalllist");
        if (cursor.moveToFirst()) {
            //Log.e("david", "list not null count = " + cursor.getCount());
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                PlayList playList = new PlayList();
                playList.setName(cursor.getString(1));
                playList.setId(cursor.getLong(0));
                list.add(playList);
            }
        }
        return list;
    }


}
