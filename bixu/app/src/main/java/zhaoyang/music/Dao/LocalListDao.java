package zhaoyang.music.Dao;

import android.content.ContentValues;

import java.util.List;

import zhaoyang.music.Domain.LocalList;
import zhaoyang.music.Domain.PlayList;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-9.
 */

public interface LocalListDao {
    List<LocalList> getMusicListByPlayListId(int id);
    void addSongToPlayList(List<LocalList> add_list, int id);
    List<LocalList> getLocalMusicList();
    List<LocalList> getLocalListByName();
    void removeSongFromList(String song, String listname);
    void deleteSong(LocalList localList);
}
