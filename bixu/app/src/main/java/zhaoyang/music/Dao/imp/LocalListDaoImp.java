package zhaoyang.music.Dao.imp;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import zhaoyang.music.DB.Common;
import zhaoyang.music.DB.DataBaseHelper;
import zhaoyang.music.Dao.LocalListDao;
import zhaoyang.music.Domain.LocalList;
import zhaoyang.music.Domain.PlayList;
import zhaoyang.music.utils.ContentValuesBuilder;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-9.
 */

public class LocalListDaoImp extends ContextWrapper implements LocalListDao {
    private Uri uri = Uri.parse(Common.LOCAL_LIST_URI);
    private ContentValues contentValues = new ContentValues();
    private Context context;
    public LocalListDaoImp(Context base) {
        super(base);
        context = base;
    }

    @Override
    public List<LocalList> getMusicListByPlayListId(int id) {
        Log.e("david", "get for list id = " + id);
        List<LocalList> music_list = new ArrayList<LocalList>();
        Cursor cursor = getContentResolver().query(uri, null, "playlist_id = ?",
                new String[]{Integer.toString(id)}, null);
        if (cursor.moveToFirst()) {
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                LocalList localList = new LocalList();
                localList.setPlaylist_id(cursor.getInt(1));
                localList.setAudio_name(cursor.getString(2));
                localList.setAudio_path(cursor.getString(3));
                localList.setAdd_date(cursor.getString(4));
                localList.setModified_date(cursor.getString(5));
                music_list.add(localList);
            }
        }
        cursor.close();
        return music_list;

    }

    @Override
    public void addSongToPlayList(List<LocalList> add_list, int id) {
        if (add_list.isEmpty()){
            return;
        }
        ContentResolver contentResolver = getContentResolver();
            for (int i = 0; i < add_list.size(); i++) {
                add_list.get(i).setPlaylist_id(id);
                try {
                    contentValues = ContentValuesBuilder.getInstance().builder(add_list.get(i));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                contentResolver.update(uri, contentValues, "audio_path LIKE ? ",
                        new String[]{add_list.get(i).getAudio_path()});
        }
    }

    @Override
    public List<LocalList> getLocalMusicList() {
        List<LocalList> music_list = new ArrayList<LocalList>();
        ContentResolver contentResolver = getContentResolver();
        //DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        //SQLiteDatabase sqLiteDatabase = dataBaseHelper.getWritableDatabase();;
        //dataBaseHelper.clear(sqLiteDatabase);
        Cursor cursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null,
                null, null, MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
        Log.e("david", "get music = " + cursor.getCount());
        if (cursor.moveToFirst()) {
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                LocalList localList = new LocalList();
                localList.setId(cursor.getLong(cursor.getColumnIndexOrThrow(
                        MediaStore.Audio.Media._ID
                )));
                localList.setAudio_name(cursor.getString(cursor.getColumnIndexOrThrow(
                        MediaStore.Audio.Media.DISPLAY_NAME
                )));
                localList.setAudio_path(cursor.getString(cursor.getColumnIndexOrThrow(
                        MediaStore.Audio.Media.DATA
                )));
                localList.setAdd_date(new Date().toString());
                localList.setModified_date(new Date().toString());

                //
                Cursor cursor1 = getContentResolver().query(uri, null, "audio_path LIKE ?",
                        new String[]{localList.getAudio_path()}, null);
                Log.e("david", localList.getAudio_path() + "/" + cursor.getString(3));
                if (cursor1.moveToNext()) {
                    Log.e("david", "find data" + localList.getAudio_name());
                    cursor1.close();
                } else {
                    Log.e("david", "not find data" + localList.getAudio_path());
                    try {
                        contentValues = ContentValuesBuilder.getInstance().builder(localList);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    getContentResolver().insert(uri, contentValues);
                    cursor1.close();
                }
                music_list.add(localList);
            }
        }
        cursor.close();
        return music_list;
    }

    @Override
    public List<LocalList> getLocalListByName() {
        return null;
    }

    @Override
    public void removeSongFromList(String song, String listname) {

    }

    @Override
    public void deleteSong(final LocalList localList) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Cursor cursor = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null,
                        null, null, MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
                if (cursor.getCount() == 0) {
                    return;
                }
                cursor.moveToFirst();
                //int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
                cursor.close();

                try {
                    int id = (int)localList.getId();
                    //Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id);
                    Log.e("david", "path" + localList.getAudio_path());
                    getContentResolver().delete(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                            MediaStore.Images.Media.DATA + " LIKE ?", new String[]{localList.getAudio_path()});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
