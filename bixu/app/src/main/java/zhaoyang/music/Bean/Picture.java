package zhaoyang.music.Bean;

import android.graphics.Bitmap;
import java.io.Serializable;

/**
 * Created by SPREADTRUM\david.zhao on 18-3-16.
 */

public class Picture implements Serializable {
    private String bitmap;
    private String path;
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {

        return name;
    }

    public void setBitmap(String bitmap) {
        this.bitmap = bitmap;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getBitmap() {

        return bitmap;
    }

    public String getPath() {
        return path;
    }
}
