package zhaoyang.music.Bean;

import android.support.v4.app.Fragment;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-7.
 */

public class Tab {
    private String title;
    private int icon;
    private Class fragment;

    public Tab(String title, int icon, Class fragment) {
        this.title = title;
        this.icon = icon;
        this.fragment = fragment;
    }

    public Tab(int icon, Class fragment) {
        this.icon = icon;
        this.fragment = fragment;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getIcon() {
        return this.icon;
    }

    public void setFragment(Class fragment) {
        this.fragment = fragment;
    }

    public Class getFragment() {
        return this.fragment;
    }
}
