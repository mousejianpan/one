package zhaoyang.music.Bean;

import java.io.Serializable;

/**
 * Created by SPREADTRUM\david.zhao on 18-3-20.
 */

public class Video implements Serializable {
    private String path;
    private String name;
    private String thumb;
    private String title;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {

        return title;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getThumb() {

        return thumb;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {

        return path;
    }

    public String getName() {
        return name;
    }
}
