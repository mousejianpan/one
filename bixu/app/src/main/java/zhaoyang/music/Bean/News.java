package zhaoyang.music.Bean;

import java.io.Serializable;

/**
 * Created by SPREADTRUM\david.zhao on 18-4-8.
 */

public class News implements Serializable {
    private String title;
    private String content;
    private String time;
    private String image;
    private String Details;
    private String detailContent;

    public void setDetailContent(String detailContent) {
        this.detailContent = detailContent;
    }

    public String getDetailContent() {

        return detailContent;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public String getDetails() {

        return Details;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {

        return source;
    }

    private String source;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {

        return title;
    }

    public String getContent() {
        return content;
    }

    public String getTime() {
        return time;
    }

    public String getImage() {
        return image;
    }
}
