package zhaoyang.music.Bean;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonWriter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import io.reactivex.annotations.Nullable;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * Created by SPREADTRUM\david.zhao on 18-3-7.
 */

public final class MyConverter extends Converter.Factory {
    private final Gson gson = new Gson();
    private MyConverter(Gson gson) {
        super();
    }

    public static MyConverter create() {
        Log.e("david", "converter is maine 1");
        return create(new Gson());
    }
    public static MyConverter create(Gson gson) {
        return new MyConverter(gson);

    }

    @Nullable
    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        //return super.responseBodyConverter(type, annotations, retrofit);
        Log.e("david", "converter is maine 3");
        TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(type));
        return new ResponseBodyConverter<>(adapter);

    }

    @Nullable
    @Override
    public Converter<?, RequestBody> requestBodyConverter(Type type, Annotation[] parameterAnnotations, Annotation[] methodAnnotations, Retrofit retrofit) {
        TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(type));
        Log.e("david", "converter is maine 2");
        //return super.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit);
        return new RequestBodyConverter<>(gson, adapter);
    }

    @Nullable
    @Override
    public Converter<?, String> stringConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        return super.stringConverter(type, annotations, retrofit);
    }

    public class RequestBodyConverter<T> implements Converter<T, RequestBody> {
        private final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=UTF-8");
        private final Charset UTF_8 = Charset.forName("UTF-8");

        private Gson gson = null;
        private TypeAdapter<T> adapter = null;
        public RequestBodyConverter(Gson gson, TypeAdapter<T> adapter){
            this.gson = gson;
            this.adapter = adapter;

        }

        @Override
        public RequestBody convert(T value) throws IOException {
            Log.e("david", "request = " + value.toString());
            Buffer buffer = new Buffer();
            Writer writer = new OutputStreamWriter(buffer.outputStream(), UTF_8);
            JsonWriter jsonWriter = new JsonWriter(writer);
            adapter.write(jsonWriter, value);
            jsonWriter.flush();
            byte[] bytes = buffer.readByteArray();
            /*ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                gzipOutputStream.write(bytes);
                gzipOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            bytes = byteArrayOutputStream.toByteArray();*/
            Log.e("david", bytes.toString());
            return RequestBody.create(MEDIA_TYPE, bytes);
        }
    }
    public class ResponseBodyConverter<T> implements Converter<ResponseBody, T> {
        private final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=UTF-8");
        private final Charset UTF_8 = Charset.forName("UTF-8");
        private TypeAdapter<T> adapter = null;
        public ResponseBodyConverter(TypeAdapter<T> adapter){
            this.adapter = adapter;

        }

        @Override
        public T convert(ResponseBody value) throws IOException {
            Log.e("david", "converter is maine 4");
            byte[] bytes = value.bytes();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream);
            byte[] bytes1 = new byte[1024];
            gzipInputStream.read(bytes1);
            int n = 0;
            while ((n = gzipInputStream.read(bytes1)) > 0) {
                byteArrayOutputStream.write(bytes1);
            }
            return adapter.fromJson(bytes1.toString());
        }
    }
}