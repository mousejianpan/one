package zhaoyang.music.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Date;

import zhaoyang.music.Domain.PlayList;
import zhaoyang.music.R;
import zhaoyang.music.utils.ContentValuesBuilder;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-9.
 */

public class DataBaseHelper extends SQLiteOpenHelper {
    public static final String LOCAL_TABLE_NAME = "local_table";
    public static final String PLAY_TABLE_NAME = "play_table";
    public static final String DATABASE_NAME = "music.db";
    Context context;

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //it is list song table
        String playTable = "CREATE TABLE IF NOT EXISTS play_table (id INTEGER PRIMARY KEY, " +
                "name TEXT CHECK(name != ''), add_date INTEGER, modified_date INTEGER);";
        //it is note list table
        String localTable = "CREATE TABLE IF NOT EXISTS local_table (id INTEGER PRIMARY KEY," +
                "playlist_id INTEGER NOT NULL, audio_name TEXT CHECK(audio_name != ''), " +
                "audio_path TEXT CHECK(audio_path != ''), add_date INTEGER, modified_date INTEGER);";
        sqLiteDatabase.execSQL(localTable);
        sqLiteDatabase.execSQL(playTable);

        PlayList playList = new PlayList();
        playList.setName(context.getString(R.string.myfavourite));
        Date date = new Date();
        playList.setAdd_date(date);
        playList.setmodified_date(date);
        try {
            ContentValues cv = ContentValuesBuilder.getInstance().builder(playList);
            sqLiteDatabase.insert("play_table", null, cv);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String localDrop = "DROP TABLE IF EXIST local_table";
        String playDrop = "DROP TABLE IF EXIST play_table";
        sqLiteDatabase.execSQL(localDrop);
        sqLiteDatabase.execSQL(playDrop);
        onCreate(sqLiteDatabase);

    }

    public void clear(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("DELETE FROM " + DataBaseHelper.LOCAL_TABLE_NAME);
    }
}
