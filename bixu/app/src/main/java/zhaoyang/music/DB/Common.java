package zhaoyang.music.DB;

/**
 * Created by SPREADTRUM\david.zhao on 18-2-9.
 */

public class Common {
    public static final String AUTHORITY = "zhaoyang.music.DB.Common.DataBaseProvider";
    public static final String LOCAL_LIST_PATH = "locallist";
    public static final String PLAY_LIST_PATH = "playlist";
    public static final String LOCAL_LIST_URI = "content://" + AUTHORITY + "/" + LOCAL_LIST_PATH;
    public static final String PLAY_LIST_URI = "content://" + AUTHORITY +"/" + PLAY_LIST_PATH;

}
