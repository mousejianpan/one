package zhaoyang.music.DB;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class DataBaseProvider extends ContentProvider {
    private DataBaseHelper dataBaseHelper;
    private static UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    SQLiteDatabase database;
    private static int LOCALLIST = 1;
    private static int PLAYLIST = 2;
    static {
        uriMatcher.addURI(Common.AUTHORITY, Common.LOCAL_LIST_PATH, LOCALLIST);
        uriMatcher.addURI(Common.AUTHORITY, Common.PLAY_LIST_PATH, PLAYLIST);
    }
    public DataBaseProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        //throw new UnsupportedOperationException("Not yet implemented");
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case 1:
                count = database.delete(DataBaseHelper.LOCAL_TABLE_NAME, selection, selectionArgs);
                break;
            case 2:
                count = database.delete(DataBaseHelper.PLAY_TABLE_NAME, selection, selectionArgs);
                break;
            default:
                break;
        }
        return count;
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // TODO: Implement this to handle requests to insert a new row.
        //throw new UnsupportedOperationException("Not yet implemented");
        long count = 0;
        switch (uriMatcher.match(uri)) {
            case 1:
                count = database.insert(DataBaseHelper.LOCAL_TABLE_NAME, null, values);
                break;
            case 2:
                count = database.insert(DataBaseHelper.PLAY_TABLE_NAME, null, values);
                break;
            default:
                break;
        }
        if (count > 0) {
            return uri;
        } else {
            return null;
        }
    }

    @Override
    public boolean onCreate() {
        // TODO: Implement this to initialize your content provider on startup.
        dataBaseHelper = new DataBaseHelper(getContext());
        database = dataBaseHelper.getWritableDatabase();
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        // TODO: Implement this to handle query requests from clients.
        //throw new UnsupportedOperationException("Not yet implemented");
        Cursor cursor = null;
        switch (uriMatcher.match(uri)) {
            case 1:
                cursor = database.query(DataBaseHelper.LOCAL_TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            case 2:
                cursor= database.query(DataBaseHelper.PLAY_TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            default:
                break;
        }
        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        //throw new UnsupportedOperationException("Not yet implemented");
        int result = 0;
        switch (uriMatcher.match(uri)) {
            case 1:
                result = database.update(DataBaseHelper.LOCAL_TABLE_NAME, values, selection, selectionArgs);
                break;
            case 2:
                result = database.update(DataBaseHelper.PLAY_TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                break;
        }
        return 1;
    }
}
