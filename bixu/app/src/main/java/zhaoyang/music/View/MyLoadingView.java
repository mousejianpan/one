package zhaoyang.music.View;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;

/**
 * Created by SPREADTRUM\david.zhao on 18-4-11.
 */

public class MyLoadingView extends View {
    private Circle circle, circle2;
    private Paint paint, paint1, paint3;
    private AnimatorSet animatorSet;
    private float x, y, t, b;
    private int gree = 0;
    public MyLoadingView(Context context) {
        super(context);
    }

    public MyLoadingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        circle.setCenterX(getWidth() / 2);
        circle.setCenterY(getHeight() / 2);
        circle2.setCenterX(getWidth() / 2);
        circle2.setCenterY(getHeight() / 2);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        circle.setCenterX(getWidth() / 2);
        circle.setCenterY(getHeight() / 2);
        circle2.setCenterX(getWidth() / 2);
        circle2.setCenterY(getHeight() / 2);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //super.onDraw(canvas);
        x = circle.getCenterX() - circle.getRadius();
        y = circle.getCenterY() - circle.getRadius();
        t = circle.getCenterX() + circle.getRadius();
        b = circle.getCenterY() + circle.getRadius();
        Log.e("david", "hua yuan a " + circle.getCenterX() + "/" + circle.getCenterY() + "/" + circle.getRadius());
        canvas.drawCircle(circle.getCenterX(), circle.getCenterY(), circle.getRadius(), paint);
        RectF oval = new RectF(x, y, t, b);
        canvas.drawArc(oval, -15, 15, true, paint3);
        canvas.drawCircle(circle2.getCenterX(), circle2.getCenterY(), circle2.getRadius(), paint1);
        canvas.drawText("loading...", circle.getCenterX()-50, circle.getCenterY(), paint3);
    }

    public void configAnimation() {
        animatorSet = new AnimatorSet();
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(circle, "radius", 150, 450, 150);
        objectAnimator.setRepeatCount(ValueAnimator.INFINITE);
        objectAnimator.setInterpolator(new LinearInterpolator());
        objectAnimator.setDuration(2000);
        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(circle2, "radius", 100, 400, 100);
        objectAnimator1.setRepeatCount(ValueAnimator.INFINITE);
        objectAnimator1.setInterpolator(new LinearInterpolator());
        objectAnimator1.setDuration(2000);
        //animatorSet.setDuration(1000);
        animatorSet.playTogether(objectAnimator, objectAnimator1);
    }

    public void init() {
        circle = new Circle();
        circle2 = new Circle();
        circle.setRadius(350);
        circle2.setRadius(230);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.GREEN);
        paint.setStyle(Paint.Style.FILL);
        paint1 = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint1.setColor(Color.parseColor("#DEDEDE"));
        paint1.setStyle(Paint.Style.FILL);
        paint3 = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint3.setColor(Color.RED);
        paint3.setTextSize(30);
        paint3.setStyle(Paint.Style.FILL);
        configAnimation();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        animatorSet.start();
    }

    @Override
    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        animatorSet.start();
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        animatorSet.start();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        animatorSet.end();
    }

    public class Circle {
        private float radius = 100;
        private float centerX = 0;
        private float centerY = 0;
        private float rotationY = 0;


        public float getRotationY() {
            return rotationY;
        }

        public void setRotationY(float rotationY) {
            this.rotationY = rotationY;
            invalidate();
        }

        public float getRadius() {
            return radius;
        }

        @Keep
        public void setRadius(float radius) {
            this.radius = radius;
            invalidate();
        }

        public float getCenterX() {
            return centerX;
        }

        public void setCenterX(float centerX) {
            this.centerX = centerX;
        }

        public float getCenterY() {
            return centerY;
        }

        public void setCenterY(float centerY) {
            this.centerY = centerY;
        }
    }
}
