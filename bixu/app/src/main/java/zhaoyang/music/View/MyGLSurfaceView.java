package zhaoyang.music.View;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by SPREADTRUM\david.zhao on 18-4-16.
 */

public class MyGLSurfaceView extends GLSurfaceView {

    public MyGLSurfaceView(Context context) {
        super(context);
    }

    public MyGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void init() {
        setRenderer(new MyRender());
        setRenderMode(RENDERMODE_WHEN_DIRTY);
    }

    public class MyRender implements GLSurfaceView.Renderer {
        @Override
        public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
            gl10.glDisable(GL10.GL_DITHER);
            gl10.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_FASTEST);
            gl10.glClearColorx(0, 0, 0, 0);
            gl10.glEnable(GL10.GL_DEPTH_TEST);
        }

        @Override
        public void onSurfaceChanged(GL10 gl10, int i, int i1) {
            gl10.glViewport(0, 0, i, i1);
            gl10.glMatrixMode(GL10.GL_PROJECTION);
            gl10.glLoadIdentity();
        }

        @Override
        public void onDrawFrame(GL10 gl10) {

        }
    }
}
