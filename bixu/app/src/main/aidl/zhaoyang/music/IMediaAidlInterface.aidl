// IMediaAidlInterface.aidl
package zhaoyang.music;

// Declare any non-default types here with import statements

interface IMediaAidlInterface {
    void play(String path);
    void start();
    void stop();
    void pause();
    void reset();
    int getCurrentPosition();
    boolean isPlaying();
    void seekTo(int i);
    int getDuration();
    void setOnSeekCompleteListener();
    void setDataSource(String path);
    void setOnCompletionListener();
    boolean isPause();
    String getPlayingPath();
    void setPlayingPath(String path);
    void setPlayingName(String name);
    String getPlayingName();
}
