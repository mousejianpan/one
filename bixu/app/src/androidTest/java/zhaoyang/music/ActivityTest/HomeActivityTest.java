package zhaoyang.music.ActivityTest;

import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import org.junit.Rule;
import org.junit.Test;

import zhaoyang.music.Activity.HomeActivity;
import zhaoyang.music.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
/**
 * Created by SPREADTRUM\david.zhao on 18-4-16.
 */

public class HomeActivityTest {
    @Rule
    public ActivityTestRule<HomeActivity> mHomeActivityTestRule =
            new ActivityTestRule<HomeActivity>(HomeActivity.class);

    @Test
    public void textViewTest() throws Exception {
        onView(withId(R.id.play_list)).check(matches(withText("音乐")));
    }
}
